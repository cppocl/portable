#!/bin/sh
g++ --version
mkdir temp
chdir temp
git clone https://gitlab.com/cppocl/compiler
git clone https://gitlab.com/cppocl/platform
git clone https://gitlab.com/cppocl/portable
git clone https://gitlab.com/cppocl/unit_test_framework
cd portable/unit_tests
make install
make
bin/portable_unit_tests_main

