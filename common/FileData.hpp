/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_FILEDATA_HPP
#define OCL_GUARD_PORTABLE_COMMON_FILEDATA_HPP

#include "DateTime64.hpp"
#include "StringUtility.hpp"
#include "StringCopy.hpp"
#include <cstdint>

/// File information for:
///     filename.
///     file size.
///     creation date/time.
///     last accessed date/time.
///     last modified date/time.
///     is directory flag.
///
/// NOTE: filename may or may not contain a full or relative path.
namespace ocl
{

template<typename CharType>
class FileData
{
public:
    typedef CharType char_type;
    typedef std::uint64_t file_size_type;
    typedef StringUtility<char_type> string_utility_type;
    typedef StringCopy<char_type> string_copy_type;
    typedef typename string_copy_type::size_type file_length_type;

public:
    FileData(char_type const* filename = nullptr, file_size_type file_size = 0) noexcept
        : m_filename(nullptr)
        , m_filename_length(0)
        , m_file_size(file_size)
        , m_is_directory(false)
    {
        CopyFilename(filename);
    }

    FileData(char_type*&& filename,
             file_size_type&& file_size,
             ocl::DateTime64&& creation_date_time,
             ocl::DateTime64&& last_accessed_date_time,
             ocl::DateTime64&& last_modified_date_time,
             bool&& is_directory) noexcept
        : m_filename(filename)
        , m_filename_length(string_utility_type::GetLength(filename))
        , m_file_size(file_size)
        , m_creation_date_time(static_cast<DateTime64&&>(creation_date_time))
        , m_last_accessed_date_time(static_cast<DateTime64&&>(last_accessed_date_time))
        , m_last_modified_date_time(static_cast<DateTime64&&>(last_modified_date_time))
        , m_is_directory(is_directory)
    {
        filename = nullptr;
        file_size = 0;
        is_directory = false;
    }

    FileData(char_type const* filename,
             file_size_type file_size,
             ocl::DateTime64 const& creation_date_time,
             ocl::DateTime64 const& last_accessed_date_time,
             ocl::DateTime64 const& last_modified_date_time,
             bool is_directory) noexcept
        : m_filename(nullptr)
        , m_filename_length(0)
        , m_file_size(file_size)
        , m_creation_date_time(creation_date_time)
        , m_last_accessed_date_time(last_accessed_date_time)
        , m_last_modified_date_time(last_modified_date_time)
        , m_is_directory(is_directory)
    {
        CopyFilename(filename);
    }

    FileData(FileData const& file_data) noexcept
        : m_filename(nullptr)
        , m_filename_length(0)
        , m_file_size(file_data.m_file_size)
        , m_creation_date_time(file_data.m_creation_date_time)
        , m_last_accessed_date_time(file_data.m_last_accessed_date_time)
        , m_last_modified_date_time(file_data.m_last_modified_date_time)
        , m_is_directory(file_data.m_is_directory)
    {
        CopyFilename(file_data.m_filename, file_data.m_filename_length);
    }

    FileData(FileData&& file_data) noexcept
        : m_filename(file_data.m_filename)
        , m_filename_length(file_data.m_filename_length)
        , m_file_size(file_data.m_file_size)
        , m_creation_date_time(static_cast<DateTime64&&>(file_data.m_creation_date_time))
        , m_last_accessed_date_time(static_cast<DateTime64&&>(file_data.m_last_accessed_date_time))
        , m_last_modified_date_time(static_cast<DateTime64&&>(file_data.m_last_modified_date_time))
        , m_is_directory(file_data.m_is_directory)
    {
        file_data.m_filename = nullptr;
        file_data.m_filename_length = 0;
        file_data.m_file_size = 0;
        file_data.m_creation_date_time.Clear();
        file_data.m_last_accessed_date_time.Clear();
        file_data.m_last_modified_date_time.Clear();
        file_data.m_is_directory = false;
    }

    ~FileData()
    {
        Free();
    }

    FileData<char_type>& operator =(FileData<char_type> const& file_data) noexcept
    {
        Copy(file_data);
        return *this;
    }

    FileData<char_type>& operator =(FileData<char_type>&& file_data) noexcept
    {
        Move(file_data);
        return *this;
    }

public:
    char_type const* GetFilename() const noexcept
    {
        return m_filename;
    }

    file_length_type GetFilenameLength() const noexcept
    {
        return m_filename_length;
    }

    file_size_type GetFileSize() const noexcept
    {
        return m_file_size;
    }

    ocl::DateTime64 const& GetCreationDateTime() const noexcept
    {
        return m_creation_date_time;
    }

    ocl::DateTime64 const& GetLastAccessedDateTime() const noexcept
    {
        return m_last_accessed_date_time;
    }

    ocl::DateTime64 const& GetLastModifiedDateTime() const noexcept
    {
        return m_last_modified_date_time;
    }

    bool IsDirectory() const noexcept
    {
        return m_is_directory;
    }

    void Copy(FileData<char_type> const& file_data) noexcept
    {
        Free();
        CopyFilename(file_data.m_filename, file_data.m_filename_length);
        m_filename_length = file_data.m_filename_length;
        m_file_size = file_data.m_file_size;
        m_creation_date_time = file_data.m_creation_date_time;
        m_last_accessed_date_time = file_data.m_last_accessed_date_time;
        m_last_modified_date_time = file_data.m_last_modified_date_time;
        m_is_directory = file_data.m_is_directory;
    }

    void Move(FileData<char_type>& file_data) noexcept
    {
        Free();
        m_filename = file_data.m_filename;
        file_data.m_filename = nullptr;
        m_filename_length = file_data.m_filename_length;
        file_data.m_filename_length = 0;
        m_file_size = file_data.m_file_size;
        file_data.m_file_size = 0;
        m_creation_date_time = static_cast<DateTime64&&>(file_data.m_creation_date_time);
        m_last_accessed_date_time = static_cast<DateTime64&&>(file_data.m_last_accessed_date_time);
        m_last_modified_date_time = static_cast<DateTime64&&>(file_data.m_last_modified_date_time);
        m_is_directory = file_data.m_is_directory;
        file_data.m_is_directory = false;
    }

private:
    void Free() noexcept
    {
        string_copy_type::Free(m_filename, m_filename_length);
    }

    void CopyFilename(char_type const* filename, file_length_type filename_len = 0) noexcept
    {
        if (filename != nullptr)
        {
            if (filename_len == 0)
                filename_len = string_utility_type::GetLength(filename);
            string_copy_type::AllocCopy(m_filename, filename_len,
                filename, string_utility_type::UnsafeGetLength(filename));
            m_filename_length = filename_len;
        }
        else
            Free();
    }

private:
    char_type* m_filename;
    file_length_type m_filename_length;
    file_size_type m_file_size;
    ocl::DateTime64 m_creation_date_time;
    ocl::DateTime64 m_last_accessed_date_time;
    ocl::DateTime64 m_last_modified_date_time;
    bool m_is_directory;
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_FILEDATA_HPP
