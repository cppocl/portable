/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_CHARCONSTANTS_HPP
#define OCL_GUARD_PORTABLE_COMMON_CHARCONSTANTS_HPP

namespace ocl
{

template<typename CharType>
struct CharConstants
{
    typedef char char_type;

    static char_type const Null         = '\0';
    static char_type const BackSlash    = '\\';
    static char_type const ForwardSlash = '/';
    static char_type const FullStop     = '.';
    static char_type const QuestionMark = '?';
    static char_type const Asterisk     = '*';
    static char_type const Percent      = '%';
    static char_type const Colon        = ':';
    static char_type const SemiColon    = ';';
    static char_type const VerticalBar  = '|';
    static char_type const Del          = '\x7f';
};

template<>
struct CharConstants<wchar_t>
{
    typedef wchar_t char_type;

    static char_type const Null         = L'\0';
    static char_type const BackSlash    = L'\\';
    static char_type const ForwardSlash = L'/';
    static char_type const FullStop     = L'.';
    static char_type const QuestionMark = L'?';
    static char_type const Asterisk     = L'*';
    static char_type const Percent      = L'%';
    static char_type const Colon        = L':';
    static char_type const SemiColon    = L';';
    static char_type const VerticalBar  = L'|';
    static char_type const Del          = L'\x7f';
};

template<>
struct CharConstants<char16_t>
{
    typedef char16_t char_type;

    static char_type const Null         = u'\0';
    static char_type const BackSlash    = u'\\';
    static char_type const ForwardSlash = u'/';
    static char_type const FullStop     = u'.';
    static char_type const QuestionMark = u'?';
    static char_type const Asterisk     = u'*';
    static char_type const Percent      = u'%';
    static char_type const Colon        = u':';
    static char_type const SemiColon    = u';';
    static char_type const VerticalBar  = u'|';
    static char_type const Del          = u'\x7f';
};

template<>
struct CharConstants<char32_t>
{
    typedef char32_t char_type;

    static char_type const Null         = U'\0';
    static char_type const BackSlash    = U'\\';
    static char_type const ForwardSlash = U'/';
    static char_type const FullStop     = U'.';
    static char_type const QuestionMark = U'?';
    static char_type const Asterisk     = U'*';
    static char_type const Percent      = U'%';
    static char_type const Colon        = U':';
    static char_type const SemiColon    = U';';
    static char_type const VerticalBar  = U'|';
    static char_type const Del          = U'\x7f';
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_CHARCONSTANTS_HPP
