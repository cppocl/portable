/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_DATE32_HPP
#define OCL_GUARD_PORTABLE_COMMON_DATE32_HPP

#include <cstdint>
#include "BitUtility.hpp"

namespace ocl
{

/**
 * Store a date within a 32-bit value.
 * Use the interface to set or retrieve the parts of the date.
 */
class Date32
{
/// Types and constants.
public:
    /// Date types.
    typedef std::uint8_t  day_type;          /// range of 1..31.
    typedef std::uint8_t  month_type;        /// range of 1..12.
    typedef std::uint16_t year_type;         /// range of 1..65535.

    typedef std::uint32_t date_type;
    /// NOTE: These ranges match the cppocl Date32 project ranges.
    static day_type   const MIN_DAY   = 1U;
    static day_type   const MAX_DAY   = 31U;
    static month_type const MIN_MONTH = 1U;
    static month_type const MAX_MONTH = 12U;
    static year_type  const MIN_YEAR  = 1U;
    static year_type  const MAX_YEAR  = 65535U;

    static month_type const JANUARY   = 1;
    static month_type const FEBRUARY  = 2;
    static month_type const MARCH     = 3;
    static month_type const APRIL     = 4;
    static month_type const MAY       = 5;
    static month_type const JUNE      = 6;
    static month_type const JULY      = 7;
    static month_type const AUGUST    = 8;
    static month_type const SEPTEMBER = 9;
    static month_type const OCTOBER   = 10;
    static month_type const NOVERMBER = 11;
    static month_type const DECEMBER  = 12;

// Internal use bit masks.
private:
    static date_type const DAY_BITS   = static_cast<date_type>(0x000000ff); /// 8 bits.
    static date_type const MONTH_BITS = static_cast<date_type>(0x0000ff00); /// 8 bits.
    static date_type const YEAR_BITS  = static_cast<date_type>(0xffff0000); /// 16 bits.

/// Member functions.
public:
    /// Set date initially as unused.
    Date32() noexcept
        : m_date(0u)
    {
    }

    Date32(Date32 const& date) noexcept
        : m_date(date.m_date)
    {
    }

    Date32(Date32&& date) noexcept
        : m_date(date.m_date)
    {
        date.m_date = 0U;
    }

    Date32(day_type day, month_type month, year_type year) noexcept
        : m_date(0u)
    {
        SetDay(day);
        SetMonth(month);
        SetYear(year);
    }

    Date32& operator =(Date32 const& date) noexcept
    {
        Copy(date);
        return *this;
    }

    Date32& operator =(Date32&& date) noexcept
    {
        Move(static_cast<Date32&&>(date));
        return *this;
    }

    Date32& operator =(date_type date) noexcept
    {
        SetDate(date);
        return *this;
    }

    bool operator ==(Date32 const& date) const noexcept
    {
        return m_date == date.m_date;
    }

    bool operator !=(Date32 const& date) const noexcept
    {
        return m_date != date.m_date;
    }

    bool operator <(Date32 const& date) const noexcept
    {
        return m_date < date.m_date;
    }

    bool operator <=(Date32 const& date) const noexcept
    {
        return m_date <= date.m_date;
    }

    bool operator >(Date32 const& date) const noexcept
    {
        return m_date > date.m_date;
    }

    bool operator >=(Date32 const& date) const noexcept
    {
        return m_date >= date.m_date;
    }

    void Clear() noexcept
    {
        m_date = 0u;
    }

    /// Copy date from another Date32 object.
    void Copy(Date32 const& date) noexcept
    {
        m_date = date.m_date;
    }

    /// Move date from another Date32 object.
    void Move(Date32&& date) noexcept
    {
        m_date = date.m_date;
        date.m_date = 0U;
    }

    /// Get 32-bit date value.
    date_type GetDate() const noexcept
    {
        return m_date;
    }

    /// Set date from 32-bit value.
    void SetDate(date_type date) noexcept
    {
        m_date = date;
    }

    /// Get the day within the range of 1..31.
    day_type GetDay() const noexcept
    {
        return static_cast<day_type>(m_date & DAY_BITS);
    }

    /// Set day within range of 1..31.
    /// NOTE: Value of 0 means not used.
    /// NOTE: There is no validation of day with month.
    void SetDay(day_type day) noexcept
    {
        m_date = BitUtility::MaskSet<date_type, day_type>(m_date, DAY_BITS, day);
    }

    /// Get month within range of 1..12.
    month_type GetMonth() const noexcept
    {
        return BitUtility::MaskGet<date_type, month_type>(m_date, MONTH_BITS);
    }

    /// Set month within range of 1..12.
    /// NOTE: Value of 0 means not used.
    void SetMonth(month_type month) noexcept
    {
        m_date = BitUtility::MaskSet<date_type, month_type>(m_date, MONTH_BITS, month);
    }

    /// Get year within range of 1..65535.
    year_type GetYear() const noexcept
    {
        return BitUtility::MaskGet<date_type, year_type>(m_date, YEAR_BITS);
    }

    /// Set year within range of 1..65535.
    /// NOTE: Value of 0 means not used.
    void SetYear(year_type year) noexcept
    {
        m_date = BitUtility::MaskSet<date_type, year_type>(m_date, YEAR_BITS, year);
    }

/// Internal data.
private:
    /// Bit layout is first 8 bits for day, next 8 bits for month,
    /// last 16 bits for year.
    date_type m_date;
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_DATE32_HPP
