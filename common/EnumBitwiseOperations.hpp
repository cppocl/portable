/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_ENUMBITWISEOPERATIONS_HPP
#define OCL_GUARD_PORTABLE_COMMON_ENUMBITWISEOPERATIONS_HPP

#include <type_traits>

namespace ocl
{
    template<typename EnumType>
    constexpr inline EnumType EnumOr(EnumType a, EnumType b) noexcept
    {
        typedef typename std::underlying_type<EnumType>::type IntType;
        return static_cast<EnumType>(static_cast<IntType>(a) | static_cast<IntType>(b));
    }

    template<typename EnumType>
    constexpr inline EnumType EnumAnd(EnumType a, EnumType b) noexcept
    {
        typedef typename std::underlying_type<EnumType>::type IntType;
        return static_cast<EnumType>(static_cast<IntType>(a) & static_cast<IntType>(b));
    }

    template<typename EnumType>
    constexpr inline EnumType EnumXor(EnumType a, EnumType b) noexcept
    {
        typedef typename std::underlying_type<EnumType>::type IntType;
        return static_cast<EnumType>(static_cast<IntType>(a) ^ static_cast<IntType>(b));
    }

    template<typename EnumType>
    constexpr inline EnumType EnumNot(EnumType a) noexcept
    {
        typedef typename std::underlying_type<EnumType>::type IntType;
        return static_cast<EnumType>(~static_cast<IntType>(a));
    }

    template<typename EnumType>
    inline void EnumOrEq(EnumType& a, EnumType b) noexcept
    {
        typedef typename std::underlying_type<EnumType>::type IntType;
        a = static_cast<EnumType>(static_cast<IntType>(a) | static_cast<IntType>(b));
    }

    template<typename EnumType>
    inline void EnumAndEq(EnumType& a, EnumType b) noexcept
    {
        typedef typename std::underlying_type<EnumType>::type IntType;
        a = static_cast<EnumType>(static_cast<IntType>(a) & static_cast<IntType>(b));
    }

    template<typename EnumType>
    inline void EnumXorEq(EnumType& a, EnumType b) noexcept
    {
        typedef typename std::underlying_type<EnumType>::type IntType;
        a = static_cast<EnumType>(static_cast<IntType>(a) ^ static_cast<IntType>(b));
    }

    template<typename EnumType>
    inline void EnumNotEq(EnumType& a) noexcept
    {
        typedef typename std::underlying_type<EnumType>::type IntType;
        a = static_cast<EnumType>(~static_cast<IntType>(a));
    }

    template<typename EnumType>
    constexpr EnumType operator |(EnumType a, EnumType b) noexcept
    {
        return EnumOr(a, b);
    }

    template<typename EnumType>
    constexpr EnumType& operator |=(EnumType& a, EnumType b) noexcept
    {
        EnumOrEq(a, b);
        return a;
    }

    template<typename EnumType>
    constexpr EnumType operator &(EnumType a, EnumType b) noexcept
    {
        return EnumAnd(a, b);
    }

    template<typename EnumType>
    constexpr EnumType& operator &=(EnumType& a, EnumType b) noexcept
    {
        EnumAndEq(a, b);
        return a;
    }

    template<typename EnumType>
    constexpr EnumType operator ^(EnumType a, EnumType b) noexcept
    {
        return EnumXor(a, b);
    }

    template<typename EnumType>
    constexpr EnumType& operator ^=(EnumType& a, EnumType b) noexcept
    {
        EnumXorEq(a, b);
        return a;
    }

    template<typename EnumType>
    constexpr EnumType operator ~(EnumType a) noexcept
    {
        return EnumNot(a);
    }

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_ENUMBITWISEOPERATIONS_HPP
