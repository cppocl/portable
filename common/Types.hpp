/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_TYPES_HPP
#define OCL_GUARD_PORTABLE_COMMON_TYPES_HPP

#include <limits>

#ifdef MIN
#error Cannot use Types.hpp when MIN is defined.
#endif

#ifdef MAX
#error Cannot use Types.hpp when MAX is defined.
#endif

namespace ocl
{

template<typename Type>
struct Types;

template<>
struct Types<signed char>
{
// Type defines.
    typedef signed char type;
    typedef type diff_type;

// Constants.
    static constexpr type ZERO      = 0;
    static constexpr type ONE       = 1;
    static constexpr type MINUS_ONE = static_cast<signed char>(-1);
    static constexpr type MIN       = SCHAR_MIN;
    static constexpr type MAX       = SCHAR_MAX;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value < ZERO ? value * MINUS_ONE : value;
    }
};

template<>
struct Types<unsigned char>
{
// Type defines.
    typedef unsigned char type;
    typedef signed char diff_type;

// Constants.
    static constexpr type ZERO = 0;
    static constexpr type ONE  = 1;
    static constexpr type MIN  = 0U;
    static constexpr type MAX  = UCHAR_MAX;

    static constexpr type Zero() noexcept { return ZERO; }
    static constexpr type One() noexcept  { return ONE; }
    static constexpr type Min() noexcept  { return MIN; }
    static constexpr type Max() noexcept  { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value;
    }
};

template<>
struct Types<signed short>
{
// Type defines.
    typedef signed short type;
    typedef type diff_type;

// Constants.
    static constexpr type ZERO      = 0;
    static constexpr type ONE       = 1;
    static constexpr type MINUS_ONE = -1;
    static constexpr type MIN       = SHRT_MIN;
    static constexpr type MAX       = SHRT_MAX;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value < 0 ? value * MINUS_ONE : value;
    }
};

template<>
struct Types<unsigned short>
{
// Type defines.
    typedef unsigned short type;
    typedef signed short diff_type;

// Constants.
    static constexpr type ZERO = 0U;
    static constexpr type ONE  = 1U;
    static constexpr type MIN  = 0U;
    static constexpr type MAX  = USHRT_MAX;

    static constexpr type Zero() noexcept { return ZERO; }
    static constexpr type One() noexcept  { return ONE; }
    static constexpr type Min() noexcept  { return MIN; }
    static constexpr type Max() noexcept  { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value;
    }
};

template<>
struct Types<signed int>
{
// Type defines.
    typedef signed int type;
    typedef type diff_type;

// Constants.
    static constexpr type ZERO      = 0;
    static constexpr type ONE       = 1;
    static constexpr type MINUS_ONE = -1;
    static constexpr type MIN       = INT_MIN;
    static constexpr type MAX       = INT_MAX;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value < 0 ? value * MINUS_ONE : value;
    }
};

template<>
struct Types<unsigned int>
{
// Type defines.
    typedef unsigned int type;
    typedef signed int diff_type;

// Constants.
    static constexpr type ZERO = 0U;
    static constexpr type ONE  = 1U;
    static constexpr type MIN  = 0U;
    static constexpr type MAX  = UINT_MAX;

    static constexpr type Zero() noexcept { return ZERO; }
    static constexpr type One() noexcept  { return ONE; }
    static constexpr type Min() noexcept  { return MIN; }
    static constexpr type Max() noexcept  { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value;
    }
};

template<>
struct Types<signed long>
{
// Type defines.
    typedef signed long type;
    typedef type diff_type;

// Constants.
    static constexpr type ZERO      = 0L;
    static constexpr type ONE       = 1L;
    static constexpr type MINUS_ONE = -1L;
    static constexpr type MIN       = LONG_MIN;
    static constexpr type MAX       = LONG_MAX;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value < 0 ? value * MINUS_ONE : value;
    }
};

template<>
struct Types<unsigned long>
{
// Type defines.
    typedef unsigned long type;
    typedef signed long diff_type;

// Constants.
    static constexpr type ZERO = 0UL;
    static constexpr type ONE  = 1UL;
    static constexpr type MIN  = 0UL;
    static constexpr type MAX  = ULONG_MAX;

    static constexpr type Zero() noexcept { return ZERO; }
    static constexpr type One() noexcept  { return ONE; }
    static constexpr type Min() noexcept  { return MIN; }
    static constexpr type Max() noexcept  { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value;
    }
};

template<>
struct Types<signed long long>
{
// Type defines.
    typedef signed long long type;
    typedef type diff_type;

// Constants.
    static constexpr type ZERO      = 0LL;
    static constexpr type ONE       = 1LL;
    static constexpr type MINUS_ONE = -1LL;
    static constexpr type MIN       = LLONG_MIN;
    static constexpr type MAX       = LLONG_MAX;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value < 0 ? value * MINUS_ONE : value;
    }
};

template<>
struct Types<unsigned long long>
{
// Type defines.
    typedef unsigned long long type;
    typedef signed long long diff_type;

// Constants.
    static constexpr type ZERO = 0ULL;
    static constexpr type ONE  = 1ULL;
    static constexpr type MIN  = 0ULL;
    static constexpr type MAX  = ULLONG_MAX;

    static constexpr type Zero() noexcept { return ZERO; }
    static constexpr type One() noexcept  { return ONE; }
    static constexpr type Min() noexcept  { return MIN; }
    static constexpr type Max() noexcept  { return MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value;
    }
};

template<>
struct Types<float>
{
// Type defines.
    typedef float type;
    typedef type diff_type;

// Constants.
    static constexpr type Zero() noexcept     { return 0.0f; }
    static constexpr type One() noexcept      { return 1.0f; }
    static constexpr type MinusOne() noexcept { return -1.0f; }
    static constexpr type Min() noexcept      { return FLT_MAX * -1.0f; }
    static constexpr type Max() noexcept      { return FLT_MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value < 0 ? value * MinusOne() : value;
    }
};

template<>
struct Types<double>
{
// Type defines.
    typedef double type;
    typedef type diff_type;

// Constants.
    static constexpr type Zero() noexcept     { return 0.0; }
    static constexpr type One() noexcept      { return 1.0; }
    static constexpr type MinusOne() noexcept { return -1.0; }
    static constexpr type Min() noexcept      { return DBL_MAX * -1.0; }
    static constexpr type Max() noexcept      { return DBL_MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value < 0 ? value * MinusOne() : value;
    }
};

template<>
struct Types<long double>
{
// Type defines.
    typedef long double type;
    typedef type diff_type;

// Constants.
    static constexpr type Zero() noexcept     { return 0.0; }
    static constexpr type One() noexcept      { return 1.0; }
    static constexpr type MinusOne() noexcept { return -1.0; }
    static constexpr type Min() noexcept      { return LDBL_MAX * -1.0; }
    static constexpr type Max() noexcept      { return LDBL_MAX; }

// Helper functions for types.
    static type Abs(type value) noexcept
    {
        return value < 0 ? value * MinusOne() : value;
    }
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_TYPES_HPP
