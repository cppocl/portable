/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_STRINGUTILITY_HPP
#define OCL_GUARD_PORTABLE_COMMON_STRINGUTILITY_HPP

#include "CharConstants.hpp"
#include <cstring>
#include <cstddef>

namespace ocl
{

template<typename CharType, typename SizeType = std::size_t>
class StringUtility
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;

    static char_type const NullChar = CharConstants<char_type>::Null;

public:
    /// Get the length of the string or return 0 if string is null.
    static size_type GetLength(char_type const* str) noexcept
    {
        size_type len = 0;
        if (str != nullptr)
            for (; *str != NullChar; ++str)
                ++len;
        return len;
    }

    /// Get the length of the 2 combined strings, and any null string will be treated as length of 0.
    static size_type GetLength(char_type const* str1, char_type const* str2) noexcept
    {
        return GetLength(str1) + GetLength(str2);
    }

    /// Get the length of the 3 combined strings, and any null string will be treated as length of 0.
    static size_type GetLength(char_type const* str1,
                               char_type const* str2,
                               char_type const* str3) noexcept
    {
        return GetLength(str1) + GetLength(str2) + GetLength(str3);
    }

    /// Get the length of the string.
    static size_type UnsafeGetLength(char_type const* str) noexcept
    {
        size_type len = 0;
        for (; *str != NullChar; ++str)
            ++len;
        return len;
    }

    /// Get the length of the 2 combined strings.
    static size_type UnsafeGetLength(char_type const* str1, char_type const* str2) noexcept
    {
        return UnsafeGetLength(str1) + UnsafeGetLength(str2);
    }

    /// Get the length of the 3 combined strings.
    static size_type UnsafeGetLength(char_type const* str1,
                                     char_type const* str2,
                                     char_type const* str3) noexcept
    {
        return UnsafeGetLength(str1) + UnsafeGetLength(str2) + UnsafeGetLength(str3);
    }

    /// Get the position of the character at the end of the string for a match,
    /// otherwise return the length of the string for no match.
    static size_type EndsWith(char_type const* str, char_type char_to_match) noexcept
    {
        return str != nullptr ? UnsafeEndsWith(str, char_to_match) : 0;
    }

    /// Get the position of the character at the end of the string for a match,
    /// otherwise return the length of the string for no match.
    static size_type EndsWith(char_type const* str,
                              size_type& str_len,
                              char_type char_to_match) noexcept
    {
        if (str != nullptr)
            return UnsafeEndsWith(str, str_len, char_to_match);
        str_len = 0;
        return 0;
    }

    /// Get the position of the character at the end of the string for a match,
    /// otherwise return the length of the string for no match.
    static size_type UnsafeEndsWith(char_type const* str, char_type char_to_match) noexcept
    {
        size_type pos = UnsafeGetLength(str);
        if (pos > 0 && str[pos - 1] == char_to_match)
            --pos;
        return pos;
    }

    /// Get the position of the character at the end of the string for a match,
    /// otherwise return the length of the string for no match.
    static size_type UnsafeEndsWith(char_type const* str,
                                    size_type& str_len,
                                    char_type char_to_match) noexcept
    {
        size_type pos = UnsafeGetLength(str);
        str_len = pos;
        if (pos > 0 && str[pos - 1] == char_to_match)
            --pos;
        return pos;
    }

    /// Compare two string, and return true if they match.
    /// If one of the two strings is null then false is returned.
    /// If most strings are null then true is returned.
    static bool Compare(char_type const* str1, char_type const* str2) noexcept
    {
        if (str1 != nullptr)
        {
            if (str2 != nullptr)
            {
                while (*str1 == *str2)
                {
                    if (*str1 == NullChar)
                        return true;
                    ++str1;
                    ++str2;
                }
            }
        }
        else
            return str2 == nullptr;
        return false;
    }

    /// Compare two strings and return true if they match.
    static bool UnsafeCompare(char_type const* str1, char_type const* str2) noexcept
    {
        while (*str1 == *str2)
        {
            if (*str1 == NullChar)
                return true;
            ++str1;
            ++str2;
        }
        return false;
    }

    /// Compare two strings and return true if they match.
    static bool UnsafeCompare(char_type const* str1, char_type const* str2, size_type count) noexcept
    {
        char_type const* str1_end = str1 + count;
        while (str1 < str1_end)
        {
            if (*str1 != *str2)
                return false;
            ++str1;
            ++str2;
        }
        return true;
    }

    /// Copy str1 to dest.
    static void UnsafeCopy(char_type* dest, char_type const* str, size_type count) noexcept
    {
        ::memcpy(dest, str, (count + 1) * sizeof(char_type));
    }

    /// Copy str1 to dest then append str2 to dest afterwards.
    static void UnsafeCopy(char_type* dest,
                           char_type const* str1, size_type count1,
                           char_type const* str2, size_type count2) noexcept
    {
        ::memcpy(dest, str1, count1 * sizeof(char_type));
        ::memcpy(dest + count1, str2, (count2 + 1) * sizeof(char_type));
    }

    /// Copy str1 to dest then append str2 and str3 to dest afterwards.
    static void UnsafeCopy(char_type* dest,
                           char_type const* str1, size_type count1,
                           char_type const* str2, size_type count2,
                           char_type const* str3, size_type count3) noexcept
    {
        ::memcpy(dest, str1, count1 * sizeof(char_type));
        ::memcpy(dest + count1, str2, count2 * sizeof(char_type));
        ::memcpy(dest + count1 + count2, str3, (count3 + 1) * sizeof(char_type));
    }

    /// Return the position of the matching character or length if the value is not found.
    static size_type Find(char_type const* str, char_type char_to_find) noexcept
    {
        return str != nullptr ? UnsafeFind(str, char_to_find) : 0;
    }

    /// Return the position of the matching character or length if the value is not found.
    static size_type Find(char_type const* str, size_type length, char_type char_to_find) noexcept
    {
        return str != nullptr ? UnsafeFind(str, length, char_to_find) : 0;
    }

    /// Return the position of the matching character or length if the value is not found.
    static size_type UnsafeFind(char_type const* str, char_type char_to_find) noexcept
    {
        char_type const* str_start = str;
        while (*str != char_to_find && *str != NullChar)
            ++str;
        return str - str_start;
    }

    /// Return the position of the matching character or length if the value is not found.
    static size_type UnsafeFind(char_type const* str, size_type length, char_type char_to_find) noexcept
    {
        char_type const* str_start = str;
        char_type const* str_end = str + length;
        while (str < str_end && *str != char_to_find)
            ++str;
        return str - str_start;
    }
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_STRINGUTILITY_HPP
