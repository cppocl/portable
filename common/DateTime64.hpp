/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_DATETIME64_HPP
#define OCL_GUARD_PORTABLE_COMMON_DATETIME64_HPP

#include <cstdint>
#include "Date32.hpp"
#include "Time32.hpp"

namespace ocl
{

/**
 * Store a date and time within a 64-bit value (date in 32-bit and time in 32-bit).
 * Use the interface to set or retrieve the parts of the date or time.
 */
class DateTime64
{
/// Types and constants.
public:
    /// Date types.
    typedef Date32::day_type   day_type;   /// range of 1..31.
    typedef Date32::month_type month_type; /// range of 1..12.
    typedef Date32::year_type  year_type;  /// range of 1..65535.

    /// Time types.
    typedef Time32::milliseconds_type milliseconds_type; /// range of 0..999.
    typedef Time32::seconds_type      seconds_type;      /// range of 0..59.
    typedef Time32::minutes_type      minutes_type;      /// range of 0..59.
    typedef Time32::hours_type        hours_type;        /// range of 0..23.

    typedef Date32::date_type date_type;
    typedef Time32::time_type time_type;
    typedef std::uint64_t date_time_type;

    /// NOTE: These ranges match the cppocl DateTime project ranges.
    static day_type   const MIN_DAY   = Date32::MIN_DAY;
    static day_type   const MAX_DAY   = Date32::MAX_DAY;
    static month_type const MIN_MONTH = Date32::MIN_MONTH;
    static month_type const MAX_MONTH = Date32::MAX_MONTH;
    static year_type  const MIN_YEAR  = Date32::MIN_YEAR;
    static year_type  const MAX_YEAR  = Date32::MAX_YEAR;

    static month_type const JANUARY   = Date32::JANUARY;
    static month_type const FEBRUARY  = Date32::FEBRUARY;
    static month_type const MARCH     = Date32::MARCH;
    static month_type const APRIL     = Date32::APRIL;
    static month_type const MAY       = Date32::MAY;
    static month_type const JUNE      = Date32::JUNE;
    static month_type const JULY      = Date32::JULY;
    static month_type const AUGUST    = Date32::AUGUST;
    static month_type const SEPTEMBER = Date32::SEPTEMBER;
    static month_type const OCTOBER   = Date32::OCTOBER;
    static month_type const NOVERMBER = Date32::NOVERMBER;
    static month_type const DECEMBER  = Date32::DECEMBER;

    static milliseconds_type const MIN_MILLISECONDS = Time32::MIN_MILLISECONDS;
    static milliseconds_type const MAX_MILLISECONDS = Time32::MAX_MILLISECONDS;
    static seconds_type      const MIN_SECONDS      = Time32::MIN_SECONDS;
    static seconds_type      const MAX_SECONDS      = Time32::MAX_SECONDS;
    static minutes_type      const MIN_MINUTES      = Time32::MIN_MINUTES;
    static minutes_type      const MAX_MINUTES      = Time32::MAX_MINUTES;
    static hours_type        const MIN_HOURS        = Time32::MIN_HOURS;
    static hours_type        const MAX_HOURS        = Time32::MAX_HOURS;

/// Member functions.
public:
    /// Set date and time as initially as unused.
    DateTime64() noexcept
    {
        static_assert(sizeof(Date32::date_type) == 4U, "Date must be 32-bit type!");
        static_assert(sizeof(Time32::time_type) == 4U, "Time must be 32-bit type!");
    }

    DateTime64(DateTime64 const& date_time) noexcept
        : m_date(date_time.m_date)
        , m_time(date_time.m_time)
    {
    }

    DateTime64(DateTime64&& date_time) noexcept
        : m_date(static_cast<Date32&&>(date_time.m_date))
        , m_time(static_cast<Time32&&>(date_time.m_time))
    {
    }

    DateTime64(day_type day,
               month_type month,
               year_type year,
               milliseconds_type milliseconds,
               seconds_type seconds,
               minutes_type minutes,
               hours_type hours) noexcept
        : m_date(day, month, year)
        , m_time(milliseconds, seconds, minutes, hours)
    {
    }

    operator Date32 const&() const noexcept
    {
        return m_date;
    }

    operator Date32&() noexcept
    {
        return m_date;
    }

    operator Time32 const&() const noexcept
    {
        return m_time;
    }

    operator Time32&() noexcept
    {
        return m_time;
    }

    /// Get 64-bit date/time value.
    operator date_time_type() const noexcept
    {
        return GetDateTime();
    }

    DateTime64& operator =(DateTime64 const& date_time) noexcept
    {
        Copy(date_time);
        return *this;
    }

    DateTime64& operator =(DateTime64&& date_time) noexcept
    {
        Move(static_cast<DateTime64&&>(date_time));
        return *this;
    }

    DateTime64 operator =(date_time_type date_time) noexcept
    {
        SetDateTime(date_time);
        return *this;
    }

    bool operator ==(DateTime64 const& date_time) const noexcept
    {
        return m_date == date_time.m_date && m_time == date_time.m_time;
    }

    bool operator !=(DateTime64 const& date_time) const noexcept
    {
        return m_date != date_time.m_date || m_time != date_time.m_time;
    }

    bool operator <(DateTime64 const& date_time) const noexcept
    {
        return m_date < date_time.m_date || (m_date == date_time.m_date && m_time < date_time.m_time);
    }

    bool operator <=(DateTime64 const& date_time) const noexcept
    {
        return m_date < date_time.m_date || (m_date == date_time.m_date && m_time <= date_time.m_time);
    }

    bool operator >(DateTime64 const& date_time) const noexcept
    {
        return m_date > date_time.m_date || (m_date == date_time.m_date && m_time > date_time.m_time);
    }

    bool operator >=(DateTime64 const& date_time) const noexcept
    {
        return m_date > date_time.m_date || (m_date == date_time.m_date && m_time >= date_time.m_time);
    }

    void Clear() noexcept
    {
        m_date.Clear();
        m_time.Clear();
    }

    /// Copy date and time from another DateTime64 object.
    void Copy(DateTime64 const& date_time) noexcept
    {
        m_date = date_time.m_date;
        m_time = date_time.m_time;
    }

    /// Move date and time from another DateTime64 object.
    void Move(DateTime64&& date_time) noexcept
    {
        m_date.Move(static_cast<Date32&&>(date_time.m_date));
        m_time.Move(static_cast<Time32&&>(date_time.m_time));
    }

    /// Get 64-bit value containing date and time.
    /// Date is in low 32 bits and time is in high 32 bits.
    date_time_type GetDateTime() const noexcept
    {
        return BitUtility::Combine<date_type, time_type, date_time_type>(
            m_date.GetDate(), m_time.GetTime());
    }

    /// Set date and time from a 64 bit value.
    /// Date is in low 32 bits and time is in high 32 bits.
    void SetDateTime(date_time_type date_time) noexcept
    {
        date_type date;
        time_type time;
        BitUtility::Split<date_type, time_type, date_time_type>(date, time, date_time);
        m_date.SetDate(date);
        m_time.SetTime(time);
    }

    /// Get 32-bit date value.
    Date32 const& GetDate() const noexcept
    {
        return m_date;
    }

    /// Get 32-bit time value.
    Time32 const& GetTime() const noexcept
    {
        return m_time;
    }

    /// Get the day within the range of 1..31.
    day_type GetDay() const noexcept
    {
        return m_date.GetDay();
    }

    /// Set day within range of 1..31.
    /// NOTE: Value of 0 means not used.
    /// NOTE: There is no validation of day with month.
    void SetDay(day_type day) noexcept
    {
        m_date.SetDay(day);
    }

    /// Get month within range of 1..12.
    month_type GetMonth() const noexcept
    {
        return m_date.GetMonth();
    }

    /// Set month within range of 1..12.
    /// NOTE: Value of 0 means not used.
    void SetMonth(month_type month) noexcept
    {
        m_date.SetMonth(month);
    }

    /// Get year within range of 1..65535.
    year_type GetYear() const noexcept
    {
        return m_date.GetYear();
    }

    /// Set year within range of 1..65535.
    /// NOTE: Value of 0 means not used.
    void SetYear(year_type year) noexcept
    {
        m_date.SetYear(year);
    }

    /// Get milliseconds within range of 0..999.
    milliseconds_type GetMilliseconds() const noexcept
    {
        return m_time.GetMilliseconds();
    }

    /// Set milliseconds within range of 0..999.
    void SetMilliseconds(milliseconds_type milliseconds)
    {
        m_time.SetMilliseconds(milliseconds);
    }

    /// Get seconds within range of 0..59.
    seconds_type GetSeconds() const noexcept
    {
        return m_time.GetSeconds();
    }

    /// Set seconds within range of 0..59.
    void SetSeconds(seconds_type seconds) noexcept
    {
        m_time.SetSeconds(seconds);
    }

    /// Get minutes within range of 0..59.
    minutes_type GetMinutes() const noexcept
    {
        return m_time.GetMinutes();
    }

    /// Set minutes within range of 0..59.
    void SetMinutes(minutes_type minutes) noexcept
    {
        m_time.SetMinutes(minutes);
    }

    /// Get hours within range of 0..23.
    hours_type GetHours() const noexcept
    {
        return m_time.GetHours();
    }

    /// Set hours within range of 0..23.
    void SetHours(hours_type hours) noexcept
    {
        m_time.SetHours(hours);
    }

/// Internal data.
private:
    /// Bit layout is first 8 bits for day, next 8 bits for month,
    /// last 16 bits for year.
    Date32 m_date;

    /// Bit layout is first 10 bits for millisecond,
    /// next 6 bits seconds,
    /// then 6 bits for minutes and
    /// finally 5 bits for hours.
    Time32 m_time;
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_DATETIME64_HPP
