/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_BITUTILITY_HPP
#define OCL_GUARD_PORTABLE_COMMON_BITUTILITY_HPP

#include <cstddef>
#include <climits>

namespace ocl
{

struct BitUtility
{
    /// Get the number of bits for a type.
    template<typename T, typename SizeT>
    static constexpr SizeT const BitCount() noexcept
    {
        return static_cast<SizeT>(sizeof(T) * static_cast<std::size_t>(CHAR_BIT));
    }

    /// Combine two values into one type, shifting b left by bit size of a and OR b with a.
    template<typename T1, typename T2, typename T3>
    static constexpr T3 Combine(T1 a, T2 b)
    {
        static_assert(sizeof(T1) + sizeof(T2) <= sizeof(T3),
                      "Two values for T1 and T2 combined won't fit T3.");

        return static_cast<T3>(a) | (static_cast<T3>(b) << BitCount<T2, T3>());
    }

    template<typename T1, typename T2, typename T3>
    static void Split(T1& low, T2& high, T3 value)
    {
        static_assert(sizeof(T1) + sizeof(T2) <= sizeof(T3),
                      "Two values for T1 and T2 combined too small to split from T3.");

        low = static_cast<T1>(static_cast<T3>(~static_cast<T1>(0)) & value);
        high = static_cast<T2>(((static_cast<T3>(~static_cast<T2>(0)) << BitCount<T2, T3>()) & value) >> BitCount<T2, T3>());
    }

    /// Get the 0 based index of the first set bit.
    template<typename T>
    static constexpr T Pos(T value, T offset = 0) noexcept
    {
        return (value & static_cast<T>(1)) ? offset : Pos<T>(value >> static_cast<T>(1), ++offset);
    }

    /// Get the value starting from the first bit set.
    /// Preceding zeros will be removed.
    template<typename T>
    static constexpr T Start(T value) noexcept
    {
        return (value & static_cast<T>(1)) ? value : Start(value >> static_cast<T>(1));
    }

    /// Count all continuous bits set.
    template<typename T>
    static constexpr T Count(T value, T total = 0) noexcept
    {
        return (value & static_cast<T>(1)) ? Count(value >> static_cast<T>(1), ++total) : total;
    }

    /// Get the value within the range of the mask, shifted right by number of bits to first mask bit.
    /// E.g. MaskGet<std::uint16_t>(0xf000, 0xff00) == 0xf0
    template<typename TMask, typename T>
    static constexpr T MaskGet(TMask value, TMask mask) noexcept
    {
        return static_cast<T>((value & mask) >> Pos<TMask>(mask, static_cast<TMask>(0)));
    }

    /// Provide value and a mask, and set the sub-value bits within the value,
    /// shifted to fit within the mask position.
    /// E.g. MaskSet<std::uint16_t>(0x0f00, 0xff00, 0xf0) == 0xf000
    template<typename TMask, typename T>
    static constexpr TMask MaskSet(TMask value, TMask mask, T sub_value) noexcept
    {
        return (value & ((~static_cast<TMask>(0)) ^ mask)) |
               (static_cast<TMask>(sub_value) << Pos<TMask>(mask, static_cast<TMask>(0)));
    }
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_BITUTILITY_HPP
