/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_MEMORY_HPP
#define OCL_GUARD_PORTABLE_COMMON_MEMORY_HPP

#include <cstddef>
#include <cstdlib>

namespace ocl
{

template<typename Type, typename SizeType = std::size_t, bool const CanThrow = false>
class MemoryUtility
{
public:
    typedef Type type;
    typedef SizeType size_type;

    static bool const can_throw = false;

public:
    /// Allocate memory to store enough characters + an extra character for '\0'.
    static type* Allocate(size_type count) noexcept
    {
        return static_cast<type*>(::malloc(count * sizeof(type)));
    }

    static void Free(type*& ptr) noexcept
    {
        ::free(ptr);
        ptr = nullptr;
    }

    static void FastFree(type* ptr) noexcept
    {
        ::free(ptr);
    }
};

template<typename Type, typename SizeType>
class MemoryUtility<Type, SizeType, true>
{
public:
    typedef Type type;
    typedef SizeType size_type;

    static bool const can_throw = true;

public:
    /// Allocate memory to store enough characters + an extra character for '\0'.
    static type* Allocate(size_type count)
    {
        return new type[count];
    }

    static void Free(type*& ptr)
    {
        delete[] ptr;
        ptr = nullptr;
    }

    static void FastFree(type* ptr)
    {
        delete[] ptr;
    }
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_MEMORY_HPP
