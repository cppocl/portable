/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_STRINGCOPY_HPP
#define OCL_GUARD_PORTABLE_COMMON_STRINGCOPY_HPP

#include "MemoryUtility.hpp"
#include "StringUtility.hpp"
#include <cstddef>

namespace ocl
{

/// Functions to allocate memory and copy strings.
template< typename CharType,
          typename SizeType = std::size_t,
          bool const CanThrow = false,
          typename AllocType = MemoryUtility<CharType, SizeType, CanThrow> >
class StringCopy
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef AllocType alloc_type;
    typedef StringUtility<char_type, size_type> string_utility_type;

    static bool const can_throw = false;

public:
    /// Free memory for dest, set to null and set length to 0.
    static void Free(char_type*& dest) noexcept
    {
        alloc_type::Free(dest);
    }

    /// Free memory for dest, set to null and set length to 0.
    static void Free(char_type*& dest, size_type& length) noexcept
    {
        alloc_type::Free(dest);
        length = 0;
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void AllocCopy(char_type*& dest, char_type const* source) noexcept
    {
        if (source != nullptr)
        {
            size_type source_len = string_utility_type::UnsafeGetLength(source);
            if (Realloc(dest, source_len))
                string_utility_type::UnsafeCopy(dest, source, source_len);
        }
        else
            Free(dest);
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void UnsafeAllocCopy(char_type*& dest, char_type const* source) noexcept
    {
        size_type source_len = string_utility_type::UnsafeGetLength(source);
        if (Realloc(dest, source_len))
            string_utility_type::UnsafeCopy(dest, source, source_len);
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void AllocCopy(char_type*& dest, size_type& dest_length,
                          char_type const* source, size_type source_length) noexcept
    {
        if (source != nullptr && Realloc(dest, dest_length, source_length))
            string_utility_type::UnsafeCopy(dest, source, source_length);
        else
            Free(dest, dest_length);
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void AllocCopy(char_type*& dest, size_type& dest_length,
                          char_type const* source1, size_type source_length1,
                          char_type const* source2, size_type source_length2) noexcept
    {
        if (source1 != nullptr && source2 != nullptr &&
            Realloc(dest, dest_length, source_length1 + source_length2))
        {
            string_utility_type::UnsafeCopy(dest, source1, source_length1, source2, source_length2);
        }
        else
            Free(dest, dest_length);
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void AllocCopy(char_type*& dest, size_type& dest_length,
                          char_type const* source1, size_type source_length1,
                          char_type const* source2, size_type source_length2,
                          char_type const* source3, size_type source_length3) noexcept
    {
        if (source1 != nullptr && source2 != nullptr && source3 != nullptr &&
            Realloc(dest, dest_length, source_length1 + source_length2 + source_length3))
        {
            string_utility_type::UnsafeCopy(dest, source1, source_length1, source2, source_length2, source3, source_length3);
        }
        else
            Free(dest, dest_length);
    }

private:
    static bool Realloc(char_type*& dest, size_type new_length) noexcept
    {
        alloc_type::FastFree(dest);
        dest = (new_length > 0) ? alloc_type::Allocate(new_length + 1) : nullptr;
        return dest != nullptr;
    }

    static bool Realloc(char_type*& dest, size_type& dest_length, size_type new_length) noexcept
    {
        alloc_type::FastFree(dest);
        dest = (new_length > 0) ? alloc_type::Allocate(new_length + 1) : nullptr;
        dest_length = new_length;
        return dest != nullptr;
    }
};

/// Specialized for memory management with exceptions.
template<typename CharType, typename SizeType, typename AllocType>
class StringCopy<CharType, SizeType, true, AllocType>
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef AllocType alloc_type;
    typedef StringUtility<char_type, size_type> string_utility_type;

    static bool const can_throw = true;

public:
    /// Free memory for dest, set to null and set length to 0.
    static void Free(char_type*& dest)
    {
        typedef AllocType alloc_type;
        alloc_type::Free(dest);
    }

    /// Free memory for dest, set to null and set length to 0.
    static void Free(char_type*& dest, size_type& length)
    {
        typedef AllocType alloc_type;
        alloc_type::Free(dest);
        length = 0;
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void AllocCopy(char_type*& dest, char_type const* source)
    {
        if (source != nullptr)
        {
            size_type source_len = string_utility_type::UnsafeGetLength(source);
            if (Realloc(dest, source_len))
                string_utility_type::UnsafeCopy(dest, source, source_len);
        }
        else
            Free(dest);
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void UnsafeAllocCopy(char_type*& dest, char_type const* source)
    {
        size_type source_len = string_utility_type::UnsafeGetLength(source);
        if (Realloc(dest, source_len))
            string_utility_type::UnsafeCopy(dest, source, source_len);
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void AllocCopy(char_type*& dest, size_type& dest_length,
                          char_type const* source, size_type source_length)
    {
        if (source != nullptr && Realloc(dest, dest_length, source_length))
            string_utility_type::UnsafeCopy(dest, source, source_length);
        else
            Free(dest, dest_length);
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void AllocCopy(char_type*& dest, size_type& dest_length,
                          char_type const* source1, size_type source_length1,
                          char_type const* source2, size_type source_length2)
    {
        if (source1 != nullptr && source2 != nullptr &&
            Realloc(dest, dest_length, source_length1 + source_length2))
        {
            string_utility_type::UnsafeCopy(dest, source1, source_length1, source2, source_length2);
        }
        else
            Free(dest, dest_length);
    }

    /// Copy string from source to destination, freeing any existing memory allocated for destination
    /// before allocating a new string and copying.
    /// NOTE: AllocType interface must match MemoryUtility interface.
    static void AllocCopy(char_type*& dest, size_type& dest_length,
                          char_type const* source1, size_type source_length1,
                          char_type const* source2, size_type source_length2,
                          char_type const* source3, size_type source_length3)
    {
        if (source1 != nullptr && source2 != nullptr && source3 != nullptr &&
            Realloc(dest, dest_length, source_length1 + source_length2 + source_length3))
        {
            string_utility_type::UnsafeCopy(dest, source1, source_length1, source2, source_length2, source3, source_length3);
        }
        else
            Free(dest, dest_length);
    }

private:
    static bool Realloc(char_type*& dest, size_type new_length)
    {
        alloc_type::FastFree(dest);
        dest = (new_length > 0) ? alloc_type::Allocate(new_length + 1) : nullptr;
        return dest != nullptr;
    }

    static bool Realloc(char_type*& dest, size_type& dest_length, size_type new_length)
    {
        alloc_type::FastFree(dest);
        dest = (new_length > 0) ? alloc_type::Allocate(new_length + 1) : nullptr;
        dest_length = new_length;
        return dest != nullptr;
    }
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_STRINGCOPY_HPP
