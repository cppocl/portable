/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_PLATFORMDEFINES_HPP
#define OCL_GUARD_PORTABLE_COMMON_PLATFORMDEFINES_HPP

#include "../../compiler/compiler.hpp"
#include "../../platform/platform.hpp"

// Set OCL_PORTABLE_PLATFORM define for common portable library.

#ifndef OCL_PLATFORM
#error Platform not defined!
#endif

#ifndef OCL_COMPILER
#error Compiler not defined!
#endif

#ifndef OCL_COMPILER_POSIX_SUPPORT
#error Posix support not defined!
#endif

#endif // OCL_GUARD_PORTABLE_COMMON_PLATFORMDEFINES_HPP
