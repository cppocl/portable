/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_PATHUTILITY_HPP
#define OCL_GUARD_PORTABLE_COMMON_PATHUTILITY_HPP

#include "CharConstants.hpp"
#include "StringUtility.hpp"
#include "StringCopy.hpp"
#include "MemoryUtility.hpp"

namespace ocl
{

template< typename CharType,
          typename SizeType = std::size_t,
          bool const CanThrow = false,
          typename AllocType = MemoryUtility<CharType, SizeType, CanThrow> >
class PathUtility
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef AllocType alloc_type;
    typedef StringUtility<char_type, SizeType> string_utility_type;
    typedef StringCopy<char_type, SizeType, CanThrow> string_copy_type;

    static bool const can_throw = false;

    static char_type const NullChar = CharConstants<char_type>::Null;

public:
    static void Free(char_type*& path) noexcept
    {
        string_copy_type::Free(path);
    }

    static void CombinePath(char_type*& combined_path,
                            char_type const* path1,
                            char_type const* path2,
                            char_type const Separator,
                            char_type const AltSeparator = NullChar) noexcept
    {
        size_type combined_path_len = 0;
        size_type path1_len = 0;
        size_type path2_len = string_utility_type::GetLength(path2);
        size_type pos = string_utility_type::EndsWith(path1, path1_len, Separator);
        pos -= (AltSeparator != NullChar && pos == path1_len && pos > 0 && path1[pos - 1] == AltSeparator) ? 1 : 0;

        if (path1_len > 0)
        {
            if (path2_len > 0)
            {
                if (pos == path1_len)
                {
                    char_type const sep_str[2] = { Separator, NullChar };
                    string_copy_type::AllocCopy(combined_path, combined_path_len,
                                                path1, string_utility_type::UnsafeGetLength(path1),
                                                sep_str, (sizeof(sep_str) - 1) / sizeof(char_type),
                                                path2, path2_len);
                }
                else
                {
                    string_copy_type::AllocCopy(combined_path, combined_path_len,
                                                path1, path1_len,
                                                path2, path2_len);
                }
            }
            else
                string_copy_type::AllocCopy(combined_path, combined_path_len, path1, path1_len);
        }
        else if (path2_len > 0)
            string_copy_type::AllocCopy(combined_path, combined_path_len, path2, path2_len);
        else
            Free(combined_path);
    }
};


template< typename CharType, typename SizeType, typename AllocType>
class PathUtility<CharType, SizeType, true, AllocType>
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef AllocType alloc_type;
    typedef StringUtility<char_type, SizeType> string_utility_type;

    static bool const can_throw = true;

    typedef StringCopy<char_type, SizeType, can_throw> string_copy_type;

    static char_type const NullChar = CharConstants<char_type>::Null;

public:
    static void Free(char_type*& path)
    {
        string_copy_type::Free(path);
    }

    static void CombinePath(char_type*& combined_path,
                            char_type const* path1,
                            char_type const* path2,
                            char_type const Separator,
                            char_type const AltSeparator = NullChar)
    {
        size_type combined_path_len = 0;
        size_type path1_len = 0;
        size_type path2_len = string_utility_type::GetLength(path2);
        size_type pos = string_utility_type::EndsWith(path1, path1_len, Separator);
        pos -= (AltSeparator != NullChar && pos == path1_len && pos > 0 && path1[pos - 1] == AltSeparator) ? 1 : 0;

        if (path1_len > 0)
        {
            if (path2_len > 0)
            {
                if (pos == path1_len)
                {
                    char_type const sep_str[2] = { Separator, NullChar };
                    string_copy_type::AllocCopy(combined_path, combined_path_len,
                                                path1, string_utility_type::UnsafeGetLength(path1),
                                                sep_str, (sizeof(sep_str) - 1) / sizeof(char_type),
                                                path2, path2_len);
                }
                else
                {
                    string_copy_type::AllocCopy(combined_path, combined_path_len,
                                                path1, path1_len,
                                                path2, path2_len);
                }
            }
            else
                string_copy_type::AllocCopy(combined_path, combined_path_len, path1, path1_len);
        }
        else if (path2_len > 0)
            string_copy_type::AllocCopy(combined_path, combined_path_len, path2, path2_len);
        else
            Free(combined_path);
    }
};
} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_PATHUTILITY_HPP
