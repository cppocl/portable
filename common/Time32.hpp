/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_TIME32_HPP
#define OCL_GUARD_PORTABLE_COMMON_TIME32_HPP

#include <cstdint>
#include "BitUtility.hpp"

namespace ocl
{

/**
 * Store a time and time within a 64-bit value (time in 32-bit and time in 32-bit).
 * Use the interface to set or retrieve the parts of the time or time.
 */
class Time32
{
/// Types and constants.
public:
    /// Time types.
    typedef std::uint16_t milliseconds_type; /// range of 0..999.
    typedef std::uint8_t  seconds_type;      /// range of 0..59.
    typedef std::uint8_t  minutes_type;      /// range of 0..59.
    typedef std::uint8_t  hours_type;        /// range of 0..23.

    typedef std::uint32_t time_type;

    /// NOTE: These ranges match the cppocl DateTime project ranges.
    static milliseconds_type const MIN_MILLISECONDS = 0U;
    static milliseconds_type const MAX_MILLISECONDS = 999U;
    static seconds_type      const MIN_SECONDS      = 0U;
    static seconds_type      const MAX_SECONDS      = 59U;
    static minutes_type      const MIN_MINUTES      = 0U;
    static minutes_type      const MAX_MINUTES      = 59U;
    static hours_type        const MIN_HOURS        = 0U;
    static hours_type        const MAX_HOURS        = 23U;

// Internal use bit masks.
private:
    static time_type const MILLISECONDS_BITS = static_cast<time_type>(0x000003ff); /// 10 bits.
    static time_type const SECONDS_BITS      = static_cast<time_type>(0x0000fc00); /// 6 bits.
    static time_type const MINUTES_BITS      = static_cast<time_type>(0x003f0000); /// 6 bits.
    static time_type const HOURS_BITS        = static_cast<time_type>(0x7c000000); /// 5 bits.

/// Member functions.
public:
    /// Set time and time as initially as unused.
    Time32() noexcept
        : m_time(0u)
    {
    }

    Time32(Time32 const& time) noexcept
        : m_time(time.m_time)
    {
    }

    Time32(Time32&& time) noexcept
        : m_time(time.m_time)
    {
        time.m_time = 0U;
    }

    Time32(milliseconds_type milliseconds,
           seconds_type seconds,
           minutes_type minutes,
           hours_type hours) noexcept
        : m_time(0u)
    {
        SetMilliseconds(milliseconds);
        SetSeconds(seconds);
        SetMinutes(minutes);
        SetHours(hours);
    }

    Time32& operator =(Time32 const& time) noexcept
    {
        Copy(time);
        return *this;
    }

    Time32& operator =(Time32&& time) noexcept
    {
        Move(static_cast<Time32&&>(time));
        return *this;
    }

    Time32& operator =(time_type time) noexcept
    {
        SetTime(time);
        return *this;
    }

    bool operator ==(Time32 const& time) const noexcept
    {
        return m_time == time.m_time;
    }

    bool operator !=(Time32 const& time) const noexcept
    {
        return m_time != time.m_time;
    }

    bool operator <(Time32 const& time) const noexcept
    {
        return m_time < time.m_time;
    }

    bool operator <=(Time32 const& time) const noexcept
    {
        return m_time <= time.m_time;
    }

    bool operator >(Time32 const& time) const noexcept
    {
        return m_time > time.m_time;
    }

    bool operator >=(Time32 const& time) const noexcept
    {
        return m_time >= time.m_time;
    }

    void Clear() noexcept
    {
        m_time = 0u;
    }

    /// Copy time from another Time32 object.
    void Copy(Time32 const& time) noexcept
    {
        m_time = time.m_time;
    }

    /// Move time from another Time32 object.
    void Move(Time32&& time) noexcept
    {
        m_time = time.m_time;
        time.m_time = 0U;
    }

    /// Get 32-bit time value.
    time_type GetTime() const noexcept
    {
        return m_time;
    }

    /// Set 32 bit time value.
    void SetTime(time_type time) noexcept
    {
        m_time = time;
    }

    /// Get milliseconds within range of 0..999.
    milliseconds_type GetMilliseconds() const noexcept
    {
        return BitUtility::MaskGet<time_type, milliseconds_type>(m_time, MILLISECONDS_BITS);
    }

    /// Set milliseconds within range of 0..999.
    void SetMilliseconds(milliseconds_type milliseconds) noexcept
    {
        m_time = BitUtility::MaskSet<time_type, milliseconds_type>(m_time, MILLISECONDS_BITS, milliseconds);
    }

    /// Get seconds within range of 0..59.
    seconds_type GetSeconds() const noexcept
    {
        return BitUtility::MaskGet<time_type, seconds_type>(m_time, SECONDS_BITS);
    }

    /// Set seconds within range of 0..59.
    void SetSeconds(seconds_type seconds) noexcept
    {
        m_time = BitUtility::MaskSet<time_type, seconds_type>(m_time, SECONDS_BITS, seconds);
    }

    /// Get minutes within range of 0..59.
    minutes_type GetMinutes() const noexcept
    {
        return BitUtility::MaskGet<time_type, minutes_type>(m_time, MINUTES_BITS);
    }

    /// Set minutes within range of 0..59.
    void SetMinutes(minutes_type minutes) noexcept
    {
        m_time = BitUtility::MaskSet<time_type, minutes_type>(m_time, MINUTES_BITS, minutes);
    }

    /// Get hours within range of 0..23.
    hours_type GetHours() const noexcept
    {
        return BitUtility::MaskGet<time_type, hours_type>(m_time, HOURS_BITS);
    }

    /// Set hours within range of 0..23.
    void SetHours(hours_type hours) noexcept
    {
        m_time = BitUtility::MaskSet<time_type, hours_type>(m_time, HOURS_BITS, hours);
    }

/// Internal data.
private:
    /// Bit layout is first 10 bits for millisecond,
    /// next 6 bits seconds,
    /// then 6 bits for minutes and
    /// finally 5 bits for hours.
    time_type m_time;
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_TIME32_HPP
