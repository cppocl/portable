/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_POINT_HPP
#define OCL_GUARD_PORTABLE_COMMON_POINT_HPP

#include "Types.hpp"

namespace ocl
{

template<typename Type>
class Point
{
public:
    typedef Type type;
    typedef typename Types<Type>::diff_type diff_type;

public:
    Point() noexcept
        : m_x(0)
        , m_y(0)
    {
    }

    Point(Type x, Type y) noexcept
        : m_x(x)
        , m_y(y)
    {
    }

    Point(Point<Type> const& point) noexcept
    {
        m_x = point.m_x;
        m_y = point.m_y;
    }

    Point(Point<Type>&& point) noexcept
    {
        m_x = point.m_x;
        m_y = point.m_y;
    }

    Point<Type>& operator =(Point<Type> const& point) noexcept
    {
        m_x = point.m_x;
        m_y = point.m_y;
        return *this;
    }

    Point<Type>& operator +=(Point<Type> const& point) noexcept
    {
        m_x += point.m_x;
        m_y += point.m_y;
        return *this;
    }

    Point<Type>& operator -=(Point<Type> const& point) noexcept
    {
        m_x -= point.m_x;
        m_y -= point.m_y;
        return *this;
    }

    Point<Type>& operator *=(Point<Type> const& point) noexcept
    {
        m_x *= point.m_x;
        m_y *= point.m_y;
        return *this;
    }

    Point<Type>& operator /=(Point<Type> const& point) noexcept
    {
        m_x /= point.m_x;
        m_y /= point.m_y;
        return *this;
    }

    Point<Type> operator +(Point<Type> const& rhs) noexcept
    {
        Point<Type> pt(m_x + rhs.m_x, m_y + rhs.m_y);
        return pt;
    }

    Point<Type> operator -(Point<Type> const& rhs) noexcept
    {
        Point<Type> pt(m_x - rhs.m_x, m_y - rhs.m_y);
        return pt;
    }

    Point<Type> operator *(Point<Type> const& rhs) noexcept
    {
        Point<Type> pt(m_x * rhs.m_x, m_y * rhs.m_y);
        return pt;
    }

    Point<Type> operator /(Point<Type> const& rhs) noexcept
    {
        Point<Type> pt(m_x / rhs.m_x, m_y / rhs.m_y);
        return pt;
    }

    void Copy(Point<Type> const& point) noexcept
    {
        m_x = point.m_x;
        m_y = point.m_y;
    }

    void Move(Point<Type>& point) noexcept
    {
        m_x = point.m_x;
        m_y = point.m_y;
    }

    Type X() const noexcept
    {
        return m_x;
    }

    Type Y() const noexcept
    {
        return m_y;
    }

    void AddX(Type offset) noexcept
    {
        m_x += offset;
    }

    void AddY(Type offset) noexcept
    {
        m_y += offset;
    }

    void OffsetX(diff_type offset) noexcept
    {
        m_x = static_cast<Type>(m_x + offset);
    }

    void OffsetY(diff_type offset) noexcept
    {
        m_y = static_cast<Type>(m_y + offset);
    }

    void Set(Type x, Type y) noexcept
    {
        m_x = x;
        m_y = y;
    }

    /// Translate this Point from Type to diff_type without the need to manually cast X and Y.
    Point<diff_type> Translate() const noexcept
    {
        return Point<diff_type>(static_cast<diff_type>(X()), static_cast<diff_type>(Y()));
    }

    /// Translate the Point from Type to diff_type without the need to manually cast X and Y.
    static Point<diff_type> Translate(Point<Type> const& point) noexcept
    {
        return point.Translate();
    }

    /// Get the difference between this point and the other point provided.
    Point<diff_type> GetDiff(Point<Type> const& other) const noexcept
    {
        Point<diff_type> this_pt(Translate());
        Point<diff_type> other_pt(other.Translate());
        return Point<diff_type>(this_pt.X() - other_pt.X(), this_pt.Y() - other_pt.Y());
    }

    /// Get the difference between this point and the other point provided.
    Point<Type> GetAbsDiff(Point<Type> const& other) const noexcept
    {
        Point<diff_type> this_pt(Translate());
        Point<diff_type> other_pt(other.Translate());
        diff_type diff_x = Types<diff_type>::Abs(this_pt.X() - other_pt.X());
        diff_type diff_y = Types<diff_type>::Abs(this_pt.Y() - other_pt.Y());
        return Point<Type>(static_cast<Type>(diff_x), static_cast<Type>(diff_y));
    }

private:
    Type m_x;
    Type m_y;
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_POINT_HPP
