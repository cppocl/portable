/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_SIZE_HPP
#define OCL_GUARD_PORTABLE_COMMON_SIZE_HPP

#include "Types.hpp"

namespace ocl
{

template<typename Type>
class Size
{
public:
    typedef Type type;
    typedef typename Types<Type>::diff_type diff_type;

public:
    Size() noexcept
        : m_width(0)
        , m_height(0)
    {
    }

    Size(Type width, Type height) noexcept
        : m_width(width)
        , m_height(height)
    {
    }

    Size(Size<Type> const& size) noexcept
    {
        m_width = size.m_width;
        m_height = size.m_height;
    }

    Size(Size<Type>&& size) noexcept
    {
        m_width = size.m_width;
        m_height = size.m_height;
    }

    Size<Type>& operator =(Size<Type> const& size) noexcept
    {
        m_width = size.m_width;
        m_height = size.m_height;
        return *this;
    }

    Size<Type>& operator +=(Size<Type> const& size) noexcept
    {
        m_width += size.m_width;
        m_height += size.m_height;
        return *this;
    }

    Size<Type>& operator -=(Size<Type> const& size) noexcept
    {
        m_width -= size.m_width;
        m_height -= size.m_height;
        return *this;
    }

    Size<Type>& operator *=(Size<Type> const& size) noexcept
    {
        m_width *= size.m_width;
        m_height *= size.m_height;
        return *this;
    }

    Size<Type>& operator /=(Size<Type> const& size) noexcept
    {
        m_width /= size.m_width;
        m_height /= size.m_height;
        return *this;
    }

    Size<Type> operator +(Size<Type> const& rhs) noexcept
    {
        Size<Type> size(m_width + rhs.m_width, m_height + rhs.m_height);
        return size;
    }

    Size<Type> operator -(Size<Type> const& rhs) noexcept
    {
        Size<Type> size(m_width - rhs.m_width, m_height - rhs.m_height);
        return size;
    }

    Size<Type> operator *(Size<Type> const& rhs) const& noexcept
    {
        Size<Type> size(m_width * rhs.m_width, m_height * rhs.m_height);
        return size;
    }

    Size<Type> operator /(Size<Type> const& rhs) const& noexcept
    {
        Size<Type> size(m_width / rhs.m_width, m_height / rhs.m_height);
        return size;
    }

    void Copy(Size<Type> const& size) noexcept
    {
        m_width = size.m_width;
        m_height = size.m_height;
    }

    void Move(Size<Type>& size) noexcept
    {
        m_width = size.m_width;
        m_height = size.m_height;
    }

    Type Width() const noexcept
    {
        return m_width;
    }

    Type Height() const noexcept
    {
        return m_height;
    }

    void AddWidth(Type offset) noexcept
    {
        m_width += offset;
    }

    void AddHeight(Type offset) noexcept
    {
        m_height += offset;
    }

    void OffsetWidth(diff_type offset) noexcept
    {
        m_width = static_cast<Type>(m_width + offset);
    }

    void OffsetHeight(diff_type offset) noexcept
    {
        m_height = static_cast<Type>(m_height + offset);
    }

    void Set(Type width, Type height) noexcept
    {
        m_width = width;
        m_height = height;
    }

    /// Translate this Size from Type to diff_type without the need to manually cast Width and Height.
    Size<diff_type> Translate() const noexcept
    {
        return Size<diff_type>(static_cast<diff_type>(Width()), static_cast<diff_type>(Height()));
    }

    /// Translate the Size from Type to diff_type without the need to manually cast Width and Height.
    static Size<diff_type> Translate(Size<Type> const& size) noexcept
    {
        return size.Translate();
    }

    /// Get the difference between this point and the other point provided.
    Size<diff_type> GetDiff(Size<Type> const& other) const noexcept
    {
        Size<diff_type> this_size(Translate());
        Size<diff_type> other_size(other.Translate());
        return Size<diff_type>(this_size.Width() - other_size.Width(), this_size.Height() - other_size.Height());
    }

    /// Get the difference between this point and the other point provided.
    Size<Type> GetAbsDiff(Size<Type> const& other) const noexcept
    {
        Size<diff_type> this_size(Translate());
        Size<diff_type> other_size(other.Translate());
        diff_type diff_width  = Types<diff_type>::Abs(this_size.Width() - other_size.Width());
        diff_type diff_height = Types<diff_type>::Abs(this_size.Height() - other_size.Height());
        return Size<Type>(static_cast<Type>(diff_width), static_cast<Type>(diff_height));
    }

private:
    Type m_width;
    Type m_height;
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_SIZE_HPP
