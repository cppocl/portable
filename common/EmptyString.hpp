/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_COMMON_EMPTYSTRING_HPP
#define OCL_GUARD_PORTABLE_COMMON_EMPTYSTRING_HPP

namespace ocl
{

template<typename CharType>
struct EmptyString;

template<>
struct EmptyString<char>
{
    static constexpr char const* GetEmptyString() noexcept { return ""; }
};

template<>
struct EmptyString<wchar_t>
{
    static constexpr wchar_t const* GetEmptyString() noexcept { return L""; }
};

template<>
struct EmptyString<char16_t>
{
    static constexpr char16_t const* GetEmptyString() noexcept { return u""; }
};

template<>
struct EmptyString<char32_t>
{
    static constexpr char32_t const* GetEmptyString() noexcept { return U""; }
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_COMMON_EMPTYSTRING_HPP
