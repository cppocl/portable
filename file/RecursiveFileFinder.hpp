/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_RECURSIVEFILEFINDER_HPP
#define OCL_GUARD_PORTABLE_FILE_RECURSIVEFILEFINDER_HPP

#include "../common/StringCopy.hpp"
#include "../common/PathUtility.hpp"
#include "FileFinder.hpp"
#include <cstddef>
#include <new>

namespace ocl
{

template <typename CharType>
class RecursiveFileFinder
{
public:
    typedef CharType char_type;
    typedef FileData<char_type> file_data_type;
    typedef StringCopy<char_type> string_copy_type;
    typedef PathUtility<char_type> path_utility_type;
    typedef FileFinder<char_type> file_finder_type;
    typedef RecursiveFileFinder<char_type> recursive_file_finder_type;

    static char_type const NullChar = file_finder_type::NullChar;
    static char_type const FullStopChar = file_finder_type::FullStopChar;
    static char_type const SeparatorChar = file_finder_type::SeparatorChar;

public:
    RecursiveFileFinder(std::size_t depth = ~static_cast<std::size_t>(0));

    ~RecursiveFileFinder();

    /// Return true if FindFirst or FindNext still has something to do.
    bool IsFinding() const noexcept;

    /// Search for the first matching file from a full Windows path and a wild card search.
    /// GetFindData will return the file data for the match.
    bool FindFirst(CharType const* full_path, CharType const* wild_card = nullptr) noexcept;

    /// Find the next matching file specified by FindFirst.
    /// GetFindData will return the file data for the match.
    bool FindNext() noexcept;

    /// When a function fails the last error code can be obtained
    /// from calling function GetLastError.
    bool HasLastError() const noexcept;

    /// Get the latest file data for the last call to FindFirst or FindNext.
    file_data_type const& GetFileData() const noexcept;

    /// Get the current file name after FindFirst or FindNext.
    char_type const* GetFilename() const noexcept;

    /// Get the 64-bit file size of the current file for FindFirst or FindNext.
    std::uint64_t GetFileSize() const noexcept;

    /// Get Date/Time the file was created.
    DateTime64 GetCreationDateTime() const noexcept;

    /// Get the last time the file was read, modified or executed.
    DateTime64 GetLastAccessDateTime() const noexcept;

    /// Get the last time the file was modified or overwritten.
    DateTime64 GetLastModifiedDateTime() const noexcept;

    /// Return if file is a directory.
    bool IsDirectory() const noexcept;

    /// Return true if the directory name is not "." or "..".
    bool IsDirectoryImportant() const noexcept;

    /// Return true if the directory name is not "." or "..".
    bool IsDirectoryUnimportant() const noexcept;

    /// If the current directory is important and depth is greater than zero, reutrn true.
    bool IsRecursiveFind() const noexcept;

    /// Close the file finder handle and free any internal memory.
    /// NOTE: FindFirst and the destructor calls this automatically.
    void Close();

    /// Get depth for recursive file finder.
    std::size_t GetDepth() const noexcept;

    /// Get full path provided for FindFirst call.
    char_type const* GetFullPath() const noexcept;

    /// Get path for current recursive level of search.
    char_type const* GetCurrentPath() const noexcept;

    /// Get wild card specified within FindFirst.
    char_type const* GetWildCard() const noexcept;

    /// Return the last RecursiveFileFinder, where m_recursive_file_finder is null.
    recursive_file_finder_type& GetLastChild() const noexcept;

    /// Return the last RecursiveFileFinder, where m_recursive_file_finder is null.
    recursive_file_finder_type* GetChild() noexcept;
    recursive_file_finder_type const* GetChild() const noexcept;

    /// Free the last recursive file finder and return the previous one.
    recursive_file_finder_type* GetParent() noexcept;
    recursive_file_finder_type const* GetParent() const noexcept;

/// Helper functions (internal use only)
private:
    /// Must only be called on the top level recursive file finder object.
    bool RecursiveFindFirst() noexcept;

    static void CombinePath(char_type*& combined_path,
                            char_type const* path1,
                            char_type const* path2) noexcept;

/// Data (internal use only)
private:
    file_finder_type m_file_finder;

    /// When this pointer is not the top level recursive file finder, this will not be null.
    recursive_file_finder_type* m_parent;

    /// When performing the recursive find, this will not be null.
    recursive_file_finder_type* m_child;

    /// When performing the recursive find, this will not be null.
    recursive_file_finder_type* m_current;

    char_type* m_wild_card;
    char_type* m_full_path;

    /// Recursive depth.
    std::size_t m_depth;
};

#include "internal/RecursiveFileFinder.inl"

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_FILE_RECURSIVEFILEFINDER_HPP
