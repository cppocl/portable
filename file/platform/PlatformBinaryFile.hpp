/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_PLATFORM_PLATFORMBINARYFILE_HPP
#define OCL_GUARD_PORTABLE_FILE_PLATFORM_PLATFORMBINARYFILE_HPP

#include "../../common/PlatformDefines.hpp"

#if OCL_PLATFORM == OCL_PLATFORM_WINDOWS
    #if OCL_COMPILER_POSIX_SUPPORT == 0
    #include "win/WinBinaryFile.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformBinaryFile = WinBinaryFile<char_type>;
    }
    #else // OCL_COMPILER_POSIX_SUPPORT != 0
    #include "posix/PosixBinaryFile.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformBinaryFile = PosixBinaryFile<char_type>;
    }
    #endif
#elif OCL_PLATFORM == OCL_PLATFORM_LINUX
    #include "linux/LinuxBinaryFile.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformBinaryFile = LinuxBinaryFile<char_type>;
    }
#elif OCL_PLATFORM == OCL_PLATFORM_UNIX
    #include "unix/UnixBinaryFile.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformBinaryFile = UnixBinaryFile<char_type>;
    }
#elif OCL_PLATFORM == OCL_PLATFORM_BSD
    #include "bsd/BsdBinaryFile.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformBinaryFile = BsdBinaryFile<char_type>;
    }
#elif OCL_PLATFORM == OCL_PLATFORM_MAC
    #include "linux/MacBinaryFile.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformBinaryFile = MacBinaryFile<char_type>;
    }
#elif OCL_PLATFORM == OCL_PLATFORM_ANDROID
    #include "android/AndroidBinaryFile.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformBinaryFile = AndroidBinaryFile<char_type>;
    }
#else
    // If platform cannot be detected then attempt the Posix solution as a generic alternative.
    #include "posix/PosixBinaryFile.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformBinaryFile = PosixBinaryFile<char_type>;
    }
#endif

#endif // OCL_GUARD_PORTABLE_FILE_PLATFORM_PLATFORMBINARYFILE_HPP
