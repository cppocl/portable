/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_PLATFORM_WIN_WIN32_FILE_WINFILEFINDER_HPP
#define OCL_GUARD_PORTABLE_PLATFORM_WIN_WIN32_FILE_WINFILEFINDER_HPP

#include "../../../common/CharConstants.hpp"
#include "../../../common/DateTime64.hpp"
#include "../../../common/FileData.hpp"
#include "../../../common/StringCopy.hpp"
#include "../../../common/StringUtility.hpp"
#include "../../../common/PathUtility.hpp"
#include "WinFileFinderTypes.hpp"
#include <cstring>
#include <cstddef>
#include <cstdint>

namespace ocl
{

/// Provide the ability to find files using a find first/next.
template<typename CharType>
class WinFileFinder
{
// Types and constants.
public:
    typedef CharType char_type;
    typedef DWORD error_type;
    typedef typename WinFileFinderTypes<CharType> file_fnder_types;
    typedef typename file_fnder_types::find_data_type find_data_type;
    typedef typename CharConstants<CharType> char_constants_type;
    typedef FileData<CharType> file_data_type;
    typedef typename file_data_type::file_size_type file_size_type;

    static char_type const NullChar         = char_constants_type::Null;
    static char_type const BackSlashChar    = char_constants_type::BackSlash;
    static char_type const ForwardSlashChar = char_constants_type::ForwardSlash;
    static char_type const FullStopChar     = char_constants_type::FullStop;
    static char_type const SeparatorChar    = BackSlashChar;

// Construction and destruction.
public:
    /// The default constructor can be used, if you don't need to use Parse and
    // you don't want to provide a functor.
    WinFileFinder() noexcept;

    // Any handles will be closed on destruction of the WinFileFinder object.
    ~WinFileFinder() noexcept;

// Interface functions.
public:
    /// Return true if FindFirst or FindNext still has something to do.
    bool IsFinding() const noexcept;

    /// Search for the first matching file from a full Windows path and a wild card search.
    /// GetFindData will return the file data for the match.
    bool FindFirst(CharType const* full_path,
                   CharType const* wild_card = nullptr) noexcept;

    /// Find the next matching file specified by FindFirst.
    /// GetFindData will return the file data for the match.
    bool FindNext() noexcept;

    /// When a function fails the last error code can be obtained
    /// from calling function GetLastError.
    error_type GetLastError() const noexcept;

    /// Return true when FindFirst or FindNext returned an error.
    bool HasLastError() const noexcept;

    /// Get the current file name after FindFirst or FindNext.
    char_type const* GetFilename() const noexcept;

    /// Get the 64-bit file size of the current file for FindFirst or FindNext.
    file_size_type GetFileSize() const noexcept;

    /// Get file data for first or next found file.
    file_data_type const& GetFileData() const noexcept;

    /// Get Date/Time the file was created.
    DateTime64 GetCreationDateTime() const noexcept;

    /// Get the last time the file was read, modified or executed.
    DateTime64 GetLastAccessDateTime() const noexcept;

    /// Get the last time the file was modified or overwritten.
    DateTime64 GetLastModifiedDateTime() const noexcept;

    /// Return if file is a directory.
    bool IsDirectory() const noexcept;

    /// Return true if the directory name is not "." or "..".
    bool IsDirectoryImportant() const noexcept;

    /// Return true if the directory name is "." or "..".
    bool IsDirectoryUnimportant() const noexcept;

    /// Note: Closing the handle at the first possible opportunity will ensure other
    /// processes can access the files and folders.
    void Close();

    // Helper functions (internal use only)
private:
    typedef StringUtility<char_type> string_utility_type;
    typedef StringCopy<char_type>    string_copy_type;

    /// If there is a chance that FindFirst or FindNext is being used across
    /// multiple threads, then the find handle and file data can be manually managed
    /// per thread by using FindFirstFile and FindNextFile.
    ///
    /// It's also possible to call the Win32 API function ::GetLastError()
    /// to get the error code when FindFirstFile or FindNextFile return false.
    ///
    /// When FindFirstFile returns false, the handle is set to NULL and the file data
    /// is cleared.
    static bool FindFirst(CharType const* full_path,
                          CharType const* wild_card,
                          HANDLE& find_handle,
                          find_data_type& find_data) noexcept;

    /// See comments for FindFirstFile.
    static bool FindNext(HANDLE& find_handle, find_data_type& find_data) noexcept;

    /// Close the find handle and set the file handle to NULL.
    /// Return false if there is an error, otherwise return true if the handle
    /// is closed or the handle was NULL.
    /// The Win32 API function call ::GetLastError() can be used to get the error code.
    static bool CloseFindHandle(HANDLE& find_handle) noexcept;

    /// Convet the FILETIME values stored within FIND_DATA structure to a DataTime64.
    static DateTime64 CovertDateTime(FILETIME file_time) noexcept;

    /// Convert Win32 find data structure to file data structure.
    static void SetFileData(file_data_type& file_data, find_data_type const& find_data) noexcept;

    /// Thin wrapper to ::FindFirstFileA and ::FindFirstFileW
    static HANDLE InternalFindFirstFile(char const* search_path, WIN32_FIND_DATAA& find_data);
    static HANDLE InternalFindFirstFile(wchar_t const* search_path, WIN32_FIND_DATAW& find_data);

    /// Thin wrapper to ::FindNextFileA and ::FindNextFileW
    static bool InternalFindNextFile(HANDLE find_handle, WIN32_FIND_DATAA& find_data);
    static bool InternalFindNextFile(HANDLE find_handle, WIN32_FIND_DATAW& find_data);

    /// Treat . and .. as unimportant folders.
    static bool IsUnimportantFolder(CharType const* folder);

// Data (internal use only)
private:
    /// Handle used for find first and find next function calls.
    HANDLE m_find_handle;

    /// Store last error from FindFirstFile or FindLastFile;
    DWORD m_last_error;

    /// Each file found is stored in the file data structure.
    file_data_type m_file_data;
};

// Pull in the implementation for the member functions.
#include "WinFileFinder.inl"

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_PLATFORM_WIN_WIN32_FILE_WINFILEFINDER_HPP
