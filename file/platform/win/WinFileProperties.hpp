/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_PLATFORM_WIN_WINFILEPROPERTIES_HPP
#define OCL_GUARD_PORTABLE_FILE_PLATFORM_WIN_WINFILEPROPERTIES_HPP

#include <Windows.h>

namespace ocl
{
    enum class FileProperties : DWORD
    {
        Archive      = FILE_ATTRIBUTE_ARCHIVE,
        Directory    = FILE_ATTRIBUTE_DIRECTORY,
        Compressed   = FILE_ATTRIBUTE_COMPRESSED,
        Encrypted    = FILE_ATTRIBUTE_ENCRYPTED,
        Hidden       = FILE_ATTRIBUTE_HIDDEN,
        Normal       = FILE_ATTRIBUTE_NORMAL,
        ReadOnly     = FILE_ATTRIBUTE_READONLY,
        System       = FILE_ATTRIBUTE_SYSTEM,
        Offline      = FILE_ATTRIBUTE_OFFLINE,
        Unindexed    = FILE_ATTRIBUTE_NOT_CONTENT_INDEXED,
        SymbolicLink = FILE_ATTRIBUTE_REPARSE_POINT,
        Sparse       = FILE_ATTRIBUTE_SPARSE_FILE,

        // Reserved for system use.
        Device       = FILE_ATTRIBUTE_DEVICE,
        Temporary    = FILE_ATTRIBUTE_TEMPORARY
    };
}

#endif // OCL_GUARD_PORTABLE_FILE_PLATFORM_WIN_WINFILEPROPERTIES_HPP
