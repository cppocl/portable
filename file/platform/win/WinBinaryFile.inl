/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType>
WinBinaryFile<CharType>::WinBinaryFile()
    : m_file_handle(INVALID_HANDLE_VALUE)
    , m_last_error(ERROR_SUCCESS)
    , m_flags_and_attributes(0)
{
}

template<typename CharType>
WinBinaryFile<CharType>::WinBinaryFile(char_type const* full_path,
                                       OpenModes open_mode,
                                       AccessModes access_modes,
                                       ShareModes share_modes,
                                       Attributes file_attributes,
                                       Behaviour behavioural_modes,
                                       Security security_modes,
                                       SECURITY_ATTRIBUTES* security_attributes,
                                       HANDLE template_file)
    : m_file_handle(INVALID_HANDLE_VALUE)
    , m_last_error(ERROR_SUCCESS)
    , m_flags_and_attributes(0)
{
    (void)Open(full_path,
               open_mode,
               access_modes,
               share_modes,
               file_attributes,
               behavioural_modes,
               security_modes,
               security_attributes,
               template_file);
}

template<typename CharType>
WinBinaryFile<CharType>::~WinBinaryFile()
{
    if (IsOpen())
        (void)Close();
}

template<typename CharType>
bool WinBinaryFile<CharType>::Open(char_type const* full_path,
                                   OpenModes open_mode,
                                   AccessModes access_modes,
                                   ShareModes share_modes,
                                   Attributes file_attributes,
                                   Behaviour behavioural_modes,
                                   Security security_modes,
                                   SECURITY_ATTRIBUTES* security_attributes,
                                   HANDLE template_file)
{
    HANDLE file_handle = INVALID_HANDLE_VALUE;

    if (m_file_handle == INVALID_HANDLE_VALUE)
    {
        char_type* unc_path = nullptr;

        // Note that UNC path names fail to open or create when the specified path
        // is a file name relative to the current location.
        // Don't use UNC conversion when path name doesn't need handling as a long path.
        if (StrLen(full_path) >= MAX_PATH)
            unc_path = InternalMakeUNCPath(full_path);

        DWORD flags_and_attributes = GetFlagsAndAttributes(file_attributes,
                                                           behavioural_modes,
                                                           security_modes);

        file_handle = InternalOpenFile(unc_path != nullptr ? unc_path : full_path,
                                       static_cast<DWORD>(access_modes),
                                       static_cast<DWORD>(share_modes),
                                       security_attributes,
                                       static_cast<DWORD>(open_mode),
                                       static_cast<DWORD>(flags_and_attributes),
                                       template_file);

        m_file_handle = file_handle;
        m_flags_and_attributes = flags_and_attributes;
        if (file_handle == INVALID_HANDLE_VALUE)
            m_last_error = ::GetLastError();

        ::free(unc_path);
    }

    return file_handle != INVALID_HANDLE_VALUE;
}

template<typename CharType>
bool WinBinaryFile<CharType>::IsOpen() const noexcept
{
    return m_file_handle != INVALID_HANDLE_VALUE;
}

template<typename CharType>
typename WinBinaryFile<CharType>::size_type WinBinaryFile<CharType>::GetFileSize() const
{
    size_type file_size;

    if (m_file_handle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER size;
        if (::GetFileSizeEx(m_file_handle, &size) != FALSE)
        {
            if (size.QuadPart >= 0)
                file_size = static_cast<size_type>(size.QuadPart);
            else
                file_size = UNKNOWN_FILE_SIZE;
        }
        else
            file_size = UNKNOWN_FILE_SIZE;
    }
    else
        file_size = UNKNOWN_FILE_SIZE;

    return file_size;
}

template<typename CharType>
bool WinBinaryFile<CharType>::Read(unsigned char* buffer,
                                   size_type number_of_bytes,
                                   LPOVERLAPPED overlapped,
                                   LPOVERLAPPED_COMPLETION_ROUTINE completion_func)
{
    bool success;

    if (m_file_handle != INVALID_HANDLE_VALUE)
    {
        // If asynchronous then an OVERLAPPED pointer must be provided.
        bool is_asynchronous = (m_flags_and_attributes & static_cast<behavioural_type>(Behaviour::BehaviourAsynchronous)) != 0;

        if ( !is_asynchronous || (overlapped != nullptr) )
        {
            BOOL ret = (number_of_bytes > 0) ? TRUE : FALSE;

            while (number_of_bytes > 0)
            {
                DWORD block_bytes = (number_of_bytes > MAX_DWORD)
                                    ? MAX_DWORD
                                    : static_cast<DWORD>(number_of_bytes);

                if (completion_func != nullptr)
                {
                    ret = ::ReadFileEx(m_file_handle,
                                       buffer,
                                       block_bytes,
                                       overlapped,
                                       completion_func);
                }
                else
                {
                    DWORD number_of_bytes_read = 0;
                    DWORD* p_number_of_bytes_read = is_asynchronous ? nullptr : &number_of_bytes_read;
                    ret = ::ReadFile(m_file_handle,
                                     buffer,
                                     block_bytes,
                                     p_number_of_bytes_read,
                                     overlapped);
                }

                if (ret == 0)
                    break;

                if (block_bytes >= number_of_bytes)
                    number_of_bytes -= block_bytes;
                else
                    number_of_bytes = 0;
            }

            success = ret != 0;

            if (!success)
                m_last_error = ::GetLastError();
        }
        else
        {
            // Cannot do asynchronous read without OVERLAPPED pointer.
            success = false;
            m_last_error = ERROR_NOT_SUPPORTED;
        }
    }
    else
    {
        // Cannot read without a file handle.
        success = false;
        m_last_error = ERROR_INVALID_HANDLE;
    }

    return success;
}

template<typename CharType>
bool WinBinaryFile<CharType>::Read(char* buffer,
                                   size_type number_of_bytes,
                                   LPOVERLAPPED overlapped,
                                   LPOVERLAPPED_COMPLETION_ROUTINE completion_func)
{
    return Read(reinterpret_cast<unsigned char*>(buffer),
                number_of_bytes,
                overlapped,
                completion_func);
}

template<typename CharType>
bool WinBinaryFile<CharType>::Write(unsigned char const* buffer,
                                    size_type number_of_bytes,
                                    LPOVERLAPPED overlapped,
                                    LPOVERLAPPED_COMPLETION_ROUTINE completion_func)
{
    bool success;

    if (m_file_handle != INVALID_HANDLE_VALUE)
    {
        // If asynchronous then an OVERLAPPED pointer must be provided.
        bool is_asynchronous = (m_flags_and_attributes & static_cast<behavioural_type>(Behaviour::BehaviourAsynchronous)) != 0;

        if ( !is_asynchronous || (overlapped != nullptr) )
        {
            BOOL ret = (number_of_bytes > 0) ? TRUE : FALSE;

            while (number_of_bytes > 0)
            {
                DWORD block_bytes = (number_of_bytes > MAX_DWORD)
                                    ? MAX_DWORD
                                    : static_cast<DWORD>(number_of_bytes);

                if (completion_func != nullptr)
                {
                    ret = ::WriteFileEx(m_file_handle,
                                        buffer,
                                        block_bytes,
                                        overlapped,
                                        completion_func);
                }
                else
                {
                    DWORD number_of_bytes_written = 0;
                    DWORD* p_number_of_bytes_written = is_asynchronous ? nullptr : &number_of_bytes_written;
                    ret = ::WriteFile(m_file_handle,
                                      buffer,
                                      block_bytes,
                                      p_number_of_bytes_written,
                                      overlapped);
                }

                if (ret == 0)
                    break;

                if (number_of_bytes >= block_bytes)
                    number_of_bytes -= block_bytes;
                else
                    number_of_bytes = 0;
            }

            success = ret != 0;

            if (!success)
                m_last_error = ::GetLastError();
        }
        else
        {
            // Cannot do asynchronous write without OVERLAPPED pointer.
            success = false;
            m_last_error = ERROR_NOT_SUPPORTED;
        }
    }
    else
    {
        // Cannot write without a file handle.
        success = false;
        m_last_error = ERROR_INVALID_HANDLE;
    }

    return success;
}

template<typename CharType>
bool WinBinaryFile<CharType>::Write(char const* buffer,
                                    size_type number_of_bytes,
                                    LPOVERLAPPED overlapped,
                                    LPOVERLAPPED_COMPLETION_ROUTINE completion_func)
{
    return Write(reinterpret_cast<unsigned char const*>(buffer),
                 number_of_bytes,
                 overlapped,
                 completion_func);
}

template<typename CharType>
bool WinBinaryFile<CharType>::SetFileSize(size_type file_size)
{
    bool success;

    if (m_file_handle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER pos;
        LARGE_INTEGER ret_pos;
        pos.QuadPart = static_cast<decltype(pos.QuadPart)>(file_size);
        ::SetFilePointerEx(m_file_handle, pos, &ret_pos, FILE_BEGIN);
        ::SetEndOfFile(m_file_handle);
        success = true;
    }
    else
        success = false;

    return success;
}

template<typename CharType>
bool WinBinaryFile<CharType>::Seek(seek_type position)
{
    return InternalSeek(m_file_handle, position, FILE_BEGIN, m_last_error);
}

template<typename CharType>
bool WinBinaryFile<CharType>::SeekFromEnd(seek_type count)
{
    return InternalSeek(m_file_handle, count, FILE_END, m_last_error);
}

template<typename CharType>
bool WinBinaryFile<CharType>::SeekToStart()
{
    return InternalSeek(m_file_handle, 0, FILE_BEGIN, m_last_error);
}

template<typename CharType>
bool WinBinaryFile<CharType>::SeekToEnd()
{
    return InternalSeek(m_file_handle, 0, FILE_END, m_last_error);
}

template<typename CharType>
bool WinBinaryFile<CharType>::Flush()
{
    bool success;

    if (m_file_handle != INVALID_HANDLE_VALUE)
    {
        success = ::FlushFileBuffers(m_file_handle) != FALSE;
        if (!success)
            m_last_error = ::GetLastError();
    }
    else
    {
        m_last_error = ERROR_INVALID_HANDLE;
        success = false;
    }

    return success;
}

template<typename CharType>
bool WinBinaryFile<CharType>::Close()
{
    bool success;

    if (m_file_handle != INVALID_HANDLE_VALUE)
    {
        success = ::CloseHandle(m_file_handle) != FALSE;
        if (success)
            m_file_handle = INVALID_HANDLE_VALUE;
        else
            m_last_error = ::GetLastError();
    }
    else
    {
        m_last_error = ERROR_INVALID_HANDLE;
        success = false;
    }

    return success;
}

template<typename CharType>
typename WinBinaryFile<CharType>::error_type
WinBinaryFile<CharType>::GetLastError() const noexcept
{
    return m_last_error;
}

template<typename CharType>
bool WinBinaryFile<CharType>::Exists(char_type const* full_path)
{
    WIN32_FILE_ATTRIBUTE_DATA attr_data;
    return GetFileAttributes(full_path, attr_data);
}

template<typename CharType>
typename WinBinaryFile<CharType>::size_type
WinBinaryFile<CharType>::GetFileSize(char_type const* full_path)
{
    WIN32_FILE_ATTRIBUTE_DATA attr_data;
    size_type file_size = 0;

    if (GetFileAttributes(full_path, attr_data))
        file_size = attr_data.nFileSizeLow | (static_cast<size_type>(attr_data.nFileSizeHigh) << 32U);

    return file_size;
}

template<typename CharType>
typename WinBinaryFile<CharType>::Attributes
WinBinaryFile<CharType>::GetFileAttributes(char_type const* full_path)
{
    DWORD attributes;
    if (unc_utility_type::IsUNC(full_path))
        attributes = InternalGetFileAttributes(full_path);
    else
    {
        char_type* unc_path = InternalMakeUNCPath(full_path);
        attributes = InternalGetFileAttributes(full_path);
        ::free(unc_path);
    }

    return static_cast<attribute_type>(attributes);
}

template<typename CharType>
bool WinBinaryFile<CharType>::GetFileAttributes(char_type const* full_path,
                                                WIN32_FILE_ATTRIBUTE_DATA& attribute_data)
{
    bool success;
    if (unc_utility_type::IsUNC(full_path))
        success = InternalGetFileAttributes(full_path, attribute_data);
    else
    {
        char_type* unc_path = InternalMakeUNCPath(full_path);
        if (unc_path != nullptr)
        {
            success = InternalGetFileAttributes(full_path, attribute_data);
            ::free(unc_path);
        }
        else
            success = false;
    }

    return success;
}

template<typename CharType>
bool WinBinaryFile<CharType>::DeleteFile(char_type const* full_path, error_type& last_error)
{
    bool success = InternalDeleteFile(full_path);
    if (!success)
        last_error = ::GetLastError();
    return success;
}

template<typename CharType>
DWORD WinBinaryFile<CharType>::GetFlagsAndAttributes(Attributes file_attributes,
                                                     Behaviour behavioural_modes,
                                                     Security security_modes) noexcept
{
    DWORD flags_and_attributes = static_cast<behavioural_type>(behavioural_modes) |
                                 static_cast<attribute_type>(file_attributes);

    if (security_modes != Security::SecurityNone)
        flags_and_attributes |= static_cast<security_type>(security_modes) |
                                static_cast<size_type>(SECURITY_SQOS_PRESENT);

    return flags_and_attributes;
}

template<typename CharType>
HANDLE WinBinaryFile<CharType>::InternalOpenFile(char const* path,
                                                 DWORD access_mode,
                                                 DWORD share_modes,
                                                 SECURITY_ATTRIBUTES* security_attributes,
                                                 DWORD creation,
                                                 DWORD flags_and_attributes,
                                                 HANDLE template_file)
{
    return ::CreateFileA(path, access_mode, share_modes, security_attributes,
                         creation, flags_and_attributes, template_file);
}

template<typename CharType>
HANDLE WinBinaryFile<CharType>::InternalOpenFile(wchar_t const* path,
                                                 DWORD access_mode,
                                                 DWORD share_modes,
                                                 SECURITY_ATTRIBUTES* security_attributes,
                                                 DWORD creation,
                                                 DWORD flags_and_attributes,
                                                 HANDLE template_file)
{
    return ::CreateFileW(path, access_mode, share_modes, security_attributes,
                         creation, flags_and_attributes, template_file);
}

template<typename CharType>
bool WinBinaryFile<CharType>::InternalSeek(HANDLE file_handle,
                                           seek_type positon,
                                           DWORD whence,
                                           error_type& last_error)
{
    bool success;

    if (file_handle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER pos;
        pos.QuadPart = static_cast<LONGLONG>(positon);
        success = ::SetFilePointerEx(file_handle, pos, nullptr, whence) != FALSE;
        last_error = success ? ERROR_SUCCESS : ::GetLastError();
    }
    else
    {
        last_error = ERROR_INVALID_HANDLE;
        success = false;
    }

    return success;
}

template<typename CharType>
DWORD WinBinaryFile<CharType>::InternalGetFileAttributes(char const* full_path)
{
    return ::GetFileAttributesA(full_path);
}

template<typename CharType>
DWORD WinBinaryFile<CharType>::InternalGetFileAttributes(wchar_t const* full_path)
{
    return ::GetFileAttributesA(full_path);
}

template<typename CharType>
bool WinBinaryFile<CharType>::InternalGetFileAttributes(char const* full_path,
                                                        WIN32_FILE_ATTRIBUTE_DATA& attribute_data)
{
    return ::GetFileAttributesExA(full_path, GetFileExInfoStandard, &attribute_data) != FALSE;
}

template<typename CharType>
bool WinBinaryFile<CharType>::InternalGetFileAttributes(wchar_t const* full_path,
                                                        WIN32_FILE_ATTRIBUTE_DATA& attribute_data)
{
    return ::GetFileAttributesExW(full_path, GetFileExInfoStandard, &attribute_data) != FALSE;
}

template<typename CharType>
bool WinBinaryFile<CharType>::InternalDeleteFile(char const* full_path)
{
    return ::DeleteFileA(full_path) != FALSE;
}

template<typename CharType>
bool WinBinaryFile<CharType>::InternalDeleteFile(wchar_t const* full_path)
{
    return ::DeleteFileW(full_path) != FALSE;
}

template<typename CharType>
CharType* WinBinaryFile<CharType>::InternalMakeUNCPath(char_type const* path)
{
    char_type* unc_path = nullptr;
    if (path != nullptr)
    {
        size_t const path_len = StrLen(path);
        size_t const unc_size = sizeof(char_type) * (path_len + unc_utility_type::GetUNCLength() + 1);
        unc_path = static_cast<char_type*>(::malloc(unc_size));
        if (unc_path != nullptr)
            unc_utility_type::GetUNCPath(unc_path, unc_size, path, path_len);
    }
    return unc_path;
}
