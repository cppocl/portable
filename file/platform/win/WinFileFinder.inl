/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType>
WinFileFinder<CharType>::WinFileFinder() noexcept
    : m_find_handle(NULL)
    , m_last_error(0)
{
}

template<typename CharType>
WinFileFinder<CharType>::~WinFileFinder() noexcept
{
    // Handle should be closed before destruction,
    // but ensure any missed opportunities are not missed.
    Close();
}

template<typename CharType>
bool WinFileFinder<CharType>::IsFinding() const noexcept
{
    return m_find_handle != NULL;
}

template<typename CharType>
bool WinFileFinder<CharType>::FindFirst(CharType const* full_path,
                                        CharType const* wild_card) noexcept
{
    typedef typename string_copy_type::size_type search_path_size_type;
    typedef PathUtility<char_type> path_utility_type;

    bool success = true;
    Close();

    if (full_path != nullptr)
    {
        find_data_type find_data;
        success = FindFirst(full_path, wild_card, m_find_handle, find_data);
        if (!success)
            m_last_error = ::GetLastError();
        else
            SetFileData(m_file_data, find_data);
    }
    else
        success = false;

    return success;
}

template<typename CharType>
bool WinFileFinder<CharType>::FindNext() noexcept
{
    bool success;
    if (m_find_handle != NULL)
    {
        find_data_type find_data;
        success = InternalFindNextFile(m_find_handle, find_data);
        if (!success)
        {
            DWORD last_error = ::GetLastError();
            if (last_error != ERROR_NO_MORE_FILES)
                m_last_error = last_error;
            Close();
        }
        else
            SetFileData(m_file_data, find_data);
    }
    else
        success = false;
    return success;
}

template<typename CharType>
typename WinFileFinder<CharType>::error_type
WinFileFinder<CharType>::GetLastError() const noexcept
{
    return m_last_error;
}

template<typename CharType>
bool WinFileFinder<CharType>::HasLastError() const noexcept
{
    return m_last_error != ERROR_SUCCESS;
}

template<typename CharType>
CharType const* WinFileFinder<CharType>::GetFilename() const noexcept
{
    return m_file_data.GetFilename();
}

template<typename CharType>
typename WinFileFinder<CharType>::file_size_type
WinFileFinder<CharType>::GetFileSize() const noexcept
{
    return m_file_data.GetFileSize();
}

template<typename CharType>
typename WinFileFinder<CharType>::file_data_type const&
WinFileFinder<CharType>::GetFileData() const noexcept
{
    return m_file_data;
}

template<typename CharType>
DateTime64 WinFileFinder<CharType>::GetCreationDateTime() const noexcept
{
    return m_file_data.GetCreationDateTime();
}

template<typename CharType>
DateTime64 WinFileFinder<CharType>::GetLastAccessDateTime() const noexcept
{
    return m_file_data.GetLastAccessedDateTime();
}

template<typename CharType>
DateTime64 WinFileFinder<CharType>::GetLastModifiedDateTime() const noexcept
{
    return m_file_data.GetLastModifiedDateTime();
}

template<typename CharType>
bool WinFileFinder<CharType>::IsDirectory() const noexcept
{
    return m_file_data.IsDirectory();
}

template<typename CharType>
bool WinFileFinder<CharType>::IsDirectoryImportant() const noexcept
{
    return IsDirectory() && !IsUnimportantFolder(GetFilename());
}

template<typename CharType>
bool WinFileFinder<CharType>::IsDirectoryUnimportant() const noexcept
{
    return IsDirectory() && IsUnimportantFolder(GetFilename());
}

template<typename CharType>
bool WinFileFinder<CharType>::FindFirst(CharType const* full_path,
                                        CharType const* wild_card,
                                        HANDLE& find_handle,
                                        find_data_type& find_data) noexcept
{
    typedef PathUtility<char_type> path_utility_type;

    CharType* search_path = nullptr;
    path_utility_type::CombinePath(search_path, full_path, wild_card, BackSlashChar);
    find_handle = InternalFindFirstFile(search_path, find_data);
    path_utility_type::Free(search_path);
    bool success = find_handle != INVALID_HANDLE_VALUE;
    if (!success)
    {
        ::memset(&find_data, 0, sizeof(find_data));
        find_handle = NULL;
    }
    return success;
}

template<typename CharType>
bool WinFileFinder<CharType>::FindNext(HANDLE& find_handle, find_data_type& find_data) noexcept
{
    bool success = InternalFindNextFile(find_handle, find_data);
    if (!success)
    {
        ::memset(&find_data, 0, sizeof(find_data));
        find_handle = NULL;
    }
    return success;
}

template<typename CharType>
bool WinFileFinder<CharType>::CloseFindHandle(HANDLE& find_handle) noexcept
{
    bool success;
    if (find_handle != NULL)
    {
        success = ::FindClose(find_handle) != FALSE;
        find_handle = NULL;
    }
    else
        success = true;
    return success;
}

template<typename CharType>
DateTime64 WinFileFinder<CharType>::CovertDateTime(FILETIME file_time) noexcept
{
    SYSTEMTIME st;
    ::FileTimeToSystemTime(&file_time, &st);
    return DateTime64(static_cast<DateTime64::day_type>(st.wDay),
                      static_cast<DateTime64::month_type>(st.wMonth),
                      static_cast<DateTime64::year_type>(st.wYear),
                      static_cast<DateTime64::milliseconds_type>(st.wMilliseconds),
                      static_cast<DateTime64::seconds_type>(st.wSecond),
                      static_cast<DateTime64::minutes_type>(st.wMinute),
                      static_cast<DateTime64::hours_type>(st.wHour));
}

template<typename CharType>
void WinFileFinder<CharType>::Close()
{
    if (m_find_handle != NULL)
    {
        ::FindClose(m_find_handle);
        m_find_handle = NULL;
    }
}

template<typename CharType>
void WinFileFinder<CharType>::SetFileData(file_data_type& file_data,
                                          find_data_type const& find_data) noexcept
{
    typedef typename file_data_type::file_size_type file_size_type;

    file_data = static_cast<file_data_type&&>(file_data_type(
        find_data.cFileName,
        (static_cast<file_size_type>(find_data.nFileSizeHigh) << 32U) | find_data.nFileSizeLow,
        CovertDateTime(find_data.ftCreationTime),
        CovertDateTime(find_data.ftLastAccessTime),
        CovertDateTime(find_data.ftLastWriteTime),
        (find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0));
}

template<typename CharType>
HANDLE WinFileFinder<CharType>::InternalFindFirstFile(char const* search_path,
                                                      WIN32_FIND_DATAA& find_data)
{
    ::memset(&find_data, 0, sizeof(find_data));
    return ::FindFirstFileA(search_path, &find_data);
}

template<typename CharType>
HANDLE WinFileFinder<CharType>::InternalFindFirstFile(wchar_t const* search_path,
                                                      WIN32_FIND_DATAW& find_data)
{
    ::memset(&find_data, 0, sizeof(find_data));
    return ::FindFirstFileW(search_path, &find_data);
}

template<typename CharType>
bool WinFileFinder<CharType>::InternalFindNextFile(HANDLE find_handle,
                                                   WIN32_FIND_DATAA& find_data)
{
    ::memset(&find_data, 0, sizeof(find_data));
    return find_handle != NULL && ::FindNextFileA(find_handle, &find_data) != FALSE;
}

template<typename CharType>
bool WinFileFinder<CharType>::InternalFindNextFile(HANDLE find_handle,
                                                   WIN32_FIND_DATAW& find_data)
{
    ::memset(&find_data, 0, sizeof(find_data));
    return find_handle != NULL && ::FindNextFileW(find_handle, &find_data) != FALSE;
}

template<typename CharType>
bool WinFileFinder<CharType>::IsUnimportantFolder(CharType const* folder)
{
    switch (*folder)
    {
        case FullStopChar:
            switch (*(folder + 1))
            {
                case FullStopChar:
                    return *(folder + 2) == NullChar ? true : false;
                case NullChar:
                    return true;
            }
            return false;
        default:
            return false;
    }
}
