/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_PLATFORM_WIN_UNC_HPP
#define OCL_GUARD_PORTABLE_FILE_PLATFORM_WIN_UNC_HPP

#include "../../../common/CharConstants.hpp"
#include <string>

namespace ocl
{

template<typename CharType>
class UncUtility;


// helper functions for specialized UncUtility implementation.
// None of these functions validate pointers,
// and UncUtility class implementation is required to use them safely.
template<typename CharType>
class private_Unc
{
    friend class UncUtility<CharType>;

    typedef CharType char_type;
    typedef CharConstants<char_type> char_constants_type;

    static bool IsUNC(char_type const* path) noexcept
    {
        return (*path == char_constants_type::BackSlash) &&
               (*(path + 1) == char_constants_type::BackSlash);
    }

    static size_t StrLen(char_type const* str)
    {
        char_type const* end = str;
        while (*end != static_cast<char_type>(0))
            ++end;
        return static_cast<size_t>(end - str);
    }

    static void StrCat(char_type* dest,
                       size_t dest_size,
                       char_type const* str1,
                       size_t length1,
                       char_type const* str2,
                       size_t length2) noexcept
    {
        if (dest_size > length1 + length2)
        {
            ::memcpy(dest, str1, length1 * sizeof(char_type));
            ::memcpy(dest + length1, str2, length2 * sizeof(char_type));
            *(dest + length1 + length2) = char_constants_type::Null;
        }
    }
};

template<>
class UncUtility<char>
{
public:
    typedef char char_type;
    typedef CharConstants<char_type> char_constants_type;
    typedef private_Unc<char_type> private_unc_type;

    static constexpr char_type const UNC[] = "\\\\?\\";

    static size_t GetUNCLength() noexcept
    {
        static size_t const unc_len = ::strlen(UNC);
        return unc_len;
    }

    static bool IsUNC(char_type const* path) noexcept
    {
        return (path != nullptr) ? private_unc_type::IsUNC(path) : false;
    }

    // This function appends path to UNC
    static void GetUNCPath(char_type* unc_path,
                           size_t unc_path_size,
                           char_type const* path,
                           size_t path_length)
    {
        private_unc_type::StrCat(unc_path, unc_path_size, UNC, GetUNCLength(), path, path_length);
    }
};

template<>
class UncUtility<wchar_t>
{
public:
    typedef wchar_t char_type;
    typedef CharConstants<char_type> char_constants_type;
    typedef private_Unc<char_type> private_unc_type;

    static constexpr char_type const UNC[] = L"\\\\?\\";

    static size_t GetUNCLength() noexcept
    {
        static size_t const unc_len = ::wcslen(UNC);
        return unc_len;
    }

    static bool IsUNC(char_type const* path) noexcept
    {
        return private_unc_type::IsUNC(path);
    }

    // This function appends path to UNC
    static void GetUNCPath(char_type* unc_path,
                           size_t unc_path_size,
                           char_type const* path,
                           size_t path_length)
    {
        private_unc_type::StrCat(unc_path, unc_path_size, UNC, GetUNCLength(), path, path_length);
    }
};

template<>
class UncUtility<char16_t>
{
public:
    typedef char16_t char_type;
    typedef CharConstants<char_type> char_constants_type;
    typedef private_Unc<char_type> private_unc_type;

    static constexpr char_type const UNC[] = u"\\\\?\\";

    static size_t GetUNCLength() noexcept
    {
        static size_t const unc_len = private_unc_type::StrLen(UNC);
        return unc_len;
    }

    static bool IsUNC(char_type const* path) noexcept
    {
        return private_unc_type::IsUNC(path);
    }

    // This function appends path to UNC
    static void GetUNCPath(char_type* unc_path,
                           size_t unc_path_size,
                           char_type const* path,
                           size_t path_length)
    {
        private_unc_type::StrCat(unc_path, unc_path_size, UNC, GetUNCLength(), path, path_length);
    }
};

template<>
class UncUtility<char32_t>
{
public:
    typedef char32_t char_type;
    typedef CharConstants<char_type> char_constants_type;
    typedef private_Unc<char_type> private_unc_type;

    static constexpr char_type const UNC[] = U"\\\\?\\";

    static size_t GetUNCLength() noexcept
    {
        static size_t const unc_len = private_unc_type::StrLen(UNC);
        return unc_len;
    }

    static bool IsUNC(char_type const* path) noexcept
    {
        return private_unc_type::IsUNC(path);
    }

    // This function appends path to UNC
    static void GetUNCPath(char_type* unc_path,
                           size_t unc_path_size,
                           char_type const* path,
                           size_t path_length)
    {
        private_unc_type::StrCat(unc_path, unc_path_size, UNC, GetUNCLength(), path, path_length);
    }
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_FILE_PLATFORM_WIN_UNC_HPP
