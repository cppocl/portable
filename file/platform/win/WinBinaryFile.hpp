/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_PLATFORM_WIN_WIN_BINARYFILE_HPP
#define OCL_GUARD_PORTABLE_FILE_PLATFORM_WIN_WIN_BINARYFILE_HPP

#include "UncUtility.hpp"
#include <windows.h>
#include <string.h>
#include <cstddef>

namespace ocl
{

template<typename CharType>
class WinBinaryFile
{
// Types and constants.
public:
    typedef CharType char_type;
    typedef std::uint64_t size_type;
    typedef std::uint64_t seek_type;
    typedef DWORD error_type;
    typedef DWORD attribute_type;
    typedef DWORD mode_type;
    typedef DWORD behavioural_type;
    typedef DWORD security_type;

    static const size_type UNKNOWN_FILE_SIZE = ~static_cast<size_type>(0);

    /// *** File creational settings ***
    /// Only one of these open modes can be used.
    enum class OpenModes : mode_type
    {
        OpenExisting  = OPEN_EXISTING,     // Only open the file when the file exists.
        OpenTruncated = TRUNCATE_EXISTING, // Open an existing file and truncate the file to 0 size.
        OpenAlways    = OPEN_ALWAYS,       // Opens the file if it exists, otherwise the file is created.
        CreateNew     = CREATE_NEW,        // Only create file when a file with the same path and name doesn't exist.
        CreateAlways  = CREATE_ALWAYS      // Create the file when it does not exist, or overwrite and clear existing file.
    };

    /// *** File access ***
    /// These are bit values and can be OR'd together, or alternatively use
    /// the predefined OR'd enum values already provided.
    enum class AccessModes : mode_type
    {
        Read      = GENERIC_READ,
        Write     = GENERIC_WRITE,
        ReadWrite = Read | Write
    };

    /// *** Share settings ***
    /// These are bit values and can be OR'd together, or alternatively use
    /// the predefined OR'd enum values already provided.
    enum class ShareModes : mode_type
    {
        ShareNone            = 0,
        ShareRead            = 1,
        ShareWrite           = 2,
        ShareReadWrite       = ShareRead + ShareWrite,
        ShareDelete          = 4, // On Windows this is sharing delete and rename access.
        ShareReadDelete      = ShareRead + ShareDelete,
        ShareWriteDelete     = ShareWrite + ShareDelete,
        ShareReadWriteDelete = ShareRead + ShareWrite + ShareDelete
    };

    /// *** File Attributes ***
    /// These are bit values and can be OR'd together.
    /// These flags only have meaning when creating a new file.
    /// These are used as part of flags_and_attributes in the Open function.
    enum class Attributes : attribute_type
    {
        AttributeNone            = 0,
        AttributeReadOnly        = FILE_ATTRIBUTE_READONLY,
        AttributeHidden          = FILE_ATTRIBUTE_HIDDEN,
        AttributeSystem          = FILE_ATTRIBUTE_SYSTEM,
        AttributeArchive         = FILE_ATTRIBUTE_ARCHIVE,
        AttributeNormal          = FILE_ATTRIBUTE_NORMAL, // Cannot be set with any other attribute.
        AttributeTemporary       = FILE_ATTRIBUTE_TEMPORARY,
        AttributeOffline         = FILE_ATTRIBUTE_OFFLINE,
        AttributeEncrypted       = FILE_ATTRIBUTE_ENCRYPTED,

        // The following attributes cannot be used with function Open.
        AttributeFolder          = FILE_ATTRIBUTE_DIRECTORY,
        AttributeDevice          = FILE_ATTRIBUTE_DEVICE,
        AttributeIntegrityStream = FILE_ATTRIBUTE_INTEGRITY_STREAM,
        AttributeNotIndexed      = FILE_ATTRIBUTE_NOT_CONTENT_INDEXED,
        AttributeReparsePoint    = FILE_ATTRIBUTE_REPARSE_POINT,
        AttributeSparse          = FILE_ATTRIBUTE_SPARSE_FILE,
        AttributeVirtual         = FILE_ATTRIBUTE_VIRTUAL

        // Following attributes are not always defined within header.
        // AttributeNoScrubData     = FILE_ATTRIBUTE_NO_SCRUB_DATA,
        // AttributeRecallOnAccess  = FILE_ATTRIBUTE_RECALL_ON_DATA_ACCESS,
        // AttributeRecallOnOpen    = FILE_ATTRIBUTE_RECALL_ON_OPEN,
    };

    /// *** Behavioural settings ***
    /// Generic file bit values which can be OR'd together.
    /// Some of these flags only have meaning when creating a new file.
    /// These are used as part of flags_and_attributes in the Open function.

    enum class Behaviour : behavioural_type
    {
        // When 0, reads and writes use a default file caching scheme.
        BehaviourNone             = 0,

        // Retrieve remote file data without transporting the file to local storage.
        BehaviourOpenNoRecall     = static_cast<behavioural_type>(FILE_FLAG_OPEN_NO_RECALL),

        /// Windows supports creating files with reparse points, which allows user defined data.
        /// System filters and reparse tags are associated with reparse points.
        BehaviourOpenReparsePoint = static_cast<behavioural_type>(FILE_FLAG_OPEN_REPARSE_POINT),

        // Allow file names to be case sensitive.
        BehaviourPosixSemantics   = static_cast<behavioural_type>(FILE_FLAG_POSIX_SEMANTICS),

        /// Once all handles to the file are closed, the file is automatically deleted.
        BehaviourDeleteOnClose    = static_cast<behavioural_type>(FILE_FLAG_DELETE_ON_CLOSE),

        /// Hint to Windows how the file will be accessed. Ignored when using no buffering.
        BehaviourSequentialAccess = static_cast<behavioural_type>(FILE_FLAG_SEQUENTIAL_SCAN),

        /// Hint to Windows how the file will be accessed. Ignored when using no buffering.
        BehaviourRandomAccess     = static_cast<behavioural_type>(FILE_FLAG_RANDOM_ACCESS),

        /// The file is opened with no system caching for reads or writes.
        BehaviourNoBuffering      = static_cast<behavioural_type>(FILE_FLAG_NO_BUFFERING),

        /// See Microsoft documentation about overlapped I/O for more information.
        BehaviourAsynchronous     = static_cast<behavioural_type>(FILE_FLAG_OVERLAPPED),

        /// File content is immediately written to disk, bypassing any cache.
        BehaviourWriteThrough     = static_cast<behavioural_type>(FILE_FLAG_WRITE_THROUGH),

        // Behaviour not supported on Windows platform.
        BehaviourNoFollow         = BehaviourNone,
        BehaviourNoAccessTime     = BehaviourNone,
        BehaviourNoCtty           = BehaviourNone,
        BehaviourNonBlocking      = BehaviourNone,
        BehaviourDirect           = BehaviourNone,
        BehaviourSynchronous      = BehaviourNone,
        BehaviourFastSynchronous  = BehaviourNone,
    };

    /// *** Security settings ***
    /// Security bit values which some values can be OR'd together.
    /// Internally the SECURITY_SQOS_PRESENT bit will be set when
    /// using any of the security bits.
    /// These flags only have meaning when creating a new file.
    /// These are used as part of flags_and_attributes in the Open function.
    enum class Security : security_type
    {
        SecurityNone = 0,

        // Impersonate a client at the anonymous level.
        // Note that SECURITY_ANONYMOUS has the value 0,
        // which contradicts how the default behavior could possibly work.
        // According to the documentation, when SECURITY_SQOS_PRESENT is set
        // without any other security bits set, SECURITY_IMPERSONATION will
        // be used as the default, so it seems impossible to enable SECURITY_ANONYMOUS.
        // You can inspect WinBase.h to verify the values and this statement.
        SecurityAnonymous       = SECURITY_ANONYMOUS,

        // Impersonate a client at the delegation level.
        SecurityDelegation      = SECURITY_DELEGATION,

        // Impersonate a client at the identification level.
        SecurityIdentification  = SECURITY_IDENTIFICATION,

        // Impersonate a client at the impersonation level (this is the default).
        SecurityImpersonation   = SECURITY_IMPERSONATION,

        // When set security tracking is dynamic, otherwise it's static.
        SecurityContextTracking = SECURITY_CONTEXT_TRACKING,

        // Limits what client security is available to a server when set.
        SecurityEffectiveOnly   = SECURITY_EFFECTIVE_ONLY,

        // Security not supported on Windows platforms.
        SecurityUserRead              = SecurityNone,
        SecurityUserWrite             = SecurityNone,
        SecurityUserExecute           = SecurityNone,
        SecurityUserReadWrite         = SecurityNone,
        UserReadWriteExecute          = SecurityNone,
        SecurityGroupRead             = SecurityNone,
        SecurityGroupWrite            = SecurityNone,
        SecurityGroupExecute          = SecurityNone,
        SecurityGroupReadWrite        = SecurityNone,
        SecurityGroupReadWriteExecute = SecurityNone,
        SecurityOtherRead             = SecurityNone,
        SecurityOtherWrite            = SecurityNone,
        SecurityOtherReadWrite        = SecurityNone,
        SecurityOtherExecute          = SecurityNone,
        SecurityOtherReadWriteExecute = SecurityNone,
        SecuritySetUserIDBit          = SecurityNone,
        SecuritySetGroupIDBit         = SecurityNone
    };

private:
    typedef UncUtility<char_type> unc_utility_type;

public:
    WinBinaryFile();

    /// Opens or creates a file in exactly the same way as the Open function.
    /// If there is an error, then use GetLastError after construction to verify the
    /// file was opened or created without error.
    WinBinaryFile(char_type const* full_path,
                  OpenModes open_mode = OpenModes::OpenExisting,
                  AccessModes access_modes = AccessModes::ReadWrite,
                  ShareModes share_modes = ShareModes::ShareNone,
                  Attributes file_attributes = AttributeNormal,
                  Behaviour behavioural_modes = BehaviourNone,
                  Security security_modes = SecurityNone,
                  SECURITY_ATTRIBUTES* security_attributes = nullptr,
                  HANDLE template_file = NULL);

    // Any open file handle will be automatically closed on destruction.
    ~WinBinaryFile();

public:
    /// Open a file for binary access.
    bool Open(char_type const* full_path,
              OpenModes open_mode = OpenModes::OpenExisting,
              AccessModes access_modes = AccessModes::ReadWrite,
              ShareModes share_modes = ShareModes::ShareNone,
              Attributes file_attributes = AttributeNormal,
              Behaviour behavioural_modes = BehaviourNone,
              Security security_modes = SecurityNone,
              SECURITY_ATTRIBUTES* security_attributes = nullptr,
              HANDLE template_file = NULL);

    bool IsOpen() const noexcept;

    /// Get the file size for an open file.
    /// IsOpen can be used to check if the file is open before calling GetFileSize.
    /// If there is an error then UNKNOWN_FILE_SIZE is returned,
    /// which is the maximum size for a 64-bit unsigned integer.
    size_type GetFileSize() const;

    /// Read part or all of the file into a buffer provided.
    /// When Read returns false, check for the last error using GetLastError.
    /// If the file has not been opened then the last error will be ERROR_INVALID_HANDLE.
    /// If the Asynchronous flag is set but overlapped in null
    /// then the last error will be ERROR_NOT_SUPPORTED.
    bool Read(unsigned char* buffer,
              size_type number_of_bytes,
              LPOVERLAPPED overlapped = nullptr,
              LPOVERLAPPED_COMPLETION_ROUTINE completion_func = nullptr);

    /// Same as unsigned char* version of Read.
    bool Read(char* buffer,
              size_type number_of_bytes,
              LPOVERLAPPED overlapped = nullptr,
              LPOVERLAPPED_COMPLETION_ROUTINE completion_func = nullptr);

    /// Write the buffer to the file at the current file position.
    /// If the buffer is greater than the the end of the file,
    /// the file will grow in size.
    /// If the function returns false then use GetLastError to find the cause.
    bool Write(unsigned char const* buffer,
               size_type number_of_bytes,
               LPOVERLAPPED overlapped = nullptr,
               LPOVERLAPPED_COMPLETION_ROUTINE completion_func = nullptr);

    /// Same as unsigned char const* version of Write.
    bool Write(char const* buffer,
               size_type number_of_bytes,
               LPOVERLAPPED overlapped = nullptr,
               LPOVERLAPPED_COMPLETION_ROUTINE completion_func = nullptr);

    /// Resize the file, either truncating the file content or leaving
    /// any file data in the increased size as undefined.
    bool SetFileSize(size_type file_size);

    /// Move file pointer to an absolute position.
    bool Seek(seek_type position);

    /// Seek count bytes from the end of the file towards the front.
    bool SeekFromEnd(seek_type count);

    /// Set file pointer to the start of the file.
    bool SeekToStart();

    /// Set file pointer to the end of the file.
    bool SeekToEnd();

    /// Flush writes to disk.
    bool Flush();

    /// Close the file handle.
    bool Close();

    error_type GetLastError() const noexcept;

// Static helper functions.
public:
    static bool Exists(char_type const* full_path);

    static size_type GetFileSize(char_type const* full_path);

    /// Get the file attributes or return INVALID_FILE_ATTRIBUTES if the function fails.
    static Attributes GetFileAttributes(char_type const* full_path);

    /// Get the file attributes or return INVALID_FILE_ATTRIBUTES if the function fails.
    static bool GetFileAttributes(char_type const* full_path,
                                  WIN32_FILE_ATTRIBUTE_DATA& attribute_data);

    static bool DeleteFile(char_type const* full_path, error_type& last_error);

// Constants (internal use only)
private:
    static const DWORD MAX_DWORD = static_cast<DWORD>(0xffffffff);

// Helper functions (internal use only)
private:
    // Helper for returning valid flags and attributes for the 3 combined values.
    static DWORD GetFlagsAndAttributes(Attributes file_attributes,             // File attributes OR'd
                                       Behaviour behavioural_modes,            // BehaviouralModes OR'd
                                       Security security_modes) noexcept; // SecurityModes OR'd

    static HANDLE InternalOpenFile(char const* path,
                                   DWORD access_mode,
                                   DWORD share_modes,
                                   SECURITY_ATTRIBUTES* security_attributes,
                                   DWORD creation,
                                   DWORD flags_and_attributes,
                                   HANDLE template_file);

    static HANDLE InternalOpenFile(wchar_t const* path,
                                   DWORD access_mode,
                                   DWORD share_modes,
                                   SECURITY_ATTRIBUTES* security_attributes,
                                   DWORD creation,
                                   DWORD flags_and_attributes,
                                   HANDLE template_file);

    static bool InternalSeek(HANDLE file_handle,
                             seek_type positon,
                             DWORD whence,
                             error_type& last_error);

    static DWORD InternalGetFileAttributes(char const* full_path);

    static DWORD InternalGetFileAttributes(wchar_t const* full_path);

    static bool InternalGetFileAttributes(char const* full_path,
                                          WIN32_FILE_ATTRIBUTE_DATA& attribute_data);

    static bool InternalGetFileAttributes(wchar_t const* full_path,
                                          WIN32_FILE_ATTRIBUTE_DATA& attribute_data);

    static bool InternalDeleteFile(char const* full_path);

    static bool InternalDeleteFile(wchar_t const* full_path);

    static char_type* InternalMakeUNCPath(char_type const* path);

    static size_t StrLen(char const* str)
    {
        return ::strlen(str);
    }

    static size_t StrLen(wchar_t const* str)
    {
        return ::wcslen(str);
    }

// Data (internal use only)
private:
    HANDLE m_file_handle;
    error_type m_last_error;
    DWORD m_flags_and_attributes;
};

#include "WinBinaryFile.inl"

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_FILE_PLATFORM_WIN_WIN_BINARYFILE_HPP
