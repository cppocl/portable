/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_PLATFORM_POSIX_POSIXFILEFINDER_HPP
#define OCL_GUARD_PORTABLE_FILE_PLATFORM_POSIX_POSIXFILEFINDER_HPP

#include "../../../common/CharConstants.hpp"
#include "../../../common/DateTime64.hpp"
#include "../../../common/FileData.hpp"
#include "../../../common/StringCopy.hpp"
#include "../../../common/StringUtility.hpp"
#include "../../../common/PathUtility.hpp"
#include "../../../common/MemoryUtility.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <cstring>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cerrno>

namespace ocl
{

/// Provide the ability to find files using a find first/next.
template<typename CharType>
class PosixFileFinder
{
// Types and constants.
public:
    typedef CharType char_type;
    typedef int error_type;
    typedef DIR* dir_type;
    typedef CharConstants<CharType> char_constants_type;
    typedef FileData<CharType> file_data_type;
    typedef typename file_data_type::file_size_type file_size_type;

    static char_type const NullChar         = char_constants_type::Null;
    static char_type const ForwardSlashChar = char_constants_type::ForwardSlash;
    static char_type const FullStopChar     = char_constants_type::FullStop;
    static char_type const SeparatorChar    = ForwardSlashChar;

// Construction and destruction.
public:
    /// The default constructor can be used, if you don't need to use Parse and
    // you don't want to provide a functor.
    PosixFileFinder() noexcept;

    // Any handles will be closed on destruction of the PosixFileFinder object.
    ~PosixFileFinder() noexcept;

// Interface functions.
public:
    /// Return true if FindFirst or FindNext still has something to do.
    bool IsFinding() const noexcept;

    /// Search for the first matching file from a full Windows path and a wild card search.
    /// GetFindData will return the file data for the match.
    bool FindFirst(CharType const* full_path,
                   CharType const* wild_card = nullptr) noexcept;

    /// Find the next matching file specified by FindFirst.
    /// GetFindData will return the file data for the match.
    bool FindNext() noexcept;

    /// When a function fails the last error code can be obtained
    /// from calling function GetLastError.
    error_type GetLastError() const noexcept;

    /// Return true when FindFirst or FindNext returned an error.
    bool HasLastError() const noexcept;

    /// Get the current file name after FindFirst or FindNext.
    char_type const* GetFilename() const noexcept;

    /// Get the 64-bit file size of the current file for FindFirst or FindNext.
    file_size_type GetFileSize() const noexcept;

    /// Get file data for first or next found file.
    file_data_type const& GetFileData() const noexcept;

    /// Get Date/Time the file was created.
    DateTime64 GetCreationDateTime() const noexcept;

    /// Get the last time the file was read, modified or executed.
    DateTime64 GetLastAccessDateTime() const noexcept;

    /// Get the last time the file was modified or overwritten.
    DateTime64 GetLastModifiedDateTime() const noexcept;

    /// Return if file is a directory.
    bool IsDirectory() const noexcept;

    /// Return true if the directory name is not "." or "..".
    bool IsDirectoryImportant() const noexcept;

    /// Return true if the directory name is "." or "..".
    bool IsDirectoryUnimportant() const noexcept;

    /// Note: Closing the handle at the first possible opportunity will ensure other
    /// processes can access the files and folders.
    void Close() noexcept;

    // Helper functions (internal use only)
private:
    typedef StringUtility<char_type> string_utility_type;
    typedef StringCopy<char_type>    string_copy_type;
    typedef MemoryUtility<char_type> memory_utility_type;

    /// Treat . and .. as unimportant folders.
    static bool IsUnimportantFolder(CharType const* folder) noexcept;

    /// Convert Win32 find data structure to file data structure.
    static bool SetFileData(file_data_type& file_data, dirent* ent) noexcept;

    /// Read the next directory entry after opendir call.
    static dirent* InternalReadDir(DIR* dir) noexcept;

    /// Copy string, allowing wchar_t to be overloaded for the same function.
    static bool CopyString(char*& dest, char const* source) noexcept;

    /// Copy string, converting characters to wide char.
    static bool CopyString(wchar_t*& dest, char const* source) noexcept;

// Data (internal use only)
private:
    /// Handle used for find first and find next function calls.
    dir_type m_dir;

    /// Store last error from FindFirstFile or FindLastFile;
    int m_last_error;

    /// Each file found is stored in the file data structure.
    file_data_type m_file_data;
};

// Pull in the implementation for the member functions.
#include "PosixFileFinder.inl"

} // namespace ocl

#endif /*OCL_GUARD_PORTABLE_FILE_PLATFORM_POSIX_POSIXFILEFINDER_HPP*/
