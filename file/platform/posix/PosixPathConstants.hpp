/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the 'License');
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an 'AS IS' BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_PLATFORM_POSIX_POSIXPATHCONSTANTS_HPP
#define OCL_GUARD_PORTABLE_FILE_PLATFORM_POSIX_POSIXPATHCONSTANTS_HPP

#include "../../../common/CharConstants.hpp"

namespace ocl
{

template<typename CharType>
class PosixPathConstants
{
public:
    typedef CharType char_type;

public:
    static char_type const PathSeparatorChar = '/';
    static char_type const PathDriveChar     = '\0';
    static char_type const PrimaryDriveChar  = '\0';
    static char_type const NullChar          = CharConstants<char_type>::Null;

    static char_type const* GetRootPath() noexcept
    {
        static constexpr char_type const root[2] =
        {
            PathSeparatorChar, NullChar
        };

        return root;
    }

    static constexpr char_type const* GetAllFilesWildCard() noexcept
    {
        return "*";
    }
};

template<>
class PosixPathConstants<wchar_t>
{
public:
    typedef wchar_t char_type;

public:
    static char_type const PathSeparatorChar = L'/';
    static char_type const PathDriveChar     = L'\0';
    static char_type const PrimaryDriveChar  = L'\0';
    static char_type const NullChar          = CharConstants<char_type>::Null;

    static char_type const* GetRootPath() noexcept
    {
        static constexpr char_type const root[2] =
        {
            PathSeparatorChar, NullChar
        };

        return root;
    }

    static constexpr char_type const* GetAllFilesWildCard() noexcept
    {
        return L"*";
    }
};

template<>
class PosixPathConstants<char16_t>
{
public:
    typedef char16_t char_type;

public:
    static char_type const PathSeparatorChar = u'/';
    static char_type const PathDriveChar     = u'\0';
    static char_type const PrimaryDriveChar  = u'\0';
    static char_type const NullChar          = CharConstants<char_type>::Null;

    static char_type const* GetRootPath() noexcept
    {
        static constexpr char_type const root[2] =
        {
            PathSeparatorChar, NullChar
        };

        return root;
    }

    static constexpr char_type const* GetAllFilesWildCard() noexcept
    {
        return u"*";
    }
};

template<>
class PosixPathConstants<char32_t>
{
public:
    typedef char32_t char_type;

public:
    static char_type const PathSeparatorChar = U'/';
    static char_type const PathDriveChar     = U'\0';
    static char_type const PrimaryDriveChar  = U'\0';
    static char_type const NullChar          = CharConstants<char_type>::Null;

    static char_type const* GetRootPath() noexcept
    {
        static constexpr char_type const root[2] =
        {
            PathSeparatorChar, NullChar
        };

        return root;
    }

    static constexpr char_type const* GetAllFilesWildCard() noexcept
    {
        return U"*";
    }
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_FILE_PLATFORM_POSIX_POSIXPATHCONSTANTS_HPP
