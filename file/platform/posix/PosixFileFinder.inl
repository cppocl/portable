/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License")
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType>
PosixFileFinder<CharType>::PosixFileFinder() noexcept
    : m_dir(nullptr)
    , m_last_error(0)
{
}

template<typename CharType>
PosixFileFinder<CharType>::~PosixFileFinder() noexcept
{
    Close();
}

template<typename CharType>
bool PosixFileFinder<CharType>::IsFinding() const noexcept
{
    return m_dir != nullptr;
}

template<typename CharType>
bool PosixFileFinder<CharType>::FindFirst(CharType const* full_path,
                                          CharType const* wild_card) noexcept
{
    Close();

    errno = 0;
    m_dir = opendir(full_path);

    if (m_dir != nullptr)
    {
        dirent* ent = InternalReadDir(m_dir);
        if (SetFileData(m_file_data, ent))
            return true;
        Close();
    }

    return false;
}

template<typename CharType>
bool PosixFileFinder<CharType>::FindNext() noexcept
{
    if (m_dir != nullptr)
    {
        dirent* ent = InternalReadDir(m_dir);
        if (SetFileData(m_file_data, ent))
            return true;
        Close();
    }
    return false;
}

template<typename CharType>
typename PosixFileFinder<CharType>::error_type
PosixFileFinder<CharType>::GetLastError() const noexcept
{
    return m_last_error;
}

template<typename CharType>
bool PosixFileFinder<CharType>::HasLastError() const noexcept
{
    return m_last_error != 0;
}

template<typename CharType>
CharType const* PosixFileFinder<CharType>::GetFilename() const noexcept
{
    return m_file_data.GetFilename();
}

template<typename CharType>
typename PosixFileFinder<CharType>::file_size_type
PosixFileFinder<CharType>::GetFileSize() const noexcept
{
    return m_file_data.GetFileSize();
}

template<typename CharType>
typename PosixFileFinder<CharType>::file_data_type const&
PosixFileFinder<CharType>::GetFileData() const noexcept
{
    return m_file_data;
}

template<typename CharType>
DateTime64 PosixFileFinder<CharType>::GetCreationDateTime() const noexcept
{
    return m_file_data.GetCreationDateTime();
}

template<typename CharType>
DateTime64 PosixFileFinder<CharType>::GetLastAccessDateTime() const noexcept
{
    return m_file_data.GetLastAccessedDateTime();
}

template<typename CharType>
DateTime64 PosixFileFinder<CharType>::GetLastModifiedDateTime() const noexcept
{
    return m_file_data.GetLastModifiedDateTime();
}

template<typename CharType>
bool PosixFileFinder<CharType>::IsDirectory() const noexcept
{
    return m_file_data.IsDirectory();
}

template<typename CharType>
bool PosixFileFinder<CharType>::IsDirectoryImportant() const noexcept
{
    return IsDirectory() && !IsUnimportantFolder(GetFilename());
}

template<typename CharType>
bool PosixFileFinder<CharType>::IsDirectoryUnimportant() const noexcept
{
    return IsDirectory() && IsUnimportantFolder(GetFilename());
}

template<typename CharType>
void PosixFileFinder<CharType>::Close() noexcept
{
    if (m_dir != nullptr)
    {
        closedir(m_dir);
        m_dir = nullptr;
    }
}

template<typename CharType>
bool PosixFileFinder<CharType>::IsUnimportantFolder(CharType const* folder) noexcept
{
    switch (*folder)
    {
    case FullStopChar:
        switch (*(folder + 1))
        {
        case FullStopChar:
            return *(folder + 2) == NullChar ? true : false;
        case NullChar:
            return true;
        }
        return false;
    default:
        return false;
    }
}

template<typename CharType>
bool PosixFileFinder<CharType>::SetFileData(file_data_type& file_data, dirent* ent) noexcept
{
    char_type* filename = nullptr;

    if (ent != nullptr && CopyString(filename, ent->d_name))
    {
        typedef typename file_data_type::file_size_type file_size_type;
        file_size_type file_size = 0;
        DateTime64 creation_date_time;
        DateTime64 last_accessed_date_time;
        DateTime64 last_modified_date_time;
        bool is_directory = ent->d_type != DT_DIR;

        if (ent->d_type == DT_REG || ent->d_type == DT_LNK)
        {
            struct stat st;
            stat(ent->d_name, &st);
            file_size = static_cast<file_size_type>(st.st_size);
        }

        file_data = static_cast<file_data_type&&>(file_data_type(
            static_cast<CharType*&&>(filename),
            static_cast<file_size_type&&>(file_size),
            static_cast<DateTime64&&>(creation_date_time),
            static_cast<DateTime64&&>(last_accessed_date_time),
            static_cast<DateTime64&&>(last_modified_date_time),
            static_cast<bool&&>(is_directory)));

        return true;
    }

    return false;
}

template<typename CharType>
dirent* PosixFileFinder<CharType>::InternalReadDir(DIR* dir) noexcept
{
    dirent* ent = nullptr;
    if (dir != nullptr)
    {
        errno = 0;
        ent = readdir(dir);
    }
    return ent;
}

template<typename CharType>
bool PosixFileFinder<CharType>::CopyString(char*& dest, char const* source) noexcept
{
    string_copy_type::AllocCopy(dest, source);
    return dest != nullptr;
}

template<typename CharType>
bool PosixFileFinder<CharType>::CopyString(wchar_t*& dest, char const* source) noexcept
{
    typename string_utility_type::size_type len = string_utility_type::GetLength(source);
    if (len > 0)
    {
        dest = memory_utility_type::Allocate(len + 1);
        if (dest != nullptr)
            mbstowcs(dest, source, len + 1);
    }
    else
        memory_utility_type::Free(dest);
    return dest != nullptr;
}
