/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType>
PosixBinaryFile<CharType>::PosixBinaryFile()
    : m_file_descriptor(BAD_FILE_DESCRIPTOR)
    , m_last_error(0)
{
}

template<typename CharType>
PosixBinaryFile<CharType>::PosixBinaryFile(char_type const* full_path,
                                           OpenModes open_mode,
                                           AccessModes access_modes,
                                           ShareModes share_modes,
                                           Attributes file_attributes,
                                           Behaviour behavioural_modes,
                                           Security security_modes)
    : m_file_descriptor(BAD_FILE_DESCRIPTOR)
    , m_last_error(0)
{
    (void)Open(full_path,
               open_mode,
               access_modes,
               share_modes,
               file_attributes,
               behavioural_modes,
               security_modes);
}

template<typename CharType>
PosixBinaryFile<CharType>::~PosixBinaryFile()
{
    if (IsOpen())
        (void)Close();
}

template<typename CharType>
bool PosixBinaryFile<CharType>::Open(char_type const* full_path,
                                     OpenModes open_mode,
                                     AccessModes access_modes,
                                     ShareModes share_modes,
                                     Attributes file_attributes,
                                     Behaviour behavioural_modes,
                                     Security security_modes)
{
    int flags = static_cast<int>(open_mode) |
                static_cast<int>(access_modes) |
                static_cast<int>(share_modes) |
                static_cast<int>(file_attributes) |
                static_cast<int>(behavioural_modes);

    mode_t modes = static_cast<mode_t>(security_modes);

    if (m_file_descriptor == BAD_FILE_DESCRIPTOR)
        m_file_descriptor = InternalOpenFile(full_path, flags, modes, m_last_error);
    else
        m_last_error = EBADF; // File handle already open.

    return m_file_descriptor != BAD_FILE_DESCRIPTOR;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::IsOpen() const noexcept
{
    return m_file_descriptor != BAD_FILE_DESCRIPTOR;
}

template<typename CharType>
typename PosixBinaryFile<CharType>::size_type PosixBinaryFile<CharType>::GetFileSize() const
{
    size_type file_size;

    if (m_file_descriptor != BAD_FILE_DESCRIPTOR)
    {
        off_t curr_pos = ::lseek(m_file_descriptor, 0, SEEK_CUR);

        if (curr_pos != static_cast<off_t>(-1))
        {
            // Get file size then restore position back to original position.
            off_t end_file_pos = ::lseek(m_file_descriptor, 0, SEEK_END);

            if ((end_file_pos == static_cast<off_t>(-1)) ||
                (::lseek(m_file_descriptor, curr_pos, SEEK_SET) == static_cast<off_t>(-1)))
            {
                file_size = GetUnknownFileSize();
            }
            else
                file_size = static_cast<size_type>(end_file_pos);
        }
        else
            file_size = GetUnknownFileSize();
    }
    else
        file_size = GetUnknownFileSize();

    return file_size;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::Read(unsigned char* buffer, size_type number_of_bytes)
{
    bool success;

    static size_t const MAX_BLOCK_SIZE = GetMaxBlockSize();

    if (m_file_descriptor != BAD_FILE_DESCRIPTOR)
    {
        ssize_t ret = 0;

        while (number_of_bytes > 0)
        {
            size_t block_bytes = (number_of_bytes > MAX_BLOCK_SIZE)
                                 ? MAX_BLOCK_SIZE
                                 : static_cast<size_t>(number_of_bytes);

            ret = ::read(m_file_descriptor, buffer, block_bytes);

            if (ret == -1)
                break;

            if ((ret > 0) && (number_of_bytes > block_bytes))
                number_of_bytes -= block_bytes;
            else
                number_of_bytes = 0;
        }

        success = ret != -1;
        m_last_error = success ? 0 : errno;
    }
    else
    {
        // Cannot read without a file handle.
        success = false;
        m_last_error = EBADF;
    }

    return success;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::Read(char* buffer, size_type number_of_bytes)
{
    return Read(reinterpret_cast<unsigned char*>(buffer), number_of_bytes);
}

template<typename CharType>
bool PosixBinaryFile<CharType>::Write(unsigned char const* buffer, size_type number_of_bytes)
{
    bool success;

    static size_t const MAX_BLOCK_SIZE = GetMaxBlockSize();

    if (m_file_descriptor != BAD_FILE_DESCRIPTOR)
    {
        ssize_t ret = 0;

        while (number_of_bytes > 0)
        {
            size_t block_bytes = (number_of_bytes > MAX_BLOCK_SIZE)
                                 ? MAX_BLOCK_SIZE
                                 : static_cast<size_t>(number_of_bytes);

            ret = ::write(m_file_descriptor, buffer, block_bytes);

            if (ret == -1)
                break;

            if ((ret > 0) && (number_of_bytes > block_bytes))
                number_of_bytes -= block_bytes;
            else
                number_of_bytes = 0;
        }

        success = ret != -1;
        m_last_error = success ? 0 : errno;
    }
    else
    {
        // Cannot write without a file handle.
        success = false;
        m_last_error = EBADF;
    }

    return success;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::Write(char const* buffer, size_type number_of_bytes)
{
    return Write(reinterpret_cast<unsigned char const*>(buffer), number_of_bytes);
}

template<typename CharType>
bool PosixBinaryFile<CharType>::SetFileSize(size_type file_size)
{
    bool success;

    static size_t const MAX_BLOCK_SIZE = GetMaxBlockSize();

    if ((file_size > 0) && (file_size <= static_cast<size_type>(~static_cast<off_t>(0))))
    {
        if (m_file_descriptor != BAD_FILE_DESCRIPTOR)
        {
            off_t curr_file_size = ::lseek(m_file_descriptor, 0, SEEK_END);

            if (curr_file_size >= 0)
            {
                if (static_cast<off_t>(file_size) > curr_file_size)
                {
                    off_t new_file_size = ::lseek(m_file_descriptor,
                                                      static_cast<off_t>(file_size - 1),
                                                      SEEK_END);
                    success = new_file_size >= 0;

                    if (success)
                    {
                        // Size of file is only adjusted after writing after the seek.
                        unsigned char buf[1] = { 0U };
                        success = ::write(m_file_descriptor, buf, 1) != -1;
                    }

                    if (success)
                        m_last_error = 0;
                    else
                        m_last_error = new_file_size != -1 ? ENOTSUP : errno;
                }
                else if (static_cast<off_t>(file_size) < curr_file_size)
                {
#if defined(_XOPEN_SOURCE) && (_XOPEN_SOURCE >= 500)
                    success = ftruncate(m_file_descriptor, static_cast<off_t>(file_size)) == 0;
                    m_last_error = success ? 0 : errno;
#else
                    // ftruncate is not supported for earlier glibc versions,
                    // mark as operation not supported.
                    success = false;
                    m_last_error = ENOTSUP;
#endif
                }
                else
                    success = true; // No change.
            }
            else
            {
                // Unexpected failure, mark as operation not supported or errno.
                success = false;
                m_last_error = (curr_file_size != -1) ? ENOTSUP : errno;
            }
        }
        else
        {
            // Cannot set file size without a file handle.
            success = false;
            m_last_error = EBADF;
        }
    }
    else
    {
        // Requested file size exceeds limit of off_t, so report file too large.
        success = false;
        m_last_error = EFBIG;
    }

    return success;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::Seek(seek_type position)
{
    return InternalSeek(m_file_descriptor, position, SEEK_SET, m_last_error);
}

template<typename CharType>
bool PosixBinaryFile<CharType>::SeekFromEnd(seek_type count)
{
    return InternalSeek(m_file_descriptor, count, SEEK_END, m_last_error);
}

template<typename CharType>
bool PosixBinaryFile<CharType>::SeekToStart()
{
    return InternalSeek(m_file_descriptor, 0, SEEK_SET, m_last_error);
}

template<typename CharType>
bool PosixBinaryFile<CharType>::SeekToEnd()
{
    return InternalSeek(m_file_descriptor, 0, SEEK_END, m_last_error);
}

template<typename CharType>
bool PosixBinaryFile<CharType>::Flush()
{
    return false;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::Close()
{
    bool success;

    if (m_file_descriptor != BAD_FILE_DESCRIPTOR)
    {
        if (::close(m_file_descriptor) == 0)
        {
            m_file_descriptor = BAD_FILE_DESCRIPTOR;
            m_last_error = 0;
            success = true;
        }
        else
        {
            m_last_error = errno;
            success = false;
        }
    }
    else
    {
        // Cannot read without a file handle.
        m_last_error = EBADF;
        success = false;
    }

    return success;
}

template<typename CharType>
typename PosixBinaryFile<CharType>::error_type
PosixBinaryFile<CharType>::GetLastError() const noexcept
{
    return m_last_error;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::Exists(char_type const* full_path)
{
    struct stat statbuf;
    return FileStat::Stat(full_path, &statbuf) == 0;
}

template<typename CharType>
typename PosixBinaryFile<CharType>::size_type
PosixBinaryFile<CharType>::GetFileSize(char_type const* full_path)
{
    size_type file_size;
    struct stat statbuf;
    if ((FileStat::Stat(full_path, &statbuf) == 0) && (statbuf.st_size >= 0))
    {
        bool const can_fit_size =
            (sizeof(size_type) >= sizeof(off_t)) ||
            (statbuf.st_size <= static_cast<off_t>(~static_cast<size_type>(0)));

        file_size = can_fit_size ? static_cast<size_type>(statbuf.st_size) : GetUnknownFileSize();
    }
    else
        file_size = GetUnknownFileSize();

    return file_size;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::DeleteFile(char_type const* full_path, error_type& last_error)
{
    return InternalDeleteFile(full_path, last_error);
}

template<typename CharType>
typename PosixBinaryFile<CharType>::file_descriptor_type
PosixBinaryFile<CharType>::InternalOpenFile(char const* path,
                                       int flags,
                                       mode_t modes,
                                       error_type& last_error)
{
    file_descriptor_type fd = ::open(path, flags, modes);
    last_error = (fd != BAD_FILE_DESCRIPTOR) ? 0 : errno;
    return fd;
}

template<typename CharType>
typename PosixBinaryFile<CharType>::file_descriptor_type
PosixBinaryFile<CharType>::InternalOpenFile(wchar_t const* path,
                                       int flags,
                                       mode_t modes,
                                       error_type& last_error)
{
    char* multi_byte_path = InternalToMultiByte(path);
    file_descriptor_type fd = ::open(multi_byte_path, flags, modes);
    last_error = (fd != BAD_FILE_DESCRIPTOR) ? 0 : errno;
    std::free(multi_byte_path);
    return fd;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::InternalSeek(file_descriptor_type file_descriptor,
                                        off_t position,
                                        int whence,
                                        error_type& last_error)
{
    bool success = ::lseek(file_descriptor, position, whence) != static_cast<off_t>(-1);
    last_error = success ? 0 : errno;
    return success;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::InternalDeleteFile(char const* full_path, error_type& last_error)
{
    bool success = remove(full_path) == 0;
    last_error = success ? 0 : errno;
    return success;
}

template<typename CharType>
bool PosixBinaryFile<CharType>::InternalDeleteFile(wchar_t const* full_path, error_type& last_error)
{
    char* multi_byte_path = InternalToMultiByte(full_path);
    bool success = multi_byte_path != nullptr && remove(multi_byte_path) == 0;
    last_error = success ? 0 : errno;
    std::free(multi_byte_path);
    return success;
}

template<typename CharType>
char* PosixBinaryFile<CharType>::InternalToMultiByte(wchar_t const* src)
{
    mbstate_t state;
    memset(&state, 0, sizeof(state));
    size_t byte_count = 1 + wcsrtombs(nullptr, &src, 0, &state);
    char* dest = static_cast<char*>(std::malloc(byte_count + 1));
    if (dest == nullptr || wcsrtombs(dest, &src, byte_count, &state) == static_cast<size_t>(-1))
    {
        std::free(dest);
        dest = nullptr;
    }

    return dest;
}

