/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

namespace ocl
{

struct PosixSecurityFlags
{
    typedef int security_type;

    static security_type const SecurityNone = 0;

    // User permissions:
    static security_type const SecurityUserRead =
#ifdef S_IRUSR
        S_IRUSR;
#else
        0x0400;
#endif

    static security_type const SecurityUserWrite =
#ifdef S_IWUSR
        S_IWUSR;
#else
        0x0200;
#endif

    static security_type const SecurityUserExecute =
#ifdef S_IXUSR
        S_IXUSR;
#else
        0x0100;
#endif

    static security_type const SecurityUserReadWrite = SecurityUserRead | SecurityUserWrite;

    static security_type const UserReadWriteExecute =
#ifdef S_IRWXU
        S_IRWXU;
#else
        SecurityUserRead | SecurityUserWrite | SecurityUserExecute;
#endif

    static security_type const SecurityGroupRead =
#ifdef S_IRGRP
        S_IRGRP;
#else
        0x0040;
#endif

    // Group permissions:
    static security_type const SecurityGroupWrite =
#ifdef S_IWGRP
        S_IWGRP;
#else
        0x0020;
#endif

    static security_type const SecurityGroupExecute =
#ifdef S_IXGRP
        S_IXGRP;
#else
        0x0010;
#endif

    static security_type const SecurityGroupReadWrite = SecurityGroupRead | SecurityGroupWrite;

    static security_type const SecurityGroupReadWriteExecute =
#ifdef S_IRWXG
        S_IRWXG;
#else
        SecurityGroupRead | SecurityGroupWrite | SecurityGroupExecute;
#endif

    // Other permissions:
    static security_type const SecurityOtherRead =
#ifdef S_IROTH
        S_IROTH;
#else
        0x0004;
#endif

    static security_type const SecurityOtherWrite =
#ifdef S_IWOTH
        S_IWOTH;
#else
        0x0002;
#endif

    static security_type const SecurityOtherReadWrite = SecurityOtherRead | SecurityOtherWrite;

    static security_type const SecurityOtherExecute =
#ifdef S_IXOTH
        S_IXOTH;
#else
        0x0001;
#endif

    static security_type const SecurityOtherReadWriteExecute =
#ifdef S_IRWXG
        S_IRWXO;
#else
        SecurityGroupRead | SecurityGroupWrite | SecurityGroupExecute;
#endif

    // Other security modes (those not in group)
    static security_type const SecuritySetUserIDBit =
#ifdef S_ISUID
        S_ISUID;
#else
        0x4000;
#endif

    static security_type const SecuritySetGroupIDBit =
#ifdef S_ISGID
        S_ISGID;
#else
        0x2000;
#endif

    // When the sticky bit is set on a directory, files in the directory
    // can only be renamed or deleted by the owner of the file, owner
    // of the directory or a privileged process.
    static security_type const SecurityStickyBit =
#ifdef S_ISVTX
        S_ISVTX;
#else
        0x1000;
#endif
};

} // namespace ocl

