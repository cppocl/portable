/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_PLATFORM_POSIX_FILESTAT_HPP
#define OCL_GUARD_PORTABLE_FILE_PLATFORM_POSIX_FILESTAT_HPP

#include <cstdlib>
#include <cstring>
#include <cwchar>

namespace ocl
{

struct FileStat
{
    static int Stat(char const* full_path, struct stat* statbuf)
    {
        return stat(full_path, statbuf);
    }

    static int Stat(wchar_t const* full_path, struct stat* statbuf)
    {
        std::size_t bytes = (wcslen(full_path) + 1) * 4;
        char* path = static_cast<char*>(std::malloc(bytes));
        wcstombs(path, full_path, bytes);
        int ret = stat(path, statbuf);
        std::free(path);
        return ret;
    }
};

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_FILE_PLATFORM_POSIX_FILESTAT_HPP
