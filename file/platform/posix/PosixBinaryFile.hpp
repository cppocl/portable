/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_PLATFORM_POSIX_FILE_POSIXBINARYFILE_HPP
#define OCL_GUARD_PORTABLE_PLATFORM_POSIX_FILE_POSIXBINARYFILE_HPP

#ifndef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS 64
#endif

#ifndef _LARGEFILE64_SOURCE
#define _LARGEFILE64_SOURCE
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <limits>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <fcntl.h>
#include <wchar.h>
#include <unistd.h>
#include "PosixSecurityFlags.hpp"
#include "FileStat.hpp"

/*
    WARNING: This version of PosixBinaryFile is not expected to compile.
             Development is continuing for Posix compliant solution.
*/

namespace ocl
{

template<typename CharType>
class PosixBinaryFile
{
// Types and constants.
public:
    typedef CharType char_type;
    typedef int mode_type;
    typedef int behavioural_type;
    typedef PosixSecurityFlags::security_type security_type;
    typedef int attribute_type;
    typedef std::uint64_t size_type;
    typedef off_t seek_type;
    typedef int error_type;

    static const size_type UNKNOWN_FILE_SIZE = ~static_cast<size_type>(0);

    /// *** File creational settings ***
    /// Only one of these open modes can be used.
    enum class OpenModes : mode_type
    {
        OpenExisting  = 0,              // Only open the file when the file exists.
        OpenTruncated = O_TRUNC,        // Open an existing file and truncate the file to 0 size.
        OpenAlways    = O_CREAT,        // Opens the file if it exists, otherwise the file is created.
        CreateNew     = O_CREAT|O_EXCL, // Only create file when a file with the same path and name doesn't exist.
        CreateAlways  = O_CREAT|O_TRUNC // Create the file when it does not exist, or overwrite and clear existing file.
    };

    /// *** File access ***
    /// Only one of these access modes can be used.
    enum class AccessModes : mode_type
    {
        Read      = O_RDONLY,
        Write     = O_WRONLY,
        ReadWrite = O_RDWR
    };

    /// *** Share settings ***
    /// Only one of these share modes can be used.
    enum class ShareModes : mode_type
    {
        // File sharing not supported by open for regular files.
        ShareNone = 0,
        ShareRead = ShareNone,
        ShareWrite = ShareNone,
        ShareReadWrite = ShareNone,
        ShareDelete = ShareNone,
        ShareReadDelete = ShareNone,
        ShareWriteDelete = ShareNone,
        ShareReadWriteDelete = ShareNone
    };

    /// *** File Attributes ***
    /// These are bit values and can be OR'd together.
    /// These flags only have meaning when creating a new file.
    /// These are used as part of flags_and_attributes in the Open function.
    enum class Attributes : attribute_type
    {
        AttributeNone            = 0,
        AttributeNormal          = 0,

        // Create an unnamed file in the specified folder, which is deleted on close.
        AttributeTemporary       =
#ifdef O_TMPFILE
            O_TMPFILE,
#else
            AttributeNone,
#endif

        // The following attribute cannot be used with function Open.
        AttributeFolder          =
#ifdef O_DIRECTORY
            O_DIRECTORY,
#else
            AttributeNone,
#endif

        // Attributes not supported for Posix platforms.
        AttributeReadOnly        = AttributeNone,
        AttributeHidden          = AttributeNone,
        AttributeSystem          = AttributeNone,
        AttributeArchive         = AttributeNone,
        AttributeOffline         = AttributeNone,
        AttributeEncrypted       = AttributeNone,
        AttributeDevice          = AttributeNone,
        AttributeIntegrityStream = AttributeNone,
        AttributeNotIndexed      = AttributeNone,
        AttributeReparsePoint    = AttributeNone,
        AttributeSparse          = AttributeNone,
        AttributeVirtual         = AttributeNone
    };

    /// *** Behavioural settings ***
    /// Generic file bit values which can be OR'd together.
    /// Some of these flags only have meaning when creating a new file.
    /// These are used as part of flags_and_attributes in the Open function.
    enum class Behaviour : behavioural_type
    {
        /// When 0, reads and writes use a default file caching scheme.
        BehaviourNone = 0,

        /// Symbolic links will fail to open.
        BehaviourNoFollow =
#ifdef O_NOFOLLOW
            O_NOFOLLOW,
#else
            BehaviourNone,
#endif

        /// The access time is not modified. May not work on all file systems, i.e. NFS.
        BehaviourNoAccessTime =
#ifdef O_NOATIME
            O_NOATIME,
#else
            BehaviourNone,
#endif

        /// Don't allow process to control terminal.
        BehaviourNoCtty =
#ifdef O_NOCTTY
            O_NOCTTY,
#else
            BehaviourNone,
#endif

        /// Open and other operations will not cause the calling process to wait.
        BehaviourNonBlocking =
#ifdef O_NONBLOCK
            O_NONBLOCK,
#else
0,
#endif

        /// Enable signal driven I/O. Not available for regular files.
        BehaviourAsynchronous =
#ifdef O_ASYNC
            O_ASYNC,
#else
            BehaviourNone,
#endif

        // Minimize the use of cache for I/O. There are alignment restrictions on
        // the memory address and length of memory when using direct access.
        BehaviourDirect =
#ifdef O_DIRECT
            O_DIRECT,
#else
            BehaviourNone,
#endif

        // Force writes to complete to the hardware level.
        BehaviourSynchronous =
#ifdef O_SYNC
            O_SYNC,
#else
            BehaviourNone,
#endif

        // Force writes to complete to the hardware level, but faster ad this
        // operation won't immediately write timestamp metadata.
        BehaviourFastSynchronous =
#ifdef O_DSYNC
            O_DSYNC,
#else
            BehaviourNone,
#endif

        /// File content is immediately written to disk, bypassing any cache.
        BehaviourWriteThrough =
#if defined(O_DIRECT) && defined(O_SYNC)
            O_DIRECT | O_SYNC,
#else
            BehaviourNone,
#endif

        // Behaviour not supported on Posix platforms.
        BehaviourOpenNoRecall     = BehaviourNone,
        BehaviourOpenReparsePoint = BehaviourNone,
        BehaviourPosixSemantics   = BehaviourNone,
        BehaviourDeleteOnClose    = BehaviourNone,
        BehaviourSequentialAccess = BehaviourNone,
        BehaviourRandomAccess     = BehaviourNone,
        BehaviourNoBuffering      = BehaviourNone
    };

    /// *** Security settings ***
    /// Security bit values which some values can be OR'd together.
    /// Internally the SECURITY_SQOS_PRESENT bit will be set when
    /// using any of the security bits.
    /// These flags only have meaning when creating a new file.
    /// These are used as part of flags_and_attributes in the Open function.
    enum class Security : security_type
    {
        SecurityNone                  = PosixSecurityFlags::SecurityNone,
        SecurityUserRead              = PosixSecurityFlags::SecurityUserRead,
        SecurityUserWrite             = PosixSecurityFlags::SecurityUserWrite,
        SecurityUserExecute           = PosixSecurityFlags::SecurityUserExecute,
        SecurityUserReadWrite         = PosixSecurityFlags::SecurityUserReadWrite,
        UserReadWriteExecute          = PosixSecurityFlags::UserReadWriteExecute,
        SecurityGroupRead             = PosixSecurityFlags::SecurityGroupRead,
        SecurityGroupWrite            = PosixSecurityFlags::SecurityGroupWrite,
        SecurityGroupExecute          = PosixSecurityFlags::SecurityGroupExecute,
        SecurityGroupReadWrite        = PosixSecurityFlags::SecurityGroupReadWrite,
        SecurityGroupReadWriteExecute = PosixSecurityFlags::SecurityGroupReadWriteExecute,
        SecurityOtherRead             = PosixSecurityFlags::SecurityOtherRead,
        SecurityOtherWrite            = PosixSecurityFlags::SecurityOtherWrite,
        SecurityOtherReadWrite        = PosixSecurityFlags::SecurityOtherReadWrite,
        SecurityOtherExecute          = PosixSecurityFlags::SecurityOtherExecute,
        SecurityOtherReadWriteExecute = PosixSecurityFlags::SecurityOtherReadWriteExecute,
        SecuritySetUserIDBit          = PosixSecurityFlags::SecuritySetUserIDBit,
        SecuritySetGroupIDBit         = PosixSecurityFlags::SecuritySetGroupIDBit,

        // Security not supported on Posix platforms.
        SecurityAnonymous             = SecurityNone,
        SecurityDelegation            = SecurityNone,
        SecurityIdentification        = SecurityNone,
        SecurityImpersonation         = SecurityNone,
        SecurityContextTracking       = SecurityNone,
        SecurityEffectiveOnly         = SecurityNone
    };

    // File descriptor returned from open is -1 when invalid.
    static int const BAD_FILE_DESCRIPTOR = -1;

    static constexpr size_type GetUnknownFileSize() noexcept
    {
        return ~static_cast<size_type>(0);
    }

public:
    PosixBinaryFile();

    /// Opens or creates a file in exactly the same way as the Open function.
    /// If there is an error, then use GetLastError after construction to verify the
    /// file was opened or created without error.
    PosixBinaryFile(char_type const* full_path,
                    OpenModes open_mode = OpenModes::OpenExisting,
                    AccessModes access_modes = AccessModes::ReadWrite,
                    ShareModes share_modes = ShareModes::ShareNone,
                    Attributes file_attributes = Attributes::AttributeNormal,
                    Behaviour behavioural_modes = Behaviour::BehaviourNone,
                    Security security_modes = Security::SecurityNone);

    // Any open file handle will be automatically closed on destruction.
    ~PosixBinaryFile();

public:
    /// Open a file for binary access.
    bool Open(char_type const* full_path,
              OpenModes open_mode = OpenModes::OpenExisting,
              AccessModes access_modes = AccessModes::ReadWrite,
              ShareModes share_modes = ShareModes::ShareNone,
              Attributes file_attributes = Attributes::AttributeNormal,
              Behaviour behavioural_modes = Behaviour::BehaviourNone,
              Security security_modes = Security::SecurityNone);

    bool IsOpen() const noexcept;

    /// Get the file size for an open file.
    /// IsOpen can be used to check if the file is open before calling GetFileSize.
    /// If there is an error then UNKNOWN_FILE_SIZE is returned,
    /// which is the maximum size for a 64-bit unsigned integer.
    /// The internal file position will remain the same after the function call ends,
    /// unless there was an error, the the file position will be unknown.
    size_type GetFileSize() const;

    /// Read part or all of the file into a buffer provided.
    /// When Read returns false, check for the last error using GetLastError.
    /// If the file has not been opened then the last error will be ERROR_INVALID_HANDLE.
    /// If the Asynchronous flag is set but overlapped in null
    /// then the last error will be ERROR_NOT_SUPPORTED.
    bool Read(unsigned char* buffer, size_type number_of_bytes);
    bool Read(char* buffer, size_type number_of_bytes);

    bool Write(unsigned char const* buffer, size_type number_of_bytes);
    bool Write(char const* buffer, size_type number_of_bytes);

    bool SetFileSize(size_type file_size);

    bool Seek(seek_type position);

    /// Seek count bytes from the end of the file towards the front.
    bool SeekFromEnd(seek_type count);

    bool SeekToStart();

    bool SeekToEnd();

    bool Flush();

    bool Close();

    error_type GetLastError() const noexcept;

// Static helper functions.
public:
    static bool Exists(char_type const* full_path);

    static size_type GetFileSize(char_type const* full_path);

    static bool DeleteFile(char_type const* full_path, error_type& last_error);

    // Types and constants (internal use only)
private:
    typedef int file_descriptor_type;

// Helper functions (internal use only)
private:
    static file_descriptor_type InternalOpenFile(char const* path,
                                                 int flags,
                                                 mode_t modes,
                                                 error_type& last_error);

    static file_descriptor_type InternalOpenFile(wchar_t const* path,
                                                 int flags,
                                                 mode_t modes,
                                                 error_type& last_error);

    static bool InternalSeek(file_descriptor_type file_descriptor,
                             off_t position,
                             int whence,
                             error_type& last_error);

    static bool InternalDeleteFile(char const* full_path, error_type& last_error);

    static bool InternalDeleteFile(wchar_t const* full_path, error_type& last_error);

    static char* InternalToMultiByte(wchar_t const* src);

    /// Get maximum block size for size_t, which is required for reading and writing from/to a file.
    static constexpr size_t GetMaxBlockSize() noexcept
    {
        return sizeof(size_type) >= sizeof(size_t)
                    ? std::numeric_limits<size_t>::max()
                    : static_cast<size_t>(std::numeric_limits<size_type>::max());
    }

// Data (internal use only)
private:
    file_descriptor_type m_file_descriptor;
    error_type m_last_error;
};

#include "PosixBinaryFile.inl"

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_PLATFORM_POSIX_FILE_POSIXBINARYFILE_HPP
