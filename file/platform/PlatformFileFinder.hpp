/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_PLATFORM_PLATFORMFILEFINDER_HPP
#define OCL_GUARD_PORTABLE_FILE_PLATFORM_PLATFORMFILEFINDER_HPP

#include "../../common/PlatformDefines.hpp"

#if OCL_PLATFORM == OCL_PLATFORM_WINDOWS
    #if OCL_COMPILER_POSIX_SUPPORT == 0
    #include "win/WinFileFinder.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformFileFinder = WinFileFinder<char_type>;
    }
    #else // OCL_COMPILER_POSIX_SUPPORT != 0
    #include "posix/PosixFileFinder.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformFileFinder = PosixFileFinder<char_type>;
    }
    #endif
#elif OCL_PLATFORM == OCL_PLATFORM_LINUX
    #include "linux/LinuxFileFinder.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformFileFinder = LinuxFileFinder<char_type>;
    }
#elif OCL_PLATFORM == OCL_PLATFORM_UNIX
    #include "unix/UnixFileFinder.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformFileFinder = UnixFileFinder<char_type>;
    }
#elif OCL_PLATFORM == OCL_PLATFORM_BSD
    #include "bsd/BsdFileFinder.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformFileFinder = BsdFileFinder<char_type>;
    }
#elif OCL_PLATFORM == OCL_PLATFORM_MAC
    #include "linux/MacFileFinder.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformFileFinder = MacFileFinder<char_type>;
    }
#elif OCL_PLATFORM == OCL_PLATFORM_ANDROID
    #include "android/AndroidFileFinder.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformFileFinder = AndroidFileFinder<char_type>;
    }
#else
    // If platform cannot be detected then attempt the Posix solution as a generic alternative.
    #include "posix/PosixFileFinder.hpp"
    namespace ocl
    {
        template<typename char_type>
        using PlatformFileFinder = PosixFileFinder<char_type>;
    }
#endif

#endif /*OCL_GUARD_PORTABLE_FILE_PLATFORM_PLATFORMFILEFINDER_HPP*/
