/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_FILEFINDER_HPP
#define OCL_GUARD_PORTABLE_FILE_FILEFINDER_HPP

#include "platform/PlatformFileFinder.hpp"

namespace ocl
{

template <typename CharType>
class FileFinder
{
public:
    typedef CharType char_type;
    typedef FileData<char_type> file_data_type;

    static char_type const NullChar      = PlatformFileFinder<char_type>::NullChar;
    static char_type const FullStopChar  = PlatformFileFinder<char_type>::FullStopChar;
    static char_type const SeparatorChar = PlatformFileFinder<char_type>::SeparatorChar;

public:
    FileFinder();

    /// Return true if FindFirst or FindNext still has something to do.
    bool IsFinding() const noexcept;

    /// Search for the first matching file from a full Windows path and a wild card search.
    /// GetFindData will return the file data for the match.
    bool FindFirst(CharType const* full_path,
                   CharType const* wild_card = nullptr) noexcept;

    /// Find the next matching file specified by FindFirst.
    /// GetFindData will return the file data for the match.
    bool FindNext() noexcept;

    /// When a function fails the last error code can be obtained
    /// from calling function GetLastError.
    bool HasLastError() const noexcept;

    /// Get the latest file data for the last call to FindFirst or FindNext.
    file_data_type const& GetFileData() const noexcept;

    /// Get the current file name after FindFirst or FindNext.
    char_type const* GetFilename() const noexcept;

    /// Get the 64-bit file size of the current file for FindFirst or FindNext.
    std::uint64_t GetFileSize() const noexcept;

    /// Get Date/Time the file was created.
    DateTime64 GetCreationDateTime() const noexcept;

    /// Get the last time the file was read, modified or executed.
    DateTime64 GetLastAccessDateTime() const noexcept;

    /// Get the last time the file was modified or overwritten.
    DateTime64 GetLastModifiedDateTime() const noexcept;

    /// Return if file is a directory.
    bool IsDirectory() const noexcept;

    /// Return true if the directory name is not "." or "..".
    bool IsDirectoryImportant() const noexcept;

    /// Return true if the directory name is not "." or "..".
    bool IsDirectoryUnimportant() const noexcept;

    /// Close the file finder handle and free any internal memory.
    /// NOTE: FindFirst and the destructor calls this automatically.
    void Close();

private:
    PlatformFileFinder<char_type> m_file_finder;
};

#include "internal/FileFinder.inl"

} // namespace ocl

#endif /*OCL_GUARD_PORTABLE_FILE_FILEFINDER_HPP*/
