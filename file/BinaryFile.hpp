/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_FILE_BINARYFILE_HPP
#define OCL_GUARD_PORTABLE_FILE_BINARYFILE_HPP

// Include the platform specific BinaryFile class.
#include "platform/PlatformBinaryFile.hpp"
#include "../common/EnumBitwiseOperations.hpp"

namespace ocl
{

template<typename CharType>
class BinaryFile
{
// Types and constants.
public:
    typedef PlatformBinaryFile<CharType> binary_file_type;

    typedef typename binary_file_type::char_type        char_type;
    typedef typename binary_file_type::size_type        size_type;
    typedef typename binary_file_type::seek_type        seek_type;
    typedef typename binary_file_type::error_type       error_type;
    typedef typename binary_file_type::attribute_type   attribute_type;
    typedef typename binary_file_type::mode_type        mode_type;
    typedef typename binary_file_type::behavioural_type behavioural_type;
    typedef typename binary_file_type::security_type    security_type;

    typedef typename binary_file_type::OpenModes   OpenModes;
    typedef typename binary_file_type::AccessModes AccessModes;
    typedef typename binary_file_type::ShareModes  ShareModes;
    typedef typename binary_file_type::Attributes  Attributes;
    typedef typename binary_file_type::Behaviour   Behaviour;
    typedef typename binary_file_type::Security    Security;

    static const size_type UNKNOWN_FILE_SIZE = binary_file_type::UNKNOWN_FILE_SIZE;

public:
    BinaryFile();

    /// Opens or creates a file in exactly the same way as the Open function.
    /// If there is an error, then use GetLastError after construction to verify the
    /// file was opened or created without error.
    BinaryFile(char_type const* full_path,
               OpenModes open_mode = OpenModes::OpenExisting,
               AccessModes access_modes = AccessModes::ReadWrite,
               ShareModes share_modes = ShareModes::ShareNone,
               Attributes file_attributes = Attributes::AttributeNormal,
               Behaviour behavioural_modes = Behaviour::BehaviourNone,
               Security security_modes = Security::SecurityNone);

    // Any open file handle will be automatically closed on destruction.
    ~BinaryFile();

public:
    /// Open a file for binary access.
    bool Open(char_type const* full_path,
              OpenModes open_mode = OpenModes::OpenExisting,
              AccessModes access_modes = AccessModes::ReadWrite,
              ShareModes share_modes = ShareModes::ShareNone,
              Attributes file_attributes = Attributes::AttributeNormal,
              Behaviour behavioural_modes = Behaviour::BehaviourNone,
              Security security_modes = Security::SecurityNone);

    bool IsOpen() const noexcept;

    /// Get the file size for an open file.
    /// IsOpen can be used to check if the file is open before calling GetFileSize.
    /// If there is an error then UNKNOWN_FILE_SIZE is returned,
    /// which is the maximum size for a 64-bit unsigned integer.
    size_type GetFileSize() const;

    /// Read part or all of the file into a buffer provided.
    /// When Read returns false, check for the last error using GetLastError.
    /// If the file has not been opened then the last error will be ERROR_INVALID_HANDLE.
    /// If the Asynchronous flag is set but overlapped in null
    /// then the last error will be ERROR_NOT_SUPPORTED.
    bool Read(unsigned char* buffer, size_type number_of_bytes);

    /// Same as unsigned char* version of Read.
    bool Read(char* buffer, size_type number_of_bytes);

    /// Write the buffer to the file at the current file position.
    /// If the buffer is greater than the the end of the file,
    /// the file will grow in size.
    /// If the function returns false then use GetLastError to find the cause.
    bool Write(unsigned char const* buffer, size_type number_of_bytes);

    /// Same as unsigned char const* version of Write.
    bool Write(char const* buffer, size_type number_of_bytes);

    /// Resize the file, either truncating the file content or leaving
    /// any file data in the increased size as undefined.
    bool SetFileSize(size_type file_size);

    /// Move file pointer to an absolute position.
    bool Seek(seek_type position);

    /// Seek count bytes from the end of the file towards the front.
    bool SeekFromEnd(seek_type count);

    /// Set file pointer to the start of the file.
    bool SeekToStart();

    /// Set file pointer to the end of the file.
    bool SeekToEnd();

    /// Flush writes to disk.
    bool Flush();

    /// Close the file handle.
    bool Close();

    error_type GetLastError() const noexcept;

// Static helper functions.
public:
    static bool Exists(char_type const* full_path);

    static size_type GetFileSize(char_type const* full_path);

    static bool DeleteFile(char_type const* full_path, error_type& last_error);

    template<typename FlagType>
    static constexpr bool Supported(FlagType value) noexcept
    {
        return value != static_cast<FlagType>(0);
    }

// Data (internal use only)
private:
    binary_file_type m_binary_file;
};

#include "internal/BinaryFile.inl"

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_FILE_BINARYFILE_HPP
