/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template <typename CharType>
FileFinder<CharType>::FileFinder()
{
}

template <typename CharType>
bool FileFinder<CharType>::IsFinding() const noexcept
{
    return m_file_finder.IsFinding();
}

template <typename CharType>
bool FileFinder<CharType>::FindFirst(CharType const* full_path,
                                     CharType const* wild_card) noexcept
{
    return m_file_finder.FindFirst(full_path, wild_card);
}

template <typename CharType>
bool FileFinder<CharType>::FindNext() noexcept
{
    return m_file_finder.FindNext();
}

template <typename CharType>
bool FileFinder<CharType>::HasLastError() const noexcept
{
    return m_file_finder.HasLastError();
}

template <typename CharType>
typename FileFinder<CharType>::file_data_type const&
FileFinder<CharType>::GetFileData() const noexcept
{
    return m_file_finder.GetFileData();
}

template <typename CharType>
CharType const* FileFinder<CharType>::GetFilename() const noexcept
{
    return m_file_finder.GetFilename();
}

template <typename CharType>
std::uint64_t FileFinder<CharType>::GetFileSize() const noexcept
{
    return m_file_finder.GetFileSize();
}

template <typename CharType>
DateTime64 FileFinder<CharType>::GetCreationDateTime() const noexcept
{
    return m_file_finder.GetCreationDateTime();
}

template <typename CharType>
DateTime64 FileFinder<CharType>::GetLastAccessDateTime() const noexcept
{
    return m_file_finder.GetCreationDateTime();
}

template <typename CharType>
DateTime64 FileFinder<CharType>::GetLastModifiedDateTime() const noexcept
{
    return m_file_finder.GetLastModifiedDateTime();
}

template <typename CharType>
bool FileFinder<CharType>::IsDirectory() const noexcept
{
    return m_file_finder.IsDirectory();
}

template <typename CharType>
bool FileFinder<CharType>::IsDirectoryImportant() const noexcept
{
    return m_file_finder.IsDirectoryImportant();
}

template <typename CharType>
bool FileFinder<CharType>::IsDirectoryUnimportant() const noexcept
{
    return m_file_finder.IsDirectoryUnimportant();
}

template <typename CharType>
void FileFinder<CharType>::Close()
{
    m_file_finder.Close();
}
