/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template <typename CharType>
RecursiveFileFinder<CharType>::RecursiveFileFinder(std::size_t depth)
    : m_parent(nullptr)
    , m_child(nullptr)
    , m_wild_card(nullptr)
    , m_full_path(nullptr)
    , m_depth(depth)
{
    m_current = this;
}

template <typename CharType>
RecursiveFileFinder<CharType>::~RecursiveFileFinder()
{
    Close();
}

template <typename CharType>
bool RecursiveFileFinder<CharType>::IsFinding() const noexcept
{
    return m_file_finder.IsFinding();
}

template <typename CharType>
bool RecursiveFileFinder<CharType>::FindFirst(CharType const* full_path,
                                              CharType const* wild_card) noexcept
{
    m_current = this;

    // The caller of FindFirst will only ever call this once,
    // before m_child is created.
    string_copy_type::AllocCopy(m_full_path, full_path);
    string_copy_type::AllocCopy(m_wild_card, wild_card);

    /// There is no need to call FindFirst via m_current->m_file_finder, as any internal call to FindFirst
    /// will have already located the correct RecursiveFileFinder reference, before calling FindFirst.
    return m_file_finder.FindFirst(full_path, wild_card);
}

template <typename CharType>
bool RecursiveFileFinder<CharType>::FindNext() noexcept
{
    /// Get recursive file finder first, so the file finder
    /// does not call GetLastChild again.
    bool success = false;

    if (m_current->IsRecursiveFind())
        success = RecursiveFindFirst();

    if (!success)
    {
        success = m_current->m_file_finder.FindNext();
        while (!success)
        {
            if (m_current->m_parent != nullptr)
            {
                recursive_file_finder_type* parent = m_current->m_parent;
                m_current->Close();
                delete m_current;
                m_current = parent;
                m_current->m_child = nullptr;
                success = m_current->m_file_finder.FindNext();
            }
            else
                break;
        }
    }

    return success;
}

template <typename CharType>
bool RecursiveFileFinder<CharType>::HasLastError() const noexcept
{
    return m_file_finder.HasLastError();
}

template <typename CharType>
typename RecursiveFileFinder<CharType>::file_data_type const&
RecursiveFileFinder<CharType>::GetFileData() const noexcept
{
    return m_current->m_file_finder.GetFileData();
}

template <typename CharType>
CharType const* RecursiveFileFinder<CharType>::GetFilename() const noexcept
{
    return m_current->m_file_finder.GetFilename();
}

template <typename CharType>
std::uint64_t RecursiveFileFinder<CharType>::GetFileSize() const noexcept
{
    return m_current->m_file_finder.GetFileSize();
}

template <typename CharType>
DateTime64 RecursiveFileFinder<CharType>::GetCreationDateTime() const noexcept
{
    return m_current->m_file_finder.GetCreationDateTime();
}

template <typename CharType>
DateTime64 RecursiveFileFinder<CharType>::GetLastAccessDateTime() const noexcept
{
    return m_current->m_file_finder.GetCreationDateTime();
}

template <typename CharType>
DateTime64 RecursiveFileFinder<CharType>::GetLastModifiedDateTime() const noexcept
{
    return m_current->m_file_finder.GetLastModifiedDateTime();
}

template <typename CharType>
bool RecursiveFileFinder<CharType>::IsDirectory() const noexcept
{
    return m_current->m_file_finder.IsDirectory();
}

template <typename CharType>
bool RecursiveFileFinder<CharType>::IsDirectoryImportant() const noexcept
{
    return m_current->m_file_finder.IsDirectoryImportant();
}

template <typename CharType>
bool RecursiveFileFinder<CharType>::IsDirectoryUnimportant() const noexcept
{
    return m_current->m_file_finder.IsDirectoryUnimportant();
}

template <typename CharType>
bool RecursiveFileFinder<CharType>::IsRecursiveFind() const noexcept
{
    return IsDirectoryImportant() && m_current->GetDepth() > 0;
}

template <typename CharType>
void RecursiveFileFinder<CharType>::Close()
{
    string_copy_type::Free(m_wild_card);
    string_copy_type::Free(m_full_path);
    delete m_child;
    m_child = nullptr;
    m_current = this;
}

template<typename CharType>
std::size_t RecursiveFileFinder<CharType>::GetDepth() const noexcept
{
    return m_depth;
}

template<typename CharType>
bool RecursiveFileFinder<CharType>::RecursiveFindFirst() noexcept
{
    bool success = false;

    if (m_current->m_child == nullptr)
    {
        recursive_file_finder_type* child =
            new (std::nothrow) recursive_file_finder_type(m_current->GetDepth() - 1);

        if (child != nullptr)
        {
            char_type* full_path = nullptr;
            file_data_type const& file_data = GetFileData();
            CombinePath(full_path, GetCurrentPath(), file_data.GetFilename());
            if (full_path != nullptr && child->FindFirst(full_path, m_wild_card))
            {
                m_current->m_child = child;
                child->m_parent = m_current;
                m_current = child;
                success = true;
                path_utility_type::Free(full_path);
            }
            else
                delete child;
        }
    }

    return success;
}

template<typename CharType>
CharType const* RecursiveFileFinder<CharType>::GetFullPath() const noexcept
{
    return m_full_path;
}

template<typename CharType>
CharType const* RecursiveFileFinder<CharType>::GetCurrentPath() const noexcept
{
    return m_current->m_full_path;
}

template<typename CharType>
CharType const* RecursiveFileFinder<CharType>::GetWildCard() const noexcept
{
    return m_wild_card;
}

template<typename CharType>
RecursiveFileFinder<CharType>&
RecursiveFileFinder<CharType>::GetLastChild() const noexcept
{
    return m_child != nullptr ? m_child->GetLastChild() : *this;
}

template<typename CharType>
RecursiveFileFinder<CharType>*
RecursiveFileFinder<CharType>::GetChild() noexcept
{
    return m_current->m_child;
}

template<typename CharType>
RecursiveFileFinder<CharType> const*
RecursiveFileFinder<CharType>::GetChild() const noexcept
{
    return m_current->m_child;
}

template<typename CharType>
RecursiveFileFinder<CharType>*
RecursiveFileFinder<CharType>::GetParent() noexcept
{
    return m_current->m_parent;
}

template<typename CharType>
RecursiveFileFinder<CharType> const*
RecursiveFileFinder<CharType>::GetParent() const noexcept
{
    return m_current->m_parent;
}

template <typename CharType>
void RecursiveFileFinder<CharType>::CombinePath(char_type*& combined_path,
                                                char_type const* path1,
                                                char_type const* path2) noexcept
{
    path_utility_type::CombinePath(combined_path, path1, path2, SeparatorChar);
}
