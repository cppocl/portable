/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType>
BinaryFile<CharType>::BinaryFile()
{
}

template<typename CharType>
BinaryFile<CharType>::BinaryFile(char_type const* full_path,
                                 OpenModes open_mode,
                                 AccessModes access_modes,
                                 ShareModes share_modes,
                                 Attributes file_attributes,
                                 Behaviour behavioural_modes,
                                 Security security_modes)
    : m_binary_file(full_path,
                    open_mode,
                    access_modes,
                    share_modes,
                    file_attributes,
                    behavioural_modes,
                    security_modes)
{
}

template<typename CharType>
BinaryFile<CharType>::~BinaryFile()
{
    if (IsOpen())
        (void)Close();
}

template<typename CharType>
bool BinaryFile<CharType>::Open(char_type const* full_path,
                                OpenModes open_mode,
                                AccessModes access_modes,
                                ShareModes share_modes,
                                Attributes file_attributes,
                                Behaviour behavioural_modes,
                                Security security_modes)
{
    return m_binary_file.Open(full_path,
                              open_mode,
                              access_modes,
                              share_modes,
                              file_attributes,
                              behavioural_modes,
                              security_modes);
}

template<typename CharType>
bool BinaryFile<CharType>::IsOpen() const noexcept
{
    return m_binary_file.IsOpen();
}

template<typename CharType>
typename BinaryFile<CharType>::size_type BinaryFile<CharType>::GetFileSize() const
{
    return m_binary_file.GetFileSize();
}

template<typename CharType>
bool BinaryFile<CharType>::Read(unsigned char* buffer, size_type number_of_bytes)
{
    return m_binary_file.Read(buffer, number_of_bytes);
}

template<typename CharType>
bool BinaryFile<CharType>::Read(char* buffer, size_type number_of_bytes)
{
    return m_binary_file.Read(buffer, number_of_bytes);
}

template<typename CharType>
bool BinaryFile<CharType>::Write(unsigned char const* buffer, size_type number_of_bytes)
{
    return m_binary_file.Write(buffer, number_of_bytes);
}

template<typename CharType>
bool BinaryFile<CharType>::Write(char const* buffer, size_type number_of_bytes)
{
    return m_binary_file.Write(buffer, number_of_bytes);
}

template<typename CharType>
bool BinaryFile<CharType>::SetFileSize(size_type file_size)
{
    return m_binary_file.SetFileSize(file_size);
}

template<typename CharType>
bool BinaryFile<CharType>::Seek(seek_type position)
{
    return m_binary_file.Seek(position);
}

template<typename CharType>
bool BinaryFile<CharType>::SeekFromEnd(seek_type count)
{
    return m_binary_file.SeekFromEnd(count);
}

template<typename CharType>
bool BinaryFile<CharType>::SeekToStart()
{
    return m_binary_file.SeekToStart();
}

template<typename CharType>
bool BinaryFile<CharType>::SeekToEnd()
{
    return m_binary_file.SeekToEnd();
}

template<typename CharType>
bool BinaryFile<CharType>::Flush()
{
    return m_binary_file.Flush();
}

template<typename CharType>
bool BinaryFile<CharType>::Close()
{
    return m_binary_file.Close();
}

template<typename CharType>
typename BinaryFile<CharType>::error_type
BinaryFile<CharType>::GetLastError() const noexcept
{
    return m_binary_file.GetLastError();
}

template<typename CharType>
bool BinaryFile<CharType>::Exists(char_type const* full_path)
{
    return binary_file_type::Exists(full_path);
}

template<typename CharType>
typename BinaryFile<CharType>::size_type
BinaryFile<CharType>::GetFileSize(char_type const* full_path)
{
    return binary_file_type::GetFileSize(full_path);
}

template<typename CharType>
bool BinaryFile<CharType>::DeleteFile(char_type const* full_path, error_type& last_error)
{
    return binary_file_type::DeleteFile(full_path, last_error);
}
