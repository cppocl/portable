# C++ portable framework

![](header_image.jpg)

## Overview

C++ open source porting framework for Windows, Linux, Mac and Android

Currently under development.

Will work with Microsoft Visual C++, gcc, and Posix compliant compilers.
MigGW and CygWin support is theoretical only.

The portable code is developed to allow all source files to be compiled on any platform,
there is no need to have any special build configuration, or pick out specific files for a platform.
Each platform specific source code file is guarded against being compiled on the wrong platform.

## Follow

Telegram messenger: https://t.me/cppocl

## Code Examples

### Example of using fopen, fread, fclose, etc.
```cpp
#include <cstdio>

int main()
{
    char const* path = "temp_data.txt";
    FILE* f = fopen(path, "r");

    // Move to end of file and get position to calculate size, then go back to start.
    // These file operations treat the file as text.
    fseek(f, 0, SEEK_END);
    long file_size = ftell(f);
    fseek(f, 0, SEEK_SET);

    unsigned char* buffer = new unsigned char[file_size + 1];
    size_t read_size = fread(buffer, 1, static_cast<size_t>(file_size), f);
    buffer[file_size] = 0;

    printf("size read: %u, file contents: %s\n",
        static_cast<unsigned int>(read_size),
        reinterpret_cast<char const*>(buffer));

    delete[] buffer;
    fclose(f);
}
```

### Example of using fstream
```cpp
#include <fstream>

int main()
{
    std::fstream fs;
    fs.open(path, std::fstream::in | std::fstream::out | std::fstream::binary);

    fs.seekg(0, std::fstream::end);
    std::streampos file_size = fs.tellg();
    fs.seekg(0, std::fstream::beg);

    unsigned char* buffer = new unsigned char[file_size + 1];
    fs.read(reinterpret_cast<char*>(buffer), file_size);
    buffer[file_size] = 0;

    printf("file size: %u, file contents: %s\n",
        static_cast<unsigned int>(file_size),
        reinterpret_cast<char const*>(buffer));

    delete[] buffer;
    fs.close();
}
```

### Simple example of using ocl::BinaryFile
```cpp
#include "portable/file/BinaryFile.hpp"

int main()
{
    // Note that Open function supports char or wchar_t.
    typedef ocl::BinaryFile<char> binary_file_type;
    binary_file_type::size_type file_size = binary_file_type::GetFileSize(path);

    // If required, there is a lot more control on the low level behavior of handling files.
    binary_file_type f(path); // NOTE: Could also use Open function instead of the constructor.

    unsigned char* buffer = new unsigned char[file_size + 1];
    f.Read(buffer, file_size);
    buffer[file_size] = 0;

    printf("file size: %u, file contents: %s\n",
        static_cast<unsigned int>(file_size),
        reinterpret_cast<char const*>(buffer));

    f.Close();
}
```

### Example of using ocl::BinaryFile with extra control
```cpp
#include "portable/file/BinaryFile.hpp"

int main()
{
    // Note that Open function supports char or wchar_t.
    typedef ocl::BinaryFile<char> binary_file_type;

    // These various flags can be changed to give low level control over the file.
    // Read the comments within BinaryFile.hpp, as some of these flags are platform specific.
    // There is no need to do any platform detection, if the flag is not supported,
    // there won't be any side effects.
    // Note that some platforms have more or less support than others, and the Supported
    // static function can be used to test the support for the flag, e.g.
    //
    //     if (binary_file_type::Supported(binary_file_type::ShareModes::ShareReadWrite) ...
    //

    binary_file_type::OpenModes open_mode = binary_file_type::OpenModes::CreateAlways;
    binary_file_type::AccessModes access_mode = binary_file_type::AccessModes::ReadWrite;
    binary_file_type::ShareModes share_mode = binary_file_type::ShareModes::ShareNone;
    binary_file_type::Behaviour behavioural_modes = binary_file_type::Behaviour::BehaviourNone;
    binary_file_type::Attributes file_attributes = binary_file_type::Attributes::AttributeNormal;
    binary_file_type::Security security_modes = binary_file_type::Security::SecurityNone;

    bool success = binary_file.Open(filename,
                                    open_mode,
                                    access_mode,
                                    share_mode,
                                    file_attributes,
                                    behavioural_modes,
                                    security_modes);

    if (success)
    {
        // It's possible to get the file size before or after opening.
        binary_file_type::size_type file_size = f.GetFileSize();

        unsigned char* buffer = new unsigned char[file_size + 1];
        success = f.Read(buffer, file_size);
        buffer[file_size] = 0;

        printf("file size: %u, file contents: %s\n",
            static_cast<unsigned int>(file_size),
            reinterpret_cast<char const*>(buffer));

        delete[] buffer;
        success = f.Close();
    }
    else
        printf("File \'%s\' not found\n", path);
}
```

### Example of using ocl::FileFinder
```cpp
#include "portable/file/FileFinder.hpp"
#include "portable/file/PathConstants.hpp"
#include <iostream>

int main()
{
    typedef char char_type;
    ocl::FileFinder<char_type> ff;

    char_type const* root_path = ocl::PathConstants<char_type>::GetRootPath();
    char_type const* all_files = ocl::PathConstants<char_type>::GetAllFilesWildCard();

    bool success = ff.FindFirst(root_path, all_files);

    if (success)
    {
        std::cout << ff.GetFileData().GetFilename() << std::endl;
        while (ff.FindNext())
        {
            std::cout << ff.GetFileData().GetFilename() << std::endl;
        }
    }
}
```

### Example of using ocl::RecursiveFileFinder
```cpp
#include "portable/file/RecursiveFileFinder.hpp"
#include "portable/file/PathConstants.hpp"
#include <iostream>

int main()
{
    typedef char char_type;

    std::size_t const depth = 1;
    ocl::RecursiveFileFinder<char_type> ff(depth);

    char_type const* root_path = ocl::PathConstants<char_type>::GetRootPath();
    char_type const* all_files = ocl::PathConstants<char_type>::GetAllFilesWildCard();

    bool success = ff.FindFirst(root_path, all_files);

    if (success)
    {
        std::cout << ff.GetFileData().GetFilename() << std::endl;
        while (ff.FindNext())
        {
            std::cout << ff.GetFileData().GetFilename() << std::endl;
        }
    }
}
```
