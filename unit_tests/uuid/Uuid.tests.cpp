/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../uuid/Uuid.hpp"

TEST_MEMBER_FUNCTION(Uuid, GetString, NA)
{
    ocl::Uuid<char> uuid;

    // Example expected string for comparing length.
    static const size_t len = StrLen("3d87bfae-2592-4418-bd4b-dc7634daf419");

    bool generated = uuid.Generate();
    char const* uuid_str = uuid.GetString();
    CHECK_TRUE(!generated || (uuid_str != nullptr && (StrLen(uuid_str) == len)));
}
