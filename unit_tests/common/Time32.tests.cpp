/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/Time32.hpp"

TEST_MEMBER_FUNCTION(Time32, Time32, NA)
{
    ocl::Time32 t;
    CHECK_TRUE(t.GetTime() == 0);
    CHECK_TRUE(t.GetMilliseconds() == 0u);
    CHECK_TRUE(t.GetSeconds() == 0u);
    CHECK_TRUE(t.GetMinutes() == 0u);
}

TEST_MEMBER_FUNCTION(Time32, Time32, Time32_const_ref)
{
    ocl::Time32 src(1, 2, 3, 4);
    ocl::Time32 t(src);
    CHECK_TRUE(t.GetMilliseconds() == 1u);
    CHECK_TRUE(t.GetSeconds() == 2u);
    CHECK_TRUE(t.GetMinutes() == 3u);
    CHECK_TRUE(t.GetHours() == 4u);
}

TEST_MEMBER_FUNCTION(Time32, Time32, Time32_move_ref)
{
    ocl::Time32 src(1, 2, 3, 4);
    ocl::Time32 t(std::move(src));
    CHECK_TRUE(t.GetMilliseconds() == 1u);
    CHECK_TRUE(t.GetSeconds() == 2u);
    CHECK_TRUE(t.GetMinutes() == 3u);
    CHECK_TRUE(t.GetHours() == 4u);
    CHECK_TRUE(src.GetMilliseconds() == 0u);
    CHECK_TRUE(src.GetSeconds() == 0u);
    CHECK_TRUE(src.GetMinutes() == 0u);
    CHECK_TRUE(src.GetHours() == 0u);
}

TEST_MEMBER_FUNCTION(Time32, comparison_operators, NA)
{
    ocl::Time32 time1a(1, 2, 3, 4);
    ocl::Time32 time1b(1, 2, 3, 4);

    // Times greater than time1a and time1b.
    ocl::Time32 time2(2, 2, 3, 4);
    ocl::Time32 time3(1, 3, 3, 4);
    ocl::Time32 time4(1, 2, 4, 4);
    ocl::Time32 time5(1, 2, 3, 5);

    CHECK_TRUE(time1a == time1b);
    CHECK_FALSE(time1a != time1b);
    CHECK_TRUE(time1a <= time1b);
    CHECK_TRUE(time1a >= time1b);

    CHECK_FALSE(time1a == time2);
    CHECK_TRUE(time1a != time2);
    CHECK_TRUE(time1a < time2);
    CHECK_TRUE(time1a <= time2);
    CHECK_TRUE(time2 > time1a);
    CHECK_TRUE(time2 >= time1a);

    CHECK_FALSE(time1a == time3);
    CHECK_TRUE(time1a != time3);
    CHECK_TRUE(time1a < time3);
    CHECK_TRUE(time1a <= time3);
    CHECK_TRUE(time3 > time1a);
    CHECK_TRUE(time3 >= time1a);

    CHECK_FALSE(time1a == time4);
    CHECK_TRUE(time1a != time4);
    CHECK_TRUE(time1a < time4);
    CHECK_TRUE(time1a <= time4);
    CHECK_TRUE(time4 > time1a);
    CHECK_TRUE(time4 >= time1a);

    CHECK_FALSE(time1a == time5);
    CHECK_TRUE(time1a != time5);
    CHECK_TRUE(time1a < time5);
    CHECK_TRUE(time1a <= time5);
    CHECK_TRUE(time5 > time1a);
    CHECK_TRUE(time5 >= time1a);
}

TEST_MEMBER_FUNCTION(Time32, GetSet, NA)
{
    using ocl::Time32;

    Time32 src(1, 2, 3, 4);

    {
        Time32 t(std::move(src));
        CHECK_TRUE(t.GetMilliseconds() == 1u);
        CHECK_TRUE(t.GetSeconds() == 2u);
        CHECK_TRUE(t.GetMinutes() == 3u);
        CHECK_TRUE(t.GetHours() == 4u);

        t.SetMilliseconds(5u);
        t.SetSeconds(6u);
        t.SetMinutes(7u);
        t.SetHours(8u);

        CHECK_TRUE(t.GetMilliseconds() == 5u);
        CHECK_TRUE(t.GetSeconds() == 6u);
        CHECK_TRUE(t.GetMinutes() == 7u);
        CHECK_TRUE(t.GetHours() == 8u);
    }

    for (Time32::milliseconds_type milliseconds = Time32::MIN_MILLISECONDS;
         milliseconds <= Time32::MAX_MILLISECONDS; milliseconds += 10)
    {
        for (Time32::seconds_type seconds = Time32::MIN_SECONDS; seconds <= Time32::MAX_SECONDS; seconds += 4)
        {
            for (Time32::minutes_type minutes = Time32::MIN_MINUTES; minutes <= Time32::MAX_MINUTES; minutes += 4)
            {
                for (Time32::hours_type hours = Time32::MIN_HOURS; hours <= Time32::MAX_HOURS; hours += 4)
                {
                    Time32 t(milliseconds, seconds, minutes, hours);
                    CHECK_TRUE(t.GetMilliseconds() == milliseconds);
                    CHECK_TRUE(t.GetSeconds() == seconds);
                    CHECK_TRUE(t.GetMinutes() == minutes);
                    CHECK_TRUE(t.GetHours() == hours);
                }
            }
        }
    }
}

TEST_MEMBER_FUNCTION(Time32, Copy, Time32_const_ref)
{
    using ocl::Time32;

    Time32 t(Time32(1, 2, 3, 4));
    Time32 t2;
    t2.Copy(t);
    CHECK_TRUE(t2.GetMilliseconds() == 1u);
    CHECK_TRUE(t2.GetSeconds() == 2u);
    CHECK_TRUE(t2.GetMinutes() == 3u);
    CHECK_TRUE(t2.GetHours() == 4u);
}

TEST_MEMBER_FUNCTION(Time32, Move, Time32_move_ref)
{
    using ocl::Time32;

    Time32 t(Time32(1, 2, 3, 4));
    Time32 t2;
    t2.Move(std::move(t));
    CHECK_TRUE(t2.GetMilliseconds() == 1u);
    CHECK_TRUE(t2.GetSeconds() == 2u);
    CHECK_TRUE(t2.GetMinutes() == 3u);
    CHECK_TRUE(t2.GetHours() == 4u);
    CHECK_TRUE(t.GetMilliseconds() == 0u);
    CHECK_TRUE(t.GetSeconds() == 0u);
    CHECK_TRUE(t.GetMinutes() == 0u);
    CHECK_TRUE(t.GetHours() == 0u);
}
