/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/Point.hpp"

TEST_MEMBER_FUNCTION(Point, Point, NA)
{
    typedef ocl::Point<unsigned int> point_type;

    point_type pt;
    CHECK_EQUAL(pt.X(), 0U);
    CHECK_EQUAL(pt.Y(), 0U);
}

TEST_MEMBER_FUNCTION(Point, Point, int_int)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_ARGS("int, int");

    point_type pt(1, 2);
    CHECK_EQUAL(pt.X(), 1U);
    CHECK_EQUAL(pt.Y(), 2U);
}

TEST_MEMBER_FUNCTION(Point, Point, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    point_type pt(point_type(3, 4));
    CHECK_EQUAL(pt.X(), 3U);
    CHECK_EQUAL(pt.Y(), 4U);
}

TEST_MEMBER_FUNCTION(Point, assignment, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_FUNCTION_NAME("=");

    point_type pt;
    pt = point_type(3, 4);
    CHECK_EQUAL(pt.X(), 3U);
    CHECK_EQUAL(pt.Y(), 4U);
}

TEST_MEMBER_FUNCTION(Point, plus_equal, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_FUNCTION_NAME("+=");

    point_type pt(1, 2);
    pt += point_type(3, 4);
    CHECK_EQUAL(pt.X(), 4U);
    CHECK_EQUAL(pt.Y(), 6U);
}

TEST_MEMBER_FUNCTION(Point, minus_equal, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_FUNCTION_NAME("-=");

    point_type pt(4, 6);
    pt -= point_type(3, 4);
    CHECK_EQUAL(pt.X(), 1U);
    CHECK_EQUAL(pt.Y(), 2U);
}

TEST_MEMBER_FUNCTION(Point, multiply_equal, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_FUNCTION_NAME("*=");

    point_type pt(4, 6);
    pt *= point_type(4, 2);
    CHECK_EQUAL(pt.X(), 16U);
    CHECK_EQUAL(pt.Y(), 12U);
}

TEST_MEMBER_FUNCTION(Point, divide_equal, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_FUNCTION_NAME("/=");

    point_type pt(4, 12);
    pt /= point_type(2, 3);
    CHECK_EQUAL(pt.X(), 2U);
    CHECK_EQUAL(pt.Y(), 4U);
}

TEST_MEMBER_FUNCTION(Point, plus, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_FUNCTION_NAME("+");

    point_type pt(1, 2);
    point_type pt2(pt + point_type(3, 4));
    CHECK_EQUAL(pt2.X(), 4U);
    CHECK_EQUAL(pt2.Y(), 6U);
}

TEST_MEMBER_FUNCTION(Point, minus, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_FUNCTION_NAME("-");

    point_type pt(4, 6);
    point_type pt2(pt - point_type(3, 4));
    CHECK_EQUAL(pt2.X(), 1U);
    CHECK_EQUAL(pt2.Y(), 2U);
}

TEST_MEMBER_FUNCTION(Point, multiply, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_FUNCTION_NAME("*");

    point_type pt(4, 6);
    point_type pt2(pt * point_type(4, 2));
    CHECK_EQUAL(pt2.X(), 16U);
    CHECK_EQUAL(pt2.Y(), 12U);
}

TEST_MEMBER_FUNCTION(Point, divide, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    TEST_OVERRIDE_FUNCTION_NAME("/");

    point_type pt(4, 12);
    point_type pt2(pt / point_type(2, 3));
    CHECK_EQUAL(pt2.X(), 2U);
    CHECK_EQUAL(pt2.Y(), 4U);
}

TEST_MEMBER_FUNCTION(Point, OffsetX, int)
{
    typedef ocl::Point<unsigned int> point_type;

    point_type pt(1, 2);
    pt.OffsetX(2);
    CHECK_EQUAL(pt.X(), 3U);
    CHECK_EQUAL(pt.Y(), 2U);

    pt.OffsetX(-2);
    CHECK_EQUAL(pt.X(), 1U);
    CHECK_EQUAL(pt.Y(), 2U);
}

TEST_MEMBER_FUNCTION(Point, OffsetY, int)
{
    typedef ocl::Point<unsigned int> point_type;

    point_type pt(1, 2);
    pt.OffsetY(2);
    CHECK_EQUAL(pt.X(), 1U);
    CHECK_EQUAL(pt.Y(), 4U);

    pt.OffsetY(-2);
    CHECK_EQUAL(pt.X(), 1U);
    CHECK_EQUAL(pt.Y(), 2U);
}

TEST_MEMBER_FUNCTION(Point, Translate, NA)
{
    typedef ocl::Point<unsigned int> point_type;

    point_type pt(1, 2);
    ocl::Point<int> pt2(pt.Translate());

    CHECK_EQUAL(pt2.X(), 1);
    CHECK_EQUAL(pt2.Y(), 2);
}

TEST_MEMBER_FUNCTION(Point, Translate, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    ocl::Point<int> pt(point_type::Translate(point_type(1, 2)));

    CHECK_EQUAL(pt.X(), 1);
    CHECK_EQUAL(pt.Y(), 2);
}

TEST_MEMBER_FUNCTION(Point, GetDiff, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    point_type pt1(1, 2);
    point_type pt2(2, 4);

    ocl::Point<int> diff = pt1.GetDiff(pt2);

    CHECK_EQUAL(diff.X(), -1);
    CHECK_EQUAL(diff.Y(), -2);
}

TEST_MEMBER_FUNCTION(Point, GetAbsDiff, Point)
{
    typedef ocl::Point<unsigned int> point_type;

    point_type pt1(1, 2);
    point_type pt2(2, 4);

    point_type diff = pt1.GetAbsDiff(pt2);

    CHECK_EQUAL(diff.X(), 1U);
    CHECK_EQUAL(diff.Y(), 2U);
}
