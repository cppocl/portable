/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/MemoryUtility.hpp"

TEST_MEMBER_FUNCTION(MemoryUtility, AllocateFree, size_t)
{
    {
        char* ptr = ocl::MemoryUtility<char, std::size_t, false>::Allocate(1);
        CHECK_TRUE(ptr != nullptr);

        ocl::MemoryUtility<char>::Free(ptr);
        CHECK_TRUE(ptr == nullptr);
    }

    {
        char* ptr = ocl::MemoryUtility<char, std::size_t, true>::Allocate(1);
        CHECK_TRUE(ptr != nullptr);

        ocl::MemoryUtility<char>::Free(ptr);
        CHECK_TRUE(ptr == nullptr);
    }
}

TEST_MEMBER_FUNCTION(MemoryUtility, AllocateFastFree, size_t)
{
    {
        char* ptr = ocl::MemoryUtility<char, std::size_t, false>::Allocate(1);
        CHECK_TRUE(ptr != nullptr);

        ocl::MemoryUtility<char>::FastFree(ptr);
        CHECK_TRUE(ptr != nullptr);
    }

    {
        char* ptr = ocl::MemoryUtility<char, std::size_t, true>::Allocate(1);
        CHECK_TRUE(ptr != nullptr);

        ocl::MemoryUtility<char>::FastFree(ptr);
        CHECK_TRUE(ptr != nullptr);
    }
}
