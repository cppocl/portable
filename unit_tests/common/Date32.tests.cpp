/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/Date32.hpp"
#include <utility>

TEST_MEMBER_FUNCTION(Date32, Date32, NA)
{
    ocl::Date32 d;
    CHECK_TRUE(d.GetDate() == 0);
    CHECK_TRUE(d.GetDay() == 0u);
    CHECK_TRUE(d.GetMonth() == 0u);
    CHECK_TRUE(d.GetYear() == 0u);
}

TEST_MEMBER_FUNCTION(Date32, Date32, Date32_const_ref)
{
    ocl::Date32 src(1, 2, 1987);
    ocl::Date32 d(src);
    CHECK_TRUE(d.GetDay() == 1u);
    CHECK_TRUE(d.GetMonth() == 2u);
    CHECK_TRUE(d.GetYear() == 1987u);
}

TEST_MEMBER_FUNCTION(Date32, Date32, Date32_move_ref)
{
    ocl::Date32 src(3, 4, 1986);
    ocl::Date32 d(std::move(src));
    CHECK_TRUE(d.GetDay() == 3u);
    CHECK_TRUE(d.GetMonth() == 4u);
    CHECK_TRUE(d.GetYear() == 1986u);
    CHECK_TRUE(src.GetDay() == 0u);
    CHECK_TRUE(src.GetMonth() == 0u);
    CHECK_TRUE(src.GetYear() == 0u);
}

TEST_MEMBER_FUNCTION(Date32, comparison_operators, NA)
{
    ocl::Date32 date1a(3, 4, 1986);
    ocl::Date32 date1b(3, 4, 1986);

    // Dates greater than date1a and date1b.
    ocl::Date32 date2(4, 4, 1986);
    ocl::Date32 date3(3, 5, 1986);
    ocl::Date32 date4(3, 4, 1987);

    CHECK_TRUE(date1a == date1b);
    CHECK_FALSE(date1a != date1b);
    CHECK_TRUE(date1a <= date1b);
    CHECK_TRUE(date1a >= date1b);

    CHECK_FALSE(date1a == date2);
    CHECK_TRUE(date1a != date2);
    CHECK_TRUE(date1a < date2);
    CHECK_TRUE(date1a <= date2);
    CHECK_TRUE(date2 > date1a);
    CHECK_TRUE(date2 >= date1a);

    CHECK_FALSE(date1a == date3);
    CHECK_TRUE(date1a != date3);
    CHECK_TRUE(date1a < date3);
    CHECK_TRUE(date1a <= date3);
    CHECK_TRUE(date3 > date1a);
    CHECK_TRUE(date3 >= date1a);

    CHECK_FALSE(date1a == date4);
    CHECK_TRUE(date1a != date4);
    CHECK_TRUE(date1a < date4);
    CHECK_TRUE(date1a <= date4);
    CHECK_TRUE(date4 > date1a);
    CHECK_TRUE(date4 >= date1a);
}

TEST_MEMBER_FUNCTION(Date32, GetSet, NA)
{
    using ocl::Date32;

    Date32 src(3, 4, 1986);

    {
        Date32 d(std::move(src));
        CHECK_TRUE(d.GetDay() == 3u);
        CHECK_TRUE(d.GetMonth() == 4u);
        CHECK_TRUE(d.GetYear() == 1986u);

        d.SetDay(5u);
        d.SetMonth(6u);
        d.SetYear(2010u);

        CHECK_TRUE(d.GetDay() == 5u);
        CHECK_TRUE(d.GetMonth() == 6u);
        CHECK_TRUE(d.GetYear() == 2010u);
    }

    for (Date32::day_type day = Date32::MIN_DAY; day <= Date32::MAX_DAY; ++day)
    {
        for (Date32::month_type month = Date32::MIN_MONTH; month <= Date32::MAX_MONTH; ++month)
        {
            for (Date32::year_type year = 1980u; year <= 2099u; ++year)
            {
                Date32 d(day, month, year);
                CHECK_TRUE(d.GetDay() == day);
                CHECK_TRUE(d.GetMonth() == month);
                CHECK_TRUE(d.GetYear() == year);
            }
        }
    }
}

TEST_MEMBER_FUNCTION(Date32, Copy, Date32_const_ref)
{
    using ocl::Date32;

    Date32 d(Date32(3, 4, 1986));
    Date32 d2;
    d2.Copy(d);
    CHECK_TRUE(d2.GetDay() == 3u);
    CHECK_TRUE(d2.GetMonth() == 4u);
    CHECK_TRUE(d2.GetYear() == 1986u);
}

TEST_MEMBER_FUNCTION(Date32, Move, Date32_move_ref)
{
    using ocl::Date32;

    Date32 d(Date32(3, 4, 1986));
    Date32 d2;
    d2.Move(std::move(d));
    CHECK_TRUE(d2.GetDay() == 3u);
    CHECK_TRUE(d2.GetMonth() == 4u);
    CHECK_TRUE(d2.GetYear() == 1986u);
    CHECK_TRUE(d.GetDay() == 0u);
    CHECK_TRUE(d.GetMonth() == 0u);
    CHECK_TRUE(d.GetYear() == 0u);
}
