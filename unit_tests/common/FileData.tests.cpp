/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/FileData.hpp"
#include "../../common/StringCopy.hpp"

TEST_MEMBER_FUNCTION(FileData, FileData, char_const_ptr_uint64_t)
{
    {
        typedef char char_type;
        typedef ocl::FileData<char_type> file_data_type;
        ocl::DateTime64 empty_date_time;

        file_data_type file_data("abc", 1);
        CHECK_TRUE(file_data.GetFilename() != nullptr);
        CHECK_TRUE(StrCmp(file_data.GetFilename(), "abc") == 0);
        CHECK_TRUE(file_data.GetFilenameLength() == 3U);
        CHECK_TRUE(file_data.GetFileSize() == 1);
        CHECK_TRUE(file_data.GetCreationDateTime() == empty_date_time);
        CHECK_TRUE(file_data.GetLastAccessedDateTime() == empty_date_time);
        CHECK_TRUE(file_data.GetLastModifiedDateTime() == empty_date_time);
        CHECK_FALSE(file_data.IsDirectory());
    }

    {
        typedef wchar_t char_type;
        typedef ocl::FileData<char_type> file_data_type;
        ocl::DateTime64 empty_date_time;

        file_data_type file_data(L"abc", 1);
        CHECK_TRUE(file_data.GetFilename() != nullptr);
        CHECK_TRUE(StrCmp(file_data.GetFilename(), L"abc") == 0);
        CHECK_TRUE(file_data.GetFilenameLength() == 3U);
        CHECK_TRUE(file_data.GetFileSize() == 1);
        CHECK_TRUE(file_data.GetCreationDateTime() == empty_date_time);
        CHECK_TRUE(file_data.GetLastAccessedDateTime() == empty_date_time);
        CHECK_TRUE(file_data.GetLastModifiedDateTime() == empty_date_time);
        CHECK_FALSE(file_data.IsDirectory());
    }
}

TEST_MEMBER_FUNCTION(FileData, FileData, char_const_ptr_uint64_t_DateTime64_DateTime64_DateTime64_bool)
{
    ocl::DateTime64 const creation_date_time(1, 2, 1986, 3, 4, 5, 6);
    ocl::DateTime64 const accessed_date_time(3, 4, 1986, 5, 6, 7, 8);
    ocl::DateTime64 const modified_date_time(4, 5, 1986, 6, 7, 8, 9);

    {
        typedef char char_type;
        typedef ocl::FileData<char_type> file_data_type;

        file_data_type file_data("abc", 1, creation_date_time, accessed_date_time, modified_date_time, true);
        CHECK_TRUE(file_data.GetFilename() != nullptr);
        CHECK_TRUE(StrCmp(file_data.GetFilename(), "abc") == 0);
        CHECK_TRUE(file_data.GetFilenameLength() == 3U);
        CHECK_TRUE(file_data.GetFileSize() == 1);
        CHECK_TRUE(file_data.GetCreationDateTime() == creation_date_time);
        CHECK_TRUE(file_data.GetLastAccessedDateTime() == accessed_date_time);
        CHECK_TRUE(file_data.GetLastModifiedDateTime() == modified_date_time);
        CHECK_TRUE(file_data.IsDirectory());
    }

    {
        typedef char char_type;
        typedef ocl::FileData<char_type> file_data_type;

        file_data_type file_data("abc", 1, creation_date_time, accessed_date_time, modified_date_time, true);
        CHECK_TRUE(file_data.GetFilename() != nullptr);
        CHECK_TRUE(StrCmp(file_data.GetFilename(), "abc") == 0);
        CHECK_TRUE(file_data.GetFilenameLength() == 3U);
        CHECK_TRUE(file_data.GetFileSize() == 1);
        CHECK_TRUE(file_data.GetCreationDateTime() == creation_date_time);
        CHECK_TRUE(file_data.GetLastAccessedDateTime() == accessed_date_time);
        CHECK_TRUE(file_data.GetLastModifiedDateTime() == modified_date_time);
        CHECK_TRUE(file_data.IsDirectory());
    }
}

TEST_MEMBER_FUNCTION(FileData, FileData, char_ptr_move_uint64_t_move_DateTime64_move_DateTime64_move_DateTime64_move_bool_move)
{
    ocl::DateTime64 empty_date_time;

    ocl::DateTime64 const init_creation_date_time(1, 2, 1986, 3, 4, 5, 6);
    ocl::DateTime64 const init_accessed_date_time(3, 4, 1986, 5, 6, 7, 8);
    ocl::DateTime64 const init_modified_date_time(4, 5, 1986, 6, 7, 8, 9);

    {
        typedef char char_type;
        typedef ocl::FileData<char_type> file_data_type;
        typedef ocl::StringCopy<char_type> string_copy_type;
        typedef typename file_data_type::file_size_type file_size_type;

        char_type* str = nullptr;
        file_size_type file_size = 1;
        ocl::DateTime64 creation_date_time(init_creation_date_time);
        ocl::DateTime64 accessed_date_time(init_accessed_date_time);
        ocl::DateTime64 modified_date_time(init_modified_date_time);
        bool is_directory = true;

        string_copy_type::AllocCopy(str, "abc");

        file_data_type file_data(
            static_cast<char_type*&&>(str),
            static_cast<file_size_type&&>(file_size),
            static_cast<ocl::DateTime64&&>(creation_date_time),
            static_cast<ocl::DateTime64&&>(accessed_date_time),
            static_cast<ocl::DateTime64&&>(modified_date_time),
            static_cast<bool&&>(is_directory));

        CHECK_TRUE(file_data.GetFilename() != nullptr);
        CHECK_TRUE(StrCmp(file_data.GetFilename(), "abc") == 0);
        CHECK_TRUE(file_data.GetFilenameLength() == 3U);
        CHECK_TRUE(file_data.GetFileSize() == 1);
        CHECK_TRUE(file_data.GetCreationDateTime() == init_creation_date_time);
        CHECK_TRUE(file_data.GetLastAccessedDateTime() == init_accessed_date_time);
        CHECK_TRUE(file_data.GetLastModifiedDateTime() == init_modified_date_time);
        CHECK_TRUE(file_data.IsDirectory());

        CHECK_TRUE(str == nullptr);
        CHECK_TRUE(file_size == 0);
        CHECK_TRUE(creation_date_time == empty_date_time);
        CHECK_TRUE(accessed_date_time == empty_date_time);
        CHECK_TRUE(modified_date_time == empty_date_time);
        CHECK_FALSE(is_directory);
    }

    {
        typedef wchar_t char_type;
        typedef ocl::FileData<char_type> file_data_type;
        typedef ocl::StringCopy<char_type> string_copy_type;
        typedef typename file_data_type::file_size_type file_size_type;

        char_type* str = nullptr;
        file_size_type file_size = 1;
        ocl::DateTime64 creation_date_time(init_creation_date_time);
        ocl::DateTime64 accessed_date_time(init_accessed_date_time);
        ocl::DateTime64 modified_date_time(init_modified_date_time);
        bool is_directory = true;

        string_copy_type::AllocCopy(str, L"abc");

        file_data_type file_data(
            static_cast<char_type*&&>(str),
            static_cast<file_size_type&&>(file_size),
            static_cast<ocl::DateTime64&&>(creation_date_time),
            static_cast<ocl::DateTime64&&>(accessed_date_time),
            static_cast<ocl::DateTime64&&>(modified_date_time),
            static_cast<bool&&>(is_directory));

        CHECK_TRUE(file_data.GetFilename() != nullptr);
        CHECK_TRUE(StrCmp(file_data.GetFilename(), L"abc") == 0);
        CHECK_TRUE(file_data.GetFilenameLength() == 3U);
        CHECK_TRUE(file_data.GetFileSize() == 1);
        CHECK_TRUE(file_data.GetCreationDateTime() == init_creation_date_time);
        CHECK_TRUE(file_data.GetLastAccessedDateTime() == init_accessed_date_time);
        CHECK_TRUE(file_data.GetLastModifiedDateTime() == init_modified_date_time);
        CHECK_TRUE(file_data.IsDirectory());

        CHECK_TRUE(str == nullptr);
        CHECK_TRUE(file_size == 0);
        CHECK_TRUE(creation_date_time == empty_date_time);
        CHECK_TRUE(accessed_date_time == empty_date_time);
        CHECK_TRUE(modified_date_time == empty_date_time);
        CHECK_FALSE(is_directory);
    }
}
