/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/TriState.hpp"

TEST_MEMBER_FUNCTION(TriState, TriState, NA)
{
    {
        ocl::TriState t;
        CHECK_TRUE(t.IsUnknown());
    }
}

TEST_MEMBER_FUNCTION(TriState, TriState, bool)
{
    {
        ocl::TriState t(true);
        CHECK_TRUE(t);
        CHECK_TRUE(t == true);
    }

    {
        ocl::TriState t(false);
        CHECK_FALSE(t);
    }
}

TEST_MEMBER_FUNCTION(TriState, TriState, State)
{
    {
        ocl::TriState t(ocl::TriState::State::True);
        CHECK_TRUE(static_cast<bool>(t));
    }

    {
        ocl::TriState t(ocl::TriState::State::False);
        CHECK_FALSE(static_cast<bool>(t));
    }
}

TEST_MEMBER_FUNCTION(TriState, TriState, TriState_const_ref)
{
    {
        ocl::TriState t(ocl::TriState::State::True);
        CHECK_TRUE(static_cast<bool>(t));
    }

    {
        ocl::TriState t(ocl::TriState::State::False);
        CHECK_FALSE(static_cast<bool>(t));
    }
}

TEST_MEMBER_FUNCTION(TriState, bool_operator, State)
{
    {
        ocl::TriState t;
        CHECK_TRUE(t.IsUnknown());
    }

    {
        ocl::TriState t(ocl::TriState::State::True);
        CHECK_TRUE(t);
    }

    {
        ocl::TriState t(ocl::TriState::State::False);
        CHECK_FALSE(t);
    }
}

TEST_MEMBER_FUNCTION(TriState, assignment, State)
{
    ocl::TriState t;
    ocl::TriState t2;
    CHECK_TRUE(t.IsUnknown());
    CHECK_TRUE(t2.IsUnknown());

    t = true;
    CHECK_TRUE(t.IsTrue());

    t2 = t;
    CHECK_TRUE(t2.IsTrue());

    t = false;
    CHECK_TRUE(t.IsFalse());

    t2 = t;
    CHECK_TRUE(t2.IsFalse());

    t2 = std::move(t);
    CHECK_TRUE(t.IsUnknown());
}
