/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/BitUtility.hpp"

TEST_MEMBER_FUNCTION(BitUtility, Pos, uint32_t)
{
    CHECK_TRUE(ocl::BitUtility::Pos<std::uint32_t>(1) == 0U);
    CHECK_TRUE(ocl::BitUtility::Pos<std::uint32_t>(2) == 1U);
    CHECK_TRUE(ocl::BitUtility::Pos<std::uint32_t>(3) == 0U);
    CHECK_TRUE(ocl::BitUtility::Pos<std::uint32_t>(0x10) == 4U);
    CHECK_TRUE(ocl::BitUtility::Pos<std::uint32_t>(0x100) == 8U);
}

TEST_MEMBER_FUNCTION(BitUtility, Start, uint32_t)
{
    CHECK_TRUE(ocl::BitUtility::Start<std::uint32_t>(1) == 1U);
    CHECK_TRUE(ocl::BitUtility::Start<std::uint32_t>(2) == 1U);
    CHECK_TRUE(ocl::BitUtility::Start<std::uint32_t>(3) == 3U);
    CHECK_TRUE(ocl::BitUtility::Start<std::uint32_t>(6) == 3U);
}

TEST_MEMBER_FUNCTION(BitUtility, Count, uint32_t)
{
    CHECK_TRUE(ocl::BitUtility::Count<std::uint32_t>(1) == 1U);
    CHECK_TRUE(ocl::BitUtility::Count<std::uint32_t>(3) == 2U);
    CHECK_TRUE(ocl::BitUtility::Count<std::uint32_t>(7) == 3U);
}

TEST_MEMBER_FUNCTION(BitUtility, MaskGet, uint32_t)
{
    CHECK_TRUE((ocl::BitUtility::MaskGet<std::uint32_t, std::uint8_t>(0xf0, 0xff) == 0xf0U));
    CHECK_TRUE((ocl::BitUtility::MaskGet<std::uint32_t, std::uint8_t>(0xf000, 0xff00) == 0xf0U));
}

TEST_MEMBER_FUNCTION(BitUtility, MaskSet, uint32_t_uint32_t_uint8_t)
{
    // Test first 8 bits are set.
    CHECK_TRUE((ocl::BitUtility::MaskSet<std::uint32_t, std::uint8_t>(0, 0xff, 15U) == 15U));
    CHECK_TRUE((ocl::BitUtility::MaskSet<std::uint32_t, std::uint8_t>(0xfffffff0, 0xff, 15U) == 0xffffff0fU));

    // Test first 8 bits are skipped, and following 8 bits are set to 15,
    // with return being (15 << 8) or 0xf00.
    CHECK_TRUE((ocl::BitUtility::MaskSet<std::uint32_t, std::uint8_t>(0, 0xff00, 15U) == 0xf00u));
    CHECK_TRUE((ocl::BitUtility::MaskSet<std::uint32_t, std::uint8_t>(0x00130000, 0x003f0000, 0x24) == 0x00240000));
    CHECK_TRUE((ocl::BitUtility::MaskSet<std::uint16_t, std::uint8_t>(0x0f00, 0xff00, 0xf0) == 0xf000));
}

TEST_MEMBER_FUNCTION(BitUtility, Combine, uint32_t_uint32_t)
{
    using ocl::BitUtility;
    std::uint16_t r16;
    //std::uint32_t r32;

    r16 = BitUtility::Combine<std::uint8_t, std::uint8_t, std::uint16_t>(0xff, 0xff);
    CHECK_TRUE(r16 == 0xffffu);

    r16 = BitUtility::Combine<std::uint8_t, std::uint8_t, std::uint16_t>(0xf, 0xf);
    CHECK_TRUE(r16 == 0x0f0fu);
}

TEST_MEMBER_FUNCTION(BitUtility, Split, uint32_t_uint32_t)
{
    using ocl::BitUtility;
    std::uint8_t low8, high8;
    std::uint16_t low16, high16;

    BitUtility::Split<std::uint8_t, std::uint8_t, std::uint16_t>(low8, high8, 0xabcd);
    CHECK_TRUE(low8 == 0xcd);
    CHECK_TRUE(high8 == 0xab);

    BitUtility::Split<std::uint16_t, std::uint16_t, std::uint32_t>(low16, high16, 0xabcd);
    CHECK_TRUE(low16 == static_cast<std::uint16_t>(0xabcd));
    CHECK_TRUE(high16 == static_cast<std::uint16_t>(0x0));

    BitUtility::Split<std::uint16_t, std::uint16_t, std::uint32_t>(low16, high16, 0x12345678);
    CHECK_TRUE(low16 == static_cast<std::uint16_t>(0x5678));
    CHECK_TRUE(high16 == static_cast<std::uint16_t>(0x1234));
}
