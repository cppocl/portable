/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/StringUtility.hpp"
#include <cstring>
#include <cstddef>

TEST_MEMBER_FUNCTION(StringUtility, GetLength, char_const_ptr)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::GetLength(nullptr) == 0);
        CHECK_TRUE(string_utility_type::GetLength("") == 0);
        CHECK_TRUE(string_utility_type::GetLength("1") == 1);
        CHECK_TRUE(string_utility_type::GetLength("12") == 2);
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;

        CHECK_TRUE(string_utility_type::GetLength(nullptr) == 0);
        CHECK_TRUE(string_utility_type::GetLength(L"") == 0);
        CHECK_TRUE(string_utility_type::GetLength(L"1") == 1);
        CHECK_TRUE(string_utility_type::GetLength(L"12") == 2);
    }
}

TEST_MEMBER_FUNCTION(StringUtility, UnsafeGetLength, char_const_ptr)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::UnsafeGetLength("") == 0);
        CHECK_TRUE(string_utility_type::UnsafeGetLength("1") == 1);
        CHECK_TRUE(string_utility_type::UnsafeGetLength("12") == 2);
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;

        CHECK_TRUE(string_utility_type::UnsafeGetLength(L"") == 0);
        CHECK_TRUE(string_utility_type::UnsafeGetLength(L"1") == 1);
        CHECK_TRUE(string_utility_type::UnsafeGetLength(L"12") == 2);
    }
}

TEST_MEMBER_FUNCTION(StringUtility, EndsWith, char_const_ptr_char)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::EndsWith(nullptr, 'A') == 0);
        CHECK_TRUE(string_utility_type::EndsWith("B", 'A') == 1);
        CHECK_TRUE(string_utility_type::EndsWith("A", 'A') == 0);
        CHECK_TRUE(string_utility_type::EndsWith("BA", 'A') == 1);
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;

        CHECK_TRUE(string_utility_type::EndsWith(nullptr, L'A') == 0);
        CHECK_TRUE(string_utility_type::EndsWith(L"B", L'A') == 1);
        CHECK_TRUE(string_utility_type::EndsWith(L"A", L'A') == 0);
        CHECK_TRUE(string_utility_type::EndsWith(L"BA", L'A') == 1);
    }
}

TEST_MEMBER_FUNCTION(StringUtility, EndsWith, char_const_ptr_size_t_char)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::EndsWith(nullptr, 'A') == 0);
        CHECK_TRUE(string_utility_type::EndsWith("B", 'A') == 1);
        CHECK_TRUE(string_utility_type::EndsWith("A", 'A') == 0);
        CHECK_TRUE(string_utility_type::EndsWith("BA", 'A') == 1);
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;

        CHECK_TRUE(string_utility_type::EndsWith(nullptr, L'A') == 0);
        CHECK_TRUE(string_utility_type::EndsWith(L"B", L'A') == 1);
        CHECK_TRUE(string_utility_type::EndsWith(L"A", L'A') == 0);
        CHECK_TRUE(string_utility_type::EndsWith(L"BA", L'A') == 1);
    }
}

TEST_MEMBER_FUNCTION(StringUtility, EndsWith, char_const_ptr_size_t_ref_char)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;
        typedef typename string_utility_type::size_type size_type;

        size_type len = 1;
        CHECK_TRUE(string_utility_type::EndsWith(nullptr, len, 'A') == 0);
        CHECK_TRUE(len == 0);

        CHECK_TRUE(string_utility_type::EndsWith("B", len, 'A') == 1);
        CHECK_TRUE(len == 1);

        CHECK_TRUE(string_utility_type::EndsWith("A", len, 'A') == 0);
        CHECK_TRUE(len == 1);

        CHECK_TRUE(string_utility_type::EndsWith("BA", len, 'A') == 1);
        CHECK_TRUE(len == 2);
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;
        typedef typename string_utility_type::size_type size_type;

        size_type len = 1;

        CHECK_TRUE(string_utility_type::EndsWith(nullptr, len, L'A') == 0);
        CHECK_TRUE(len == 0);

        CHECK_TRUE(string_utility_type::EndsWith(L"B", len, L'A') == 1);
        CHECK_TRUE(len == 1);

        CHECK_TRUE(string_utility_type::EndsWith(L"A", len, L'A') == 0);
        CHECK_TRUE(len == 1);

        CHECK_TRUE(string_utility_type::EndsWith(L"BA", len, L'A') == 1);
        CHECK_TRUE(len == 2);
    }
}

TEST_MEMBER_FUNCTION(StringUtility, UnsafeEndsWith, char_const_ptr_char)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::UnsafeEndsWith("B", 'A') == 1);
        CHECK_TRUE(string_utility_type::UnsafeEndsWith("A", 'A') == 0);
        CHECK_TRUE(string_utility_type::UnsafeEndsWith("BA", 'A') == 1);
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;

        CHECK_TRUE(string_utility_type::UnsafeEndsWith(L"B", L'A') == 1);
        CHECK_TRUE(string_utility_type::UnsafeEndsWith(L"A", L'A') == 0);
        CHECK_TRUE(string_utility_type::UnsafeEndsWith(L"BA", L'A') == 1);
    }
}

TEST_MEMBER_FUNCTION(StringUtility, Compare, char_const_ptr_char_const_ptr)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::Compare(nullptr, nullptr));
        CHECK_TRUE(string_utility_type::Compare("", ""));
        CHECK_TRUE(string_utility_type::Compare("1", "1"));
        CHECK_FALSE(string_utility_type::Compare("1", "12"));
        CHECK_FALSE(string_utility_type::Compare("12", "1"));
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;

        CHECK_TRUE(string_utility_type::Compare(nullptr, nullptr));
        CHECK_TRUE(string_utility_type::Compare(L"", L""));
        CHECK_TRUE(string_utility_type::Compare(L"1", L"1"));
        CHECK_FALSE(string_utility_type::Compare(L"1", L"12"));
        CHECK_FALSE(string_utility_type::Compare(L"12", L"1"));
    }
}

TEST_MEMBER_FUNCTION(StringUtility, UnsafeCompare, char_const_ptr_char_const_ptr)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::UnsafeCompare("", ""));
        CHECK_TRUE(string_utility_type::UnsafeCompare("1", "1"));
        CHECK_FALSE(string_utility_type::UnsafeCompare("1", "12"));
        CHECK_FALSE(string_utility_type::UnsafeCompare("12", "1"));
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;

        CHECK_TRUE(string_utility_type::UnsafeCompare(L"", L""));
        CHECK_TRUE(string_utility_type::UnsafeCompare(L"1", L"1"));
        CHECK_FALSE(string_utility_type::UnsafeCompare(L"1", L"12"));
        CHECK_FALSE(string_utility_type::UnsafeCompare(L"12", L"1"));
    }
}

TEST_MEMBER_FUNCTION(StringUtility, UnsafeCopy, char_ptr_char_const_ptr_size_t)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;
        char str[100];

        string_utility_type::UnsafeCopy(str, "", 0);
        CHECK_TRUE(str[0] == '\0');

        string_utility_type::UnsafeCopy(str, "a", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "a"));

        string_utility_type::UnsafeCopy(str, "ab", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "ab"));
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;
        wchar_t str[100];

        string_utility_type::UnsafeCopy(str, L"", 0);
        CHECK_TRUE(str[0] == '\0');

        string_utility_type::UnsafeCopy(str, L"a", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"a"));

        string_utility_type::UnsafeCopy(str, L"ab", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"ab"));
    }
}

TEST_MEMBER_FUNCTION(StringUtility, UnsafeCopy, char_ptr_char_const_ptr_size_t_char_const_ptr_size_t)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;
        char str[100];

        string_utility_type::UnsafeCopy(str, "", 0, "", 0);
        CHECK_TRUE(str[0] == '\0');

        string_utility_type::UnsafeCopy(str, "a", 1, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "a"));

        string_utility_type::UnsafeCopy(str, "", 0, "a", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "a"));

        string_utility_type::UnsafeCopy(str, "ab", 2, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "ab"));

        string_utility_type::UnsafeCopy(str, "", 0, "ab", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "ab"));

        string_utility_type::UnsafeCopy(str, "a", 1, "b", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "ab"));

        string_utility_type::UnsafeCopy(str, "a", 1, "bc", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abc"));

        string_utility_type::UnsafeCopy(str, "ab", 2, "c", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abc"));

        string_utility_type::UnsafeCopy(str, "ab", 2, "cd", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abcd"));
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;
        wchar_t str[100];

        string_utility_type::UnsafeCopy(str, L"", 0, L"", 0);
        CHECK_TRUE(str[0] == L'\0');

        string_utility_type::UnsafeCopy(str, L"a", 1, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"a"));

        string_utility_type::UnsafeCopy(str, L"", 0, L"a", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"a"));

        string_utility_type::UnsafeCopy(str, L"ab", 2, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"ab"));

        string_utility_type::UnsafeCopy(str, L"", 0, L"ab", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"ab"));

        string_utility_type::UnsafeCopy(str, L"a", 1, L"b", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"ab"));

        string_utility_type::UnsafeCopy(str, L"a", 1, L"bc", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abc"));

        string_utility_type::UnsafeCopy(str, L"ab", 2, L"c", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abc"));

        string_utility_type::UnsafeCopy(str, L"ab", 2, L"cd", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abcd"));
    }
}

TEST_MEMBER_FUNCTION(StringUtility, UnsafeCopy, char_ptr_char_const_ptr_size_t_char_const_ptr_size_t_char_const_ptr_size_t)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;
        char str[100];

        string_utility_type::UnsafeCopy(str, "", 0, "", 0, "", 0);
        CHECK_TRUE(str[0] == '\0');

        string_utility_type::UnsafeCopy(str, "a", 1, "", 0, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "a"));

        string_utility_type::UnsafeCopy(str, "", 0, "a", 1, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "a"));

        string_utility_type::UnsafeCopy(str, "ab", 2, "", 0, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "ab"));

        string_utility_type::UnsafeCopy(str, "", 0, "ab", 2, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "ab"));

        string_utility_type::UnsafeCopy(str, "a", 1, "b", 1, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "ab"));

        string_utility_type::UnsafeCopy(str, "a", 1, "bc", 2, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abc"));

        string_utility_type::UnsafeCopy(str, "ab", 2, "c", 1, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abc"));

        string_utility_type::UnsafeCopy(str, "ab", 2, "cd", 2, "", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abcd"));

        string_utility_type::UnsafeCopy(str, "a", 1, "b", 1, "c", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abc"));

        string_utility_type::UnsafeCopy(str, "ab", 2, "c", 1, "d", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abcd"));

        string_utility_type::UnsafeCopy(str, "a", 1, "bc", 2, "d", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abcd"));

        string_utility_type::UnsafeCopy(str, "a", 1, "b", 1, "cd", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, "abcd"));
    }

    {
        typedef ocl::StringUtility<wchar_t> string_utility_type;
        wchar_t str[100];

        string_utility_type::UnsafeCopy(str, L"", 0, L"", 0, L"", 0);
        CHECK_TRUE(str[0] == L'\0');

        string_utility_type::UnsafeCopy(str, L"a", 1, L"", 0, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"a"));

        string_utility_type::UnsafeCopy(str, L"", 0, L"a", 1, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"a"));

        string_utility_type::UnsafeCopy(str, L"ab", 2, L"", 0, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"ab"));

        string_utility_type::UnsafeCopy(str, L"", 0, L"ab", 2, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"ab"));

        string_utility_type::UnsafeCopy(str, L"a", 1, L"b", 1, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"ab"));

        string_utility_type::UnsafeCopy(str, L"a", 1, L"bc", 2, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abc"));

        string_utility_type::UnsafeCopy(str, L"ab", 2, L"c", 1, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abc"));

        string_utility_type::UnsafeCopy(str, L"ab", 2, L"cd", 2, L"", 0);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abcd"));

        string_utility_type::UnsafeCopy(str, L"a", 1, L"b", 1, L"c", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abc"));

        string_utility_type::UnsafeCopy(str, L"ab", 2, L"c", 1, L"d", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abcd"));

        string_utility_type::UnsafeCopy(str, L"a", 1, L"bc", 2, L"d", 1);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abcd"));

        string_utility_type::UnsafeCopy(str, L"a", 1, L"b", 1, L"cd", 2);
        CHECK_TRUE(string_utility_type::UnsafeCompare(str, L"abcd"));
    }
}

TEST_MEMBER_FUNCTION(StringUtility, Find, char_const_ptr_char)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::Find(nullptr, 'A') == 0);
        CHECK_TRUE(string_utility_type::Find("", 'A') == 0);
        CHECK_TRUE(string_utility_type::Find("B", 'A') == 1);
        CHECK_TRUE(string_utility_type::Find("A", 'A') == 0);
        CHECK_TRUE(string_utility_type::Find("AB", 'C') == 2);
        CHECK_TRUE(string_utility_type::Find("ABC", 'A') == 0);
        CHECK_TRUE(string_utility_type::Find("ABC", 'B') == 1);
        CHECK_TRUE(string_utility_type::Find("ABC", 'C') == 2);
    }
}

TEST_MEMBER_FUNCTION(StringUtility, Find, char_const_ptr_char_size_t)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::Find(nullptr, 0, 'A') == 0);
        CHECK_TRUE(string_utility_type::Find("", 0, 'A') == 0);
        CHECK_TRUE(string_utility_type::Find("B", 1, 'A') == 1);
        CHECK_TRUE(string_utility_type::Find("A", 1, 'A') == 0);
        CHECK_TRUE(string_utility_type::Find("AB", 2, 'C') == 2);
        CHECK_TRUE(string_utility_type::Find("ABC", 3, 'A') == 0);
        CHECK_TRUE(string_utility_type::Find("ABC", 3, 'B') == 1);
        CHECK_TRUE(string_utility_type::Find("ABC", 3, 'C') == 2);
    }
}

TEST_MEMBER_FUNCTION(StringUtility, UnsafeFind, char_const_ptr_char)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::UnsafeFind("", 'A') == 0);
        CHECK_TRUE(string_utility_type::UnsafeFind("B", 'A') == 1);
        CHECK_TRUE(string_utility_type::UnsafeFind("A", 'A') == 0);
        CHECK_TRUE(string_utility_type::UnsafeFind("AB", 'C') == 2);
        CHECK_TRUE(string_utility_type::UnsafeFind("ABC", 'A') == 0);
        CHECK_TRUE(string_utility_type::UnsafeFind("ABC", 'B') == 1);
        CHECK_TRUE(string_utility_type::UnsafeFind("ABC", 'C') == 2);
    }
}

TEST_MEMBER_FUNCTION(StringUtility, UnsafeFind, char_const_ptr_char_size_t)
{
    {
        typedef ocl::StringUtility<char> string_utility_type;

        CHECK_TRUE(string_utility_type::UnsafeFind("", 0, 'A') == 0);
        CHECK_TRUE(string_utility_type::UnsafeFind("B", 1, 'A') == 1);
        CHECK_TRUE(string_utility_type::UnsafeFind("A", 1, 'A') == 0);
        CHECK_TRUE(string_utility_type::UnsafeFind("AB", 2, 'C') == 2);
        CHECK_TRUE(string_utility_type::UnsafeFind("ABC", 3, 'A') == 0);
        CHECK_TRUE(string_utility_type::UnsafeFind("ABC", 3, 'B') == 1);
        CHECK_TRUE(string_utility_type::UnsafeFind("ABC", 3, 'C') == 2);
    }
}
