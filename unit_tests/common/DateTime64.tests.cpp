/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/DateTime64.hpp"
#include "../../common/BitUtility.hpp"

TEST_MEMBER_FUNCTION(DateTime64, DateTime64, NA)
{
    ocl::DateTime64 dt;
    CHECK_TRUE(dt.GetDateTime() == 0);
}

TEST_MEMBER_FUNCTION(DateTime64, DateTime64, Date32_const_ref)
{
    ocl::DateTime64 src(7, 8, 1986, 1, 2, 3, 4);
    ocl::DateTime64 dt(src);
    CHECK_TRUE(dt.GetDay() == 7u);
    CHECK_TRUE(dt.GetMonth() == 8u);
    CHECK_TRUE(dt.GetYear() == 1986u);
    CHECK_TRUE(dt.GetMilliseconds() == 1u);
    CHECK_TRUE(dt.GetSeconds() == 2u);
    CHECK_TRUE(dt.GetMinutes() == 3u);
    CHECK_TRUE(dt.GetHours() == 4u);
}

TEST_MEMBER_FUNCTION(DateTime64, DateTime64, Date32_move_ref)
{
    ocl::DateTime64 src(7, 8, 1986, 1, 2, 3, 4);
    ocl::DateTime64 dt(std::move(src));
    CHECK_TRUE(dt.GetDay() == 7u);
    CHECK_TRUE(dt.GetMonth() == 8u);
    CHECK_TRUE(dt.GetYear() == 1986u);
    CHECK_TRUE(dt.GetMilliseconds() == 1u);
    CHECK_TRUE(dt.GetSeconds() == 2u);
    CHECK_TRUE(dt.GetMinutes() == 3u);
    CHECK_TRUE(dt.GetHours() == 4u);

    CHECK_TRUE(src.GetDay() == 0u);
    CHECK_TRUE(src.GetMonth() == 0u);
    CHECK_TRUE(src.GetYear() == 0u);
    CHECK_TRUE(src.GetMilliseconds() == 0u);
    CHECK_TRUE(src.GetSeconds() == 0u);
    CHECK_TRUE(src.GetMinutes() == 0u);
    CHECK_TRUE(src.GetHours() == 0u);
}

TEST_MEMBER_FUNCTION(DateTime64, comparison_operators, NA)
{
    ocl::DateTime64 date_time1a(5, 6, 1986, 1, 2, 3, 4);
    ocl::DateTime64 date_time1b(5, 6, 1986, 1, 2, 3, 4);

    // Date and times greater than date_time1a and date_time1b.
    ocl::DateTime64 date_time2(6, 6, 1986, 1, 2, 3, 4);
    ocl::DateTime64 date_time3(5, 7, 1986, 1, 2, 3, 4);
    ocl::DateTime64 date_time4(5, 6, 1987, 1, 2, 3, 4);
    ocl::DateTime64 date_time5(5, 6, 1986, 2, 2, 3, 4);
    ocl::DateTime64 date_time6(5, 6, 1986, 1, 3, 3, 4);
    ocl::DateTime64 date_time7(5, 6, 1986, 1, 2, 4, 4);
    ocl::DateTime64 date_time8(5, 6, 1986, 1, 2, 3, 5);

    CHECK_TRUE(date_time1a == date_time1b);
    CHECK_FALSE(date_time1a != date_time1b);
    CHECK_TRUE(date_time1a <= date_time1b);
    CHECK_TRUE(date_time1a >= date_time1b);

    CHECK_FALSE(date_time1a == date_time2);
    CHECK_TRUE(date_time1a != date_time2);
    CHECK_TRUE(date_time1a < date_time2);
    CHECK_TRUE(date_time1a <= date_time2);
    CHECK_TRUE(date_time2 > date_time1a);
    CHECK_TRUE(date_time2 >= date_time1a);

    CHECK_FALSE(date_time1a == date_time3);
    CHECK_TRUE(date_time1a != date_time3);
    CHECK_TRUE(date_time1a < date_time3);
    CHECK_TRUE(date_time1a <= date_time3);
    CHECK_TRUE(date_time3 > date_time1a);
    CHECK_TRUE(date_time3 >= date_time1a);

    CHECK_FALSE(date_time1a == date_time4);
    CHECK_TRUE(date_time1a != date_time4);
    CHECK_TRUE(date_time1a < date_time4);
    CHECK_TRUE(date_time1a <= date_time4);
    CHECK_TRUE(date_time4 > date_time1a);
    CHECK_TRUE(date_time4 >= date_time1a);

    CHECK_FALSE(date_time1a == date_time5);
    CHECK_TRUE(date_time1a != date_time5);
    CHECK_TRUE(date_time1a < date_time5);
    CHECK_TRUE(date_time1a <= date_time5);
    CHECK_TRUE(date_time5 > date_time1a);
    CHECK_TRUE(date_time5 >= date_time1a);

    CHECK_FALSE(date_time1a == date_time6);
    CHECK_TRUE(date_time1a != date_time6);
    CHECK_TRUE(date_time1a < date_time6);
    CHECK_TRUE(date_time1a <= date_time6);
    CHECK_TRUE(date_time6 > date_time1a);
    CHECK_TRUE(date_time6 >= date_time1a);

    CHECK_FALSE(date_time1a == date_time7);
    CHECK_TRUE(date_time1a != date_time7);
    CHECK_TRUE(date_time1a < date_time7);
    CHECK_TRUE(date_time1a <= date_time7);
    CHECK_TRUE(date_time7 > date_time1a);
    CHECK_TRUE(date_time7 >= date_time1a);

    CHECK_FALSE(date_time1a == date_time8);
    CHECK_TRUE(date_time1a != date_time8);
    CHECK_TRUE(date_time1a < date_time8);
    CHECK_TRUE(date_time1a <= date_time8);
    CHECK_TRUE(date_time8 > date_time1a);
    CHECK_TRUE(date_time8 >= date_time1a);
}

TEST_MEMBER_FUNCTION(DateTime64, GetSet, NA)
{
    using ocl::DateTime64;

    DateTime64 src(7, 8, 1986, 1, 2, 3, 4);

    {
        DateTime64 dt(std::move(src));
        CHECK_TRUE(dt.GetDay() == 7u);
        CHECK_TRUE(dt.GetMonth() == 8u);
        CHECK_TRUE(dt.GetYear() == 1986u);
        CHECK_TRUE(dt.GetMilliseconds() == 1u);
        CHECK_TRUE(dt.GetSeconds() == 2u);
        CHECK_TRUE(dt.GetMinutes() == 3u);
        CHECK_TRUE(dt.GetHours() == 4u);

        dt.SetDay(5u);
        dt.SetMonth(6u);
        dt.SetYear(2010u);
        dt.SetMilliseconds(7u);
        dt.SetSeconds(8u);
        dt.SetMinutes(9u);
        dt.SetHours(10u);

        CHECK_TRUE(dt.GetDay() == 5u);
        CHECK_TRUE(dt.GetMonth() == 6u);
        CHECK_TRUE(dt.GetYear() == 2010u);
        CHECK_TRUE(dt.GetMilliseconds() == 7u);
        CHECK_TRUE(dt.GetSeconds() == 8u);
        CHECK_TRUE(dt.GetMinutes() == 9u);
        CHECK_TRUE(dt.GetHours() == 10u);
    }

    for (DateTime64::day_type day = DateTime64::MIN_DAY; day <= DateTime64::MAX_DAY; day += 8)
    {
        for (DateTime64::month_type month = DateTime64::MIN_MONTH; month <= DateTime64::MAX_MONTH; month += 4)
        {
            for (DateTime64::year_type year = 1999u; year <= 2005u; ++year)
            {
                for (DateTime64::milliseconds_type milliseconds = DateTime64::MIN_MILLISECONDS;
                     milliseconds <= DateTime64::MAX_MILLISECONDS; milliseconds += 100)
                {
                    for (DateTime64::seconds_type seconds = DateTime64::MIN_SECONDS;
                         seconds <= DateTime64::MAX_SECONDS; seconds += 10)
                    {
                        for (DateTime64::minutes_type minutes = DateTime64::MIN_MINUTES;
                             minutes <= DateTime64::MAX_MINUTES; minutes += 10)
                        {
                            for (DateTime64::hours_type hours = DateTime64::MIN_HOURS;
                                 hours <= DateTime64::MAX_HOURS; hours += 5)
                            {
                                DateTime64 dt;
                                dt.SetDay(day);
                                dt.SetMonth(month);
                                dt.SetYear(year);
                                dt.SetMilliseconds(milliseconds);
                                dt.SetSeconds(seconds);
                                dt.SetMinutes(minutes);
                                dt.SetHours(hours);
                                CHECK_TRUE(dt.GetDay() == day);
                                CHECK_TRUE(dt.GetMonth() == month);
                                CHECK_TRUE(dt.GetYear() == year);
                                CHECK_TRUE(dt.GetMilliseconds() == milliseconds);
                                CHECK_TRUE(dt.GetSeconds() == seconds);
                                CHECK_TRUE(dt.GetMinutes() == minutes);
                                CHECK_TRUE(dt.GetHours() == hours);
                            }
                        }
                    }
                }
            }
        }
    }
}

TEST_MEMBER_FUNCTION(DateTime64, SetSetDateTime, date_type_time_type)
{
    typedef ocl::Date32::date_type date_type;
    typedef ocl::Time32::time_type time_type;

    ocl::DateTime64 dt;
    dt.SetDay(5u);
    dt.SetMonth(6u);
    dt.SetYear(2010u);
    dt.SetMilliseconds(7u);
    dt.SetSeconds(8u);
    dt.SetMinutes(9u);
    dt.SetHours(10u);

    ocl::Date32 d(dt.GetDate());
    ocl::Time32 t(dt.GetTime());

    date_type date_value = d.GetDate();
    time_type time_value = t.GetTime();

    dt.GetDateTime();
    date_type split_date_value;
    time_type split_time_value;
    ocl::DateTime64::date_time_type date_time = dt.GetDateTime();
    ocl::BitUtility::Split(split_date_value, split_time_value, date_time);
    CHECK_TRUE(date_value == split_date_value);
    CHECK_TRUE(time_value == split_time_value);
}
