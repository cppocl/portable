/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/Size.hpp"

TEST_MEMBER_FUNCTION(Size, Size, NA)
{
    typedef ocl::Size<unsigned int> size_type;

    size_type size;
    CHECK_EQUAL(size.Width(), 0U);
    CHECK_EQUAL(size.Height(), 0U);
}

TEST_MEMBER_FUNCTION(Size, Size, int_int)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_ARGS("int, int");

    size_type size(1, 2);
    CHECK_EQUAL(size.Width(), 1U);
    CHECK_EQUAL(size.Height(), 2U);
}

TEST_MEMBER_FUNCTION(Size, Size, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    size_type size(size_type(3, 4));
    CHECK_EQUAL(size.Width(), 3U);
    CHECK_EQUAL(size.Height(), 4U);
}

TEST_MEMBER_FUNCTION(Size, assignment, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_FUNCTION_NAME("=");

    size_type size;
    size = size_type(3, 4);
    CHECK_EQUAL(size.Width(), 3U);
    CHECK_EQUAL(size.Height(), 4U);
}

TEST_MEMBER_FUNCTION(Size, plus_equal, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_FUNCTION_NAME("+=");

    size_type size(1, 2);
    size += size_type(3, 4);
    CHECK_EQUAL(size.Width(), 4U);
    CHECK_EQUAL(size.Height(), 6U);
}

TEST_MEMBER_FUNCTION(Size, minus_equal, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_FUNCTION_NAME("-=");

    size_type size(4, 6);
    size -= size_type(3, 4);
    CHECK_EQUAL(size.Width(), 1U);
    CHECK_EQUAL(size.Height(), 2U);
}

TEST_MEMBER_FUNCTION(Size, multiply_equal, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_FUNCTION_NAME("*=");

    size_type size(4, 6);
    size *= size_type(4, 2);
    CHECK_EQUAL(size.Width(), 16U);
    CHECK_EQUAL(size.Height(), 12U);
}

TEST_MEMBER_FUNCTION(Size, divide_equal, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_FUNCTION_NAME("/=");

    size_type size(4, 12);
    size /= size_type(2, 3);
    CHECK_EQUAL(size.Width(), 2U);
    CHECK_EQUAL(size.Height(), 4U);
}

TEST_MEMBER_FUNCTION(Size, plus, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_FUNCTION_NAME("+");

    size_type size(1, 2);
    size_type size2(size + size_type(3, 4));
    CHECK_EQUAL(size2.Width(), 4U);
    CHECK_EQUAL(size2.Height(), 6U);
}

TEST_MEMBER_FUNCTION(Size, minus, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_FUNCTION_NAME("-");

    size_type size(4, 6);
    size_type size2(size - size_type(3, 4));
    CHECK_EQUAL(size2.Width(), 1U);
    CHECK_EQUAL(size2.Height(), 2U);
}

TEST_MEMBER_FUNCTION(Size, multiply, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_FUNCTION_NAME("*");

    size_type size(4, 6);
    size_type size2(size * size_type(4, 2));
    CHECK_EQUAL(size2.Width(), 16U);
    CHECK_EQUAL(size2.Height(), 12U);
}

TEST_MEMBER_FUNCTION(Size, divide, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    TEST_OVERRIDE_FUNCTION_NAME("/");

    size_type size(4, 12);
    size_type size2(size / size_type(2, 3));
    CHECK_EQUAL(size2.Width(), 2U);
    CHECK_EQUAL(size2.Height(), 4U);
}

TEST_MEMBER_FUNCTION(Size, OffsetWidth, int)
{
    typedef ocl::Size<unsigned int> size_type;

    size_type size(1, 2);
    size.OffsetWidth(2);
    CHECK_EQUAL(size.Width(), 3U);
    CHECK_EQUAL(size.Height(), 2U);

    size.OffsetWidth(-2);
    CHECK_EQUAL(size.Width(), 1U);
    CHECK_EQUAL(size.Height(), 2U);
}

TEST_MEMBER_FUNCTION(Size, OffsetHeight, int)
{
    typedef ocl::Size<unsigned int> size_type;

    size_type size(1, 2);
    size.OffsetHeight(2);
    CHECK_EQUAL(size.Width(), 1U);
    CHECK_EQUAL(size.Height(), 4U);

    size.OffsetHeight(-2);
    CHECK_EQUAL(size.Width(), 1U);
    CHECK_EQUAL(size.Height(), 2U);
}

TEST_MEMBER_FUNCTION(Size, Translate, NA)
{
    typedef ocl::Size<unsigned int> size_type;

    size_type size(1, 2);
    ocl::Size<int> size2(size.Translate());

    CHECK_EQUAL(size2.Width(), 1);
    CHECK_EQUAL(size2.Height(), 2);
}

TEST_MEMBER_FUNCTION(Size, Translate, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    ocl::Size<int> size(size_type::Translate(size_type(1, 2)));

    CHECK_EQUAL(size.Width(), 1);
    CHECK_EQUAL(size.Height(), 2);
}

TEST_MEMBER_FUNCTION(Size, GetDiff, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    size_type pt1(1, 2);
    size_type size2(2, 4);

    ocl::Size<int> diff = pt1.GetDiff(size2);

    CHECK_EQUAL(diff.Width(), -1);
    CHECK_EQUAL(diff.Height(), -2);
}

TEST_MEMBER_FUNCTION(Size, GetAbsDiff, Size)
{
    typedef ocl::Size<unsigned int> size_type;

    size_type pt1(1, 2);
    size_type size2(2, 4);

    size_type diff = pt1.GetAbsDiff(size2);

    CHECK_EQUAL(diff.Width(), 1U);
    CHECK_EQUAL(diff.Height(), 2U);
}
