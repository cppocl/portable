/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/PathUtility.hpp"
#include "../../common/StringUtility.hpp"

TEST_MEMBER_FUNCTION(PathUtility, CombinePath, char_ptr_ref_char_const_ptr_char_const_ptr)
{
    {
        typedef char char_type;
        typedef std::size_t size_type;
        typedef ocl::PathUtility<char_type, size_type, false> path_utility_type;

        char_type* combined_path = nullptr;
        path_utility_type::CombinePath(combined_path, nullptr, nullptr, '\\', '/');
        CHECK_TRUE(combined_path == nullptr);

        path_utility_type::CombinePath(combined_path, "A", nullptr, '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A") == 0);

        path_utility_type::CombinePath(combined_path, nullptr, "A", '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A") == 0);

        path_utility_type::CombinePath(combined_path, "A", "B", '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A\\B") == 0);

        path_utility_type::CombinePath(combined_path, "A", "B", '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A/B") == 0);

        path_utility_type::CombinePath(combined_path, "A\\", "B", '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A\\B") == 0);

        path_utility_type::CombinePath(combined_path, "A/", "B", '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A/B") == 0);

        path_utility_type::Free(combined_path);
    }

    {
        typedef wchar_t char_type;
        typedef std::size_t size_type;
        typedef ocl::PathUtility<char_type, size_type, false> path_utility_type;

        char_type* combined_path = nullptr;
        path_utility_type::CombinePath(combined_path, nullptr, nullptr, L'\\', L'/');
        CHECK_TRUE(combined_path == nullptr);

        path_utility_type::CombinePath(combined_path, L"A", nullptr, L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A") == 0);

        path_utility_type::CombinePath(combined_path, nullptr, L"A", L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A") == 0);

        path_utility_type::CombinePath(combined_path, L"A", L"B", L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A\\B") == 0);

        path_utility_type::CombinePath(combined_path, L"A", L"B", L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A/B") == 0);

        path_utility_type::CombinePath(combined_path, L"A\\", L"B", L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A\\B") == 0);

        path_utility_type::CombinePath(combined_path, L"A/", L"B", L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A/B") == 0);

        path_utility_type::Free(combined_path);
    }

    {
        typedef char char_type;
        typedef std::size_t size_type;
        typedef ocl::PathUtility<char_type, size_type, true> path_utility_type;

        char_type* combined_path = nullptr;
        path_utility_type::CombinePath(combined_path, nullptr, nullptr, '\\', '/');
        CHECK_TRUE(combined_path == nullptr);

        path_utility_type::CombinePath(combined_path, "A", nullptr, '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A") == 0);

        path_utility_type::CombinePath(combined_path, nullptr, "A", '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A") == 0);

        path_utility_type::CombinePath(combined_path, "A", "B", '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A\\B") == 0);

        path_utility_type::CombinePath(combined_path, "A", "B", '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A/B") == 0);

        path_utility_type::CombinePath(combined_path, "A\\", "B", '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A\\B") == 0);

        path_utility_type::CombinePath(combined_path, "A/", "B", '\\', '/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, "A/B") == 0);

        path_utility_type::Free(combined_path);
    }

    {
        typedef wchar_t char_type;
        typedef std::size_t size_type;
        typedef ocl::PathUtility<char_type, size_type, true> path_utility_type;

        char_type* combined_path = nullptr;
        path_utility_type::CombinePath(combined_path, nullptr, nullptr, L'\\', L'/');
        CHECK_TRUE(combined_path == nullptr);

        path_utility_type::CombinePath(combined_path, L"A", nullptr, L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A") == 0);

        path_utility_type::CombinePath(combined_path, nullptr, L"A", L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A") == 0);

        path_utility_type::CombinePath(combined_path, L"A", L"B", L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A\\B") == 0);

        path_utility_type::CombinePath(combined_path, L"A", L"B", L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A/B") == 0);

        path_utility_type::CombinePath(combined_path, L"A\\", L"B", L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A\\B") == 0);

        path_utility_type::CombinePath(combined_path, L"A/", L"B", L'\\', L'/');
        CHECK_TRUE(combined_path != nullptr);
        CHECK_TRUE(StrCmp(combined_path, L"A/B") == 0);

        path_utility_type::Free(combined_path);
    }
}
