/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../common/StringCopy.hpp"
#include "../../common/StringUtility.hpp"
#include <cstring>
#include <cstddef>

TEST_MEMBER_FUNCTION(StringCopy, AllocCopyAndFree, char_ptr_size_t)
{
    {
        typedef char char_type;
        typedef std::size_t size_type;
        constexpr bool can_throw = false;
        typedef ocl::StringCopy<char_type, size_type, can_throw> string_copy_type;

        char_type* dest = nullptr;
        size_type dest_len = 0;

        string_copy_type::AllocCopy(dest, dest_len, nullptr, 0);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, "", 0);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, "abc", 3);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 3U);
        CHECK_TRUE(StrCmp(dest, "abc") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, "a", 1, "bc", 2);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 3U);
        CHECK_TRUE(StrCmp(dest, "abc") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, "a", 1, "bc", 2, "def", 3);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 6U);
        CHECK_TRUE(StrCmp(dest, "abcdef") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);
    }

    {
        typedef wchar_t char_type;
        typedef std::size_t size_type;
        constexpr bool can_throw = false;
        typedef ocl::StringCopy<char_type, size_type, can_throw> string_copy_type;
        char_type* dest = nullptr;
        size_type dest_len = 0;

        string_copy_type::AllocCopy(dest, dest_len, nullptr, 0);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, L"", 0);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, L"abc", 3);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 3U);
        CHECK_TRUE(StrCmp(dest, L"abc") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, L"a", 1, L"bc", 2);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 3U);
        CHECK_TRUE(StrCmp(dest, L"abc") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, L"a", 1, L"bc", 2, L"def", 3);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 6U);
        CHECK_TRUE(StrCmp(dest, L"abcdef") == 0);

        string_copy_type::Free(dest);
        CHECK_TRUE(dest == nullptr);

        string_copy_type::AllocCopy(dest, L"abc");
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(StrLen(dest) == 3U);
    }

    {
        typedef char char_type;
        typedef std::size_t size_type;
        constexpr bool can_throw = true;
        typedef ocl::StringCopy<char_type, size_type, can_throw> string_copy_type;

        char_type* dest = nullptr;
        size_type dest_len = 0;

        string_copy_type::AllocCopy(dest, dest_len, nullptr, 0);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, "", 0);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, "abc", 3);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 3U);
        CHECK_TRUE(StrCmp(dest, "abc") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, "a", 1, "bc", 2);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 3U);
        CHECK_TRUE(StrCmp(dest, "abc") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, "a", 1, "bc", 2, "def", 3);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 6U);
        CHECK_TRUE(StrCmp(dest, "abcdef") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);
    }

    {
        typedef wchar_t char_type;
        typedef std::size_t size_type;
        constexpr bool can_throw = true;
        typedef ocl::StringCopy<char_type, size_type, can_throw> string_copy_type;
        char_type* dest = nullptr;
        size_type dest_len = 0;

        string_copy_type::AllocCopy(dest, dest_len, nullptr, 0);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, L"", 0);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, L"abc", 3);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 3U);
        CHECK_TRUE(StrCmp(dest, L"abc") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, L"a", 1, L"bc", 2);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 3U);
        CHECK_TRUE(StrCmp(dest, L"abc") == 0);

        string_copy_type::Free(dest, dest_len);
        CHECK_TRUE(dest == nullptr);
        CHECK_TRUE(dest_len == 0U);

        string_copy_type::AllocCopy(dest, dest_len, L"a", 1, L"bc", 2, L"def", 3);
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(dest_len == 6U);
        CHECK_TRUE(StrCmp(dest, L"abcdef") == 0);

        string_copy_type::Free(dest);
        CHECK_TRUE(dest == nullptr);

        string_copy_type::AllocCopy(dest, L"abc");
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(StrLen(dest) == 3U);
    }
}

TEST_MEMBER_FUNCTION(StringCopy, UnsafeAllocCopyAndFree, char_ptr_size_t)
{
    {
        typedef char char_type;
        typedef std::size_t size_type;
        constexpr bool can_throw = false;
        typedef ocl::StringCopy<char_type, size_type, can_throw> string_copy_type;

        char_type* dest = nullptr;

        string_copy_type::UnsafeAllocCopy(dest, "abc");
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(StrLen(dest) == 3U);

        string_copy_type::Free(dest);
        CHECK_TRUE(dest == nullptr);
    }

    {
        typedef wchar_t char_type;
        typedef std::size_t size_type;
        constexpr bool can_throw = false;
        typedef ocl::StringCopy<char_type, size_type, can_throw> string_copy_type;

        char_type* dest = nullptr;

        string_copy_type::UnsafeAllocCopy(dest, L"abc");
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(StrLen(dest) == 3U);

        string_copy_type::Free(dest);
        CHECK_TRUE(dest == nullptr);
    }

    {
        typedef char char_type;
        typedef std::size_t size_type;
        constexpr bool can_throw = true;
        typedef ocl::StringCopy<char_type, size_type, can_throw> string_copy_type;

        char_type* dest = nullptr;

        string_copy_type::UnsafeAllocCopy(dest, "abc");
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(StrLen(dest) == 3U);

        string_copy_type::Free(dest);
        CHECK_TRUE(dest == nullptr);
    }

    {
        typedef wchar_t char_type;
        typedef std::size_t size_type;
        constexpr bool can_throw = true;
        typedef ocl::StringCopy<char_type, size_type, can_throw> string_copy_type;

        char_type* dest = nullptr;

        string_copy_type::UnsafeAllocCopy(dest, L"abc");
        CHECK_TRUE(dest != nullptr);
        CHECK_TRUE(StrLen(dest) == 3U);

        string_copy_type::Free(dest);
        CHECK_TRUE(dest == nullptr);
    }
}
