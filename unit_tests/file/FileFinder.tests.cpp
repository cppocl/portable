/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../file/FileFinder.hpp"
#include "../../file/PathConstants.hpp"
#include "../../common/CharConstants.hpp"
#include <cstring>
#include <memory>

namespace
{
    template<typename CharType>
    static CharType const* GetRootPath() noexcept
    {
        return ocl::PathConstants<CharType>::GetRootPath();
    }

    template<typename CharType>
    static CharType const* GetAllFilesWildCard() noexcept
    {
        return ocl::PathConstants<CharType>::GetAllFilesWildCard();
    }
}

TEST_MEMBER_FUNCTION(FileFinder, FileFinder, NA)
{
    ocl::FileFinder<char> ff;
    CHECK_FALSE(ff.IsFinding());
    auto const& file_data = ff.GetFileData();
    CHECK_TRUE(file_data.GetFilename() == nullptr);
    CHECK_TRUE(file_data.GetFilenameLength() == 0U);
    CHECK_TRUE(file_data.GetFileSize() == 0U);
}

TEST_MEMBER_FUNCTION(FileFinder, FindFirst_FindNext, NA)
{
    typedef char char_type;
    ocl::FileFinder<char_type> ff;
    bool success = ff.FindFirst(GetRootPath<char_type>(), GetAllFilesWildCard<char_type>());
    CHECK_TRUE(success);
    auto const& file_data_first = ff.GetFileData();
    CHECK_TRUE(file_data_first.GetFilename() != nullptr);
    CHECK_TRUE(ff.GetFilename() != nullptr);
    CHECK_TRUE((ff.GetFilename() == file_data_first.GetFilename()) ||
               (StrCmp(ff.GetFilename(), file_data_first.GetFilename()) == 0));
    if (!file_data_first.IsDirectory())
    {
        CHECK_FALSE(ff.IsDirectoryImportant());
        CHECK_FALSE(ff.IsDirectoryUnimportant());
    }

    char_type* prev_filename = new char_type[file_data_first.GetFilenameLength() + 1];
    StrCpy(prev_filename, file_data_first.GetFilename());

    while (ff.FindNext())
    {
        auto const& file_data_next = ff.GetFileData();
        CHECK_TRUE(file_data_next.GetFilename() != nullptr);
        CHECK_TRUE((ff.GetFilename() == file_data_next.GetFilename()) ||
            (StrCmp(ff.GetFilename(), file_data_next.GetFilename()) == 0));
        if (!file_data_next.IsDirectory())
        {
            CHECK_FALSE(ff.IsDirectoryImportant());
            CHECK_FALSE(ff.IsDirectoryUnimportant());
        }

        int cmp = StrCmp(prev_filename, file_data_next.GetFilename());
        CHECK_TRUE(cmp != 0);
        if (cmp == 0)
            break;

        delete[] prev_filename;
        prev_filename = new char_type[file_data_next.GetFilenameLength() + 1];
        StrCpy(prev_filename, file_data_next.GetFilename());
    }

    delete[] prev_filename;
}
