/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"

#include "../../file/RecursiveFileFinder.hpp"
#include "../../file/PathConstants.hpp"
#include "../../common/CharConstants.hpp"
#include <cstring>
#include <string>
#include <memory>
#include <iostream>

namespace
{
    template<typename CharType>
    static CharType const* GetRootPath() noexcept
    {
        return ocl::PathConstants<CharType>::GetRootPath();
    }

    template<typename CharType>
    static CharType const* GetAllFilesWildCard() noexcept
    {
        return ocl::PathConstants<CharType>::GetAllFilesWildCard();
    }
}

TEST_MEMBER_FUNCTION(RecursiveFileFinder, RecursiveFileFinder, NA)
{
    ocl::RecursiveFileFinder<char> ff;
    CHECK_FALSE(ff.IsFinding());
    auto const& file_data = ff.GetFileData();
    CHECK_TRUE(file_data.GetFilename() == nullptr);
    CHECK_TRUE(file_data.GetFilenameLength() == 0U);
    CHECK_TRUE(file_data.GetFileSize() == 0U);
}

namespace
{
    template<typename CharType>
    static bool Compare(CharType const* str1, CharType const* str2) noexcept
    {
        while (*str1 != static_cast<CharType>(0))
        {
            if (*str1 != *str2)
                return false;
            ++str1;
            ++str2;
        }
        return *str1 == *str2;
    }

    template<typename CharType>
    struct UnimportantFolders
    {
        static constexpr CharType const* Dot()    noexcept { return "."; }
        static constexpr CharType const* DotDot() noexcept { return ".."; }
    };

    template<>
    struct UnimportantFolders<wchar_t>
    {
        typedef wchar_t CharType;
        static constexpr CharType const* Dot()    noexcept { return L"."; }
        static constexpr CharType const* DotDot() noexcept { return L".."; }
    };

    template<typename CharType>
    static bool IsImportant(CharType const* folder_name) noexcept
    {
        if (Compare(folder_name, UnimportantFolders<CharType>::Dot()) ||
            Compare(folder_name, UnimportantFolders<CharType>::DotDot()))
        {
            return false;
        }
        return true;
    }

    template<typename CharType>
    void DebugPrint(ocl::RecursiveFileFinder<CharType> const& ff, bool show_parent = false)
    {
        typedef ocl::RecursiveFileFinder<CharType> file_finder_type;
        typedef typename file_finder_type::file_data_type file_data_type;

        char const sep_str[] = { file_finder_type::SeparatorChar, file_finder_type::NullChar };

        file_data_type const& file_data = ff.GetFileData();
        CharType const* fname = file_data.GetFilename();
        std::string s(ff.GetCurrentPath());
        s += sep_str;
        s += fname;

        file_finder_type const* parent = ff.GetParent();
        if (show_parent && parent != nullptr)
        {
            s += " - Parent: ";
            s += parent->GetFullPath();
        }

        std::cout << s << std::endl;
    }
}

TEST_MEMBER_FUNCTION(RecursiveFileFinder, FindFirst_FindNext, NA)
{
    typedef char char_type;
    typedef ocl::RecursiveFileFinder<char_type> file_finder_type;

    bool debug_print = false;

    file_finder_type ff(1);
    char_type const* full_path = GetRootPath<char_type>();

    bool success = ff.FindFirst(full_path, GetAllFilesWildCard<char_type>());
    CHECK_TRUE(success);
    auto const& file_data_first = ff.GetFileData();
    CHECK_TRUE(file_data_first.GetFilename() != nullptr);
    CHECK_TRUE(ff.GetFilename() != nullptr);
    CHECK_TRUE((ff.GetFilename() == file_data_first.GetFilename()) ||
               (StrCmp(ff.GetFilename(), file_data_first.GetFilename()) == 0));

    if (debug_print && IsImportant<char_type>(file_data_first.GetFilename()))
        DebugPrint(ff);

    if (!file_data_first.IsDirectory())
    {
        CHECK_FALSE(ff.IsDirectoryImportant());
        CHECK_FALSE(ff.IsDirectoryUnimportant());
    }

    while (ff.FindNext())
    {
        auto const& file_data_next = ff.GetFileData();
        CHECK_TRUE(file_data_next.GetFilename() != nullptr);
        CHECK_TRUE((ff.GetFilename() == file_data_next.GetFilename()) ||
                   (StrCmp(ff.GetFilename(), file_data_next.GetFilename()) == 0));

        if (debug_print && IsImportant<char_type>(file_data_next.GetFilename()))
            DebugPrint(ff);

        if (!file_data_next.IsDirectory())
        {
            CHECK_FALSE(ff.IsDirectoryImportant());
            CHECK_FALSE(ff.IsDirectoryUnimportant());
        }
    }

    CHECK_FALSE(ff.IsFinding());
}
