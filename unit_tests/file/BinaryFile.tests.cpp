/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../../unit_test_framework/test/Test.hpp"
#include "../../file/BinaryFile.hpp"
#include "../../common/StringUtility.hpp"
#include <cstring>
#include <memory>

namespace
{
    template<typename CharType>
    std::size_t StrLen(CharType const* str)
    {
        return ocl::StringUtility<CharType>::UnsafeGetLength(str);
    }

    template<typename CharType>
    bool Compare(CharType const* str1, CharType const* str2)
    {
        return ocl::StringUtility<CharType>::UnsafeCompare(str1, str2);
    }
}

TEST_MEMBER_FUNCTION(BinaryFile, IsOpen, NA)
{
    ocl::BinaryFile<wchar_t> binary_file;
    CHECK_FALSE(binary_file.IsOpen());
}

TEST_MEMBER_FUNCTION(BinaryFile, OpenReadWriteCloseDelete, char_type_ptr_modes)
{
    using ocl::BinaryFile;

    typedef wchar_t char_type;
    typedef BinaryFile<char_type> binary_file_type;

    binary_file_type binary_file;

    //
    // Test Open part.
    //

    char_type const* filename = L"temp.txt";

    binary_file_type::OpenModes open_mode = binary_file_type::OpenModes::CreateAlways;
    binary_file_type::AccessModes access_mode = binary_file_type::AccessModes::ReadWrite;
    binary_file_type::ShareModes share_mode = binary_file_type::ShareModes::ShareNone;
    binary_file_type::Behaviour behavioural_modes = binary_file_type::Behaviour::BehaviourNone;
    binary_file_type::Attributes file_attributes = binary_file_type::Attributes::AttributeNormal;
    binary_file_type::Security security_modes = binary_file_type::Security::SecurityUserRead |
                                                binary_file_type::Security::SecurityUserWrite;

    bool success = binary_file.Open(filename,
                                    open_mode, access_mode, share_mode,
                                    file_attributes, behavioural_modes, security_modes);
    CHECK_TRUE(success);

    //
    // Test Write part.
    //
    char const write_data[] = "A test line\nAnother test line\n";
    std::size_t write_data_size = sizeof(write_data) - 1;
    success = binary_file.Write(write_data, write_data_size);
    CHECK_TRUE(success);

    success = binary_file.Close();
    CHECK_TRUE(success);

    //
    // Test Read part.
    //

    open_mode = binary_file_type::OpenModes::OpenExisting;
    success = binary_file.Open(filename, open_mode, access_mode, share_mode,
                               file_attributes, behavioural_modes, security_modes);
    CHECK_TRUE(success);

    std::uint64_t file_size = binary_file.GetFileSize();
    CHECK_EQUAL(file_size, write_data_size);

    char* read_data = new char[write_data_size];
    binary_file.Read(read_data, write_data_size);
    CHECK_EQUAL(::memcmp(read_data, write_data, write_data_size), 0);
    delete[] read_data;

    success = binary_file.Close();
    CHECK_TRUE(success);

    //
    // Test Delete part.
    //

    binary_file_type::error_type last_error = 0;
    success = binary_file_type::DeleteFile(filename, last_error);
    CHECK_TRUE(success);
    CHECK_ZERO(last_error);
}

TEST_MEMBER_FUNCTION(BinaryFile, OpenWriteSeekClose, char_type_ptr_modes)
{
    using ocl::BinaryFile;

    typedef wchar_t char_type;
    typedef BinaryFile<char_type> binary_file_type;

    binary_file_type binary_file;

    //
    // Test Open part.
    //

    binary_file_type::OpenModes open_mode = binary_file_type::OpenModes::CreateAlways;
    binary_file_type::AccessModes access_mode = binary_file_type::AccessModes::ReadWrite;
    binary_file_type::ShareModes share_mode = binary_file_type::ShareModes::ShareNone;
    binary_file_type::Behaviour behavioural_modes = binary_file_type::Behaviour::BehaviourNone;
    binary_file_type::Attributes file_attributes = binary_file_type::Attributes::AttributeNormal;
    binary_file_type::Security security_modes = binary_file_type::Security::SecurityNone;

    char_type const* filename = L"temp.txt";

    bool success = binary_file.Open(filename, open_mode, access_mode, share_mode,
                                    file_attributes, behavioural_modes, security_modes);
    CHECK_TRUE(success);

    //
    // Test Write part.
    //
    char const write_data[] = "A test line\nAnother test line\n";
    std::size_t write_data_size = sizeof(write_data) - 1;
    success = binary_file.Write(reinterpret_cast<unsigned char const*>(write_data), write_data_size);
    CHECK_TRUE(success);

    //
    // Test Seek part.
    //

    success = binary_file.Seek(0);
    CHECK_TRUE(success);

    success = binary_file.Seek(write_data_size);
    CHECK_TRUE(success);

    success = binary_file.SeekFromEnd(0);
    CHECK_TRUE(success);

    success = binary_file.Seek(0);
    CHECK_TRUE(success);

    success = binary_file.Close();
    CHECK_TRUE(success);

    //
    // Test GetFileSize call.
    //

    binary_file_type::size_type file_size = binary_file_type::GetFileSize(filename);
    CHECK_EQUAL(file_size, write_data_size);

    //
    // Test Delete part.
    //

    binary_file_type::error_type last_error = 0;
    success = binary_file_type::DeleteFile(filename, last_error);
    CHECK_TRUE(success);
    CHECK_ZERO(last_error);
}

