Overview
========

The implementation of cross platform follows the following pattern of three layers:

1. Implementation specific to a platform, such as Windows or Linux.
   In some cases Posix implementation is provided and re-used for Linux, Mac, or any other Posix compliant platform.

2. Porting layer, which is the only part of the the porting layer that will examine the platform or compiler.

3. The common interface, which is a wrapper of the porting layer and provides an interface which is the same
   for all platforms.


Guidance on the layers
======================

Design the common interface early, as this can help to reduce the amount of handling of porting issues between layers 1 and 2.

1. The platform layer only needs to concern itself with platform specific issue, but it can consider common parameters,
   constants and enumerations if this can aid in simplifying the porting layer.

2. This layer only needs to implement an API for use by the platform independent 3rd layer, which will handle any specific platform issues.

3. This layer provides a common API for all platforms, ensuring any porting specific detail is not visible to the user of the API.
