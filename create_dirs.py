#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
from datetime import datetime

platforms = ("Android", "Bsd", "Linux", "Mac", "Unix", "Win", "Posix")

## Return lines containing a default class implementation.
def get_class_name_lines(class_name):
    lines = [
        "class {}".format(class_name),
        "{",
        "public:",
        "    {}();".format(class_name),
        "    ~{}();".format(class_name),
        "",
        "    {}({} const&) = delete;".format(class_name, class_name),
        "    {}({}&&) = delete;".format(class_name, class_name),
        "    {}& operator=({} const&) = delete;".format(class_name, class_name),
        "    {}& operator=({}&&) = delete;".format(class_name, class_name),
        "};"]
    return lines

## Return lines for a class implementation for .inl file.
def get_class_name_implementation_lines(class_name):
    lines = [
        "{}::{}()".format(class_name, class_name),
        "{",
        "}",
        "",
        "~{}::{}()".format(class_name, class_name),
        "{",
        "}"]
    return lines

## Return lines that will be used as the starting point for using the platform implementation.
def get_header_lines(file_name, class_name):
    inl_file_name = file_name[:-4] + ".inl" if file_name.endswith(".hpp") else ""
    lines = ['#include "platform/Platform{}"'.format(file_name),
             "",
             "namespace ocl",
             "{"]

    if class_name:
        lines.append("")
        lines.extend(get_class_name_lines(class_name))

    if inl_file_name:
        lines.extend(["", '#include "internal/{}"'.format(inl_file_name), ""])
    elif class_name:
        lines.append("")
    lines.append("} // namespace ocl")
    return lines

## Generate lines that will be written to the Platform*.hpp header file.
## The file name will exclude the leading "Platform" part of the filename.
def get_platform_lines(file_name, class_name):
    global platforms

    lines = ['#include "../../common/PlatformDefines.hpp"',
             "",
             "#if OCL_PLATFORM == OCL_PLATFORM_WINDOWS",
             "    #if OCL_COMPILER_POSIX_SUPPORT == 0",
             '    #include "win/Win{}"'.format(file_name),
             "    // TODO: Add any Windows porting code here",
             "    #else // OCL_COMPILER_POSIX_SUPPORT != 0",
             '    #include "posix/Posix{}"'.format(file_name),
             "    #endif",
             "    // TODO: Add any Windows porting code here for Cygwin, MinGW, etc."]

    for platform in platforms:
        if platform != "Win" and platform != "Posix":
            lines.append("#elif OCL_PLATFORM == {}".format(platform.upper()))
            lines.append('    #include "{}/{}{}"'.format(platform.lower(), platform, file_name))
            lines.append("    // TODO: Add any {} porting code here.".format(platform))

    # Add default Posix preprocessor section.
    lines.append("#else")
    lines.append("    // If platform cannot be detected then attempt "
                 "the Posix solution as a generic alternative.")
    lines.append('    #include "{}/{}{}"'.format("posix", "Posix", file_name))
    lines.append("#endif")
    return lines

# Generate header file guard using path and filename.
def get_header_guard(full_path, file_name):
    parent_cwd = os.path.split(os.getcwd())[0]
    if len(full_path) > len(parent_cwd) and parent_cwd == full_path[:len(parent_cwd)]:
        full_path = full_path[len(parent_cwd) + 1:]
    full_path = os.path.join(full_path, file_name)
    header_guard = "OCL_GUARD_" + full_path.replace(os.sep, "_").replace(".", "_").upper()
    return header_guard

# Create a directory and handle any exceptions.
def create_dir(full_path):
    try:
        if not os.path.isdir(full_path):
            os.mkdir(full_path)
            print("mkdir {}".format(full_path))
    except:
        sys.stderr.write("Unable to create directory: {}\n".format(full_path))
        return False
    return True

# Create all the platform sub-directories.
def create_dirs(full_path):
    global platforms

    if not create_dir(full_path):
        return False

    if not create_dir(os.path.join(full_path, "internal")):
        return False

    full_path = os.path.join(full_path, "platform")
    if not create_dir(full_path):
        return False

    for platform in platforms:
        full_platform_path = os.path.join(full_path, platform.lower())
        if not create_dir(full_platform_path):
            return False
    return True

# Create a header or inline file containing copyright and header guard for .hpp files.
def create_file(full_path, file_name, lines = None):
    copyright = (
        "/*",
        "Copyright {} Colin Girling".format(datetime.now().year),
        "",
        "Licensed under the Apache License, Version 2.0 (the \"License\");",
        "you may not use this file except in compliance with the License.",
        "You may obtain a copy of the License at",
        "",
        "    http://www.apache.org/licenses/LICENSE-2.0",
        "",
        "Unless required by applicable law or agreed to in writing, software",
        "distributed under the License is distributed on an \"AS IS\" BASIS,",
        "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.",
        "See the License for the specific language governing permissions and",
        "limitations under the License.",
        "*/",
        "")

    full_file_path = os.path.join(full_path, file_name)

    if not os.path.exists(full_file_path):
        f = None
        try:
            f = open(full_file_path, "w+")
        except:
            sys.stderr.write("Unable to create file: {}\n".format(full_file_path))
            return False

        for line in copyright:
            f.write("{}\n".format(line))

        is_header_file = file_name.endswith(".hpp")
        if is_header_file:
            header_guard = get_header_guard(full_path, file_name)
            f.write("#ifndef {}\n".format(header_guard))
            f.write("#define {}\n".format(header_guard))
            f.write("\n")

        if lines:
            for line in lines:
                f.write("{}\n".format(line))
            f.write("\n")

        if is_header_file:
            f.write("#endif /*{}*/\n".format(header_guard))

        f.close()

    return True

# Create stub header files for each directory.
def create_header_files(full_path, file_name, class_name):
    global platforms

    # Create the main header file that will use the header from the platform directory.
    header_lines = get_header_lines(file_name, class_name)
    if not create_file(full_path, file_name, header_lines):
        return False

    # Create .inl file for the main header file.
    if file_name.endswith(".hpp"):
        inl_file_name = file_name[:-4] + ".inl"
        inl_lines = get_class_name_implementation_lines(class_name)
        if not create_file(os.path.join(full_path, "internal"), inl_file_name, inl_lines):
            return False

    # Create header file within the platform directory.
    full_path = os.path.join(full_path, "platform")
    platform_lines = get_platform_lines(file_name, class_name)
    if not create_file(full_path, "Platform" + file_name, platform_lines):
        return False

    # Create header files for each platform.
    # For Windows just add the class, otherwise add #if #else for class and posix include.
    for platform in platforms:
        is_win = platform == "Win"
        is_posix = platform == "Posix"
        want_hash_if_def = not is_win and not is_posix
        lines = []
        if class_name:
            if want_hash_if_def: lines.append("#if 0")
            lines.extend(get_class_name_lines(platform + class_name))
            if want_hash_if_def:
                lines.append("#else")
        if want_hash_if_def:
            lines.append('#include "../posix/Posix{}"'.format(file_name))
            if class_name:
                lines.append("#endif")

        if not create_file(os.path.join(full_path, platform), platform + file_name, lines):
            return False

    return True

# Create directory structure with header file stubs.
if __name__ == "__main__":

    if len(sys.argv) > 2:
        dir_name = sys.argv[1]
        header_file_name = sys.argv[2]
        class_name = sys.argv[3] if len(sys.argv) > 3 else ""
        full_path = os.path.join(os.getcwd(), dir_name)
        if create_dirs(full_path) and \
           create_header_files(full_path, header_file_name, class_name):
            print("Success")
    else:
        sys.stderr.write("Need to specify at least two arguments.\n")
        sys.stderr.write("First argument is directory name, e.g. file.\n")
        sys.stderr.write("Second argument is name of header file stored in directory.\n")
        sys.stderr.write("Third argument is optional name of the class.\n")
