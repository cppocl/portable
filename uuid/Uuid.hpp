/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_UUID_UUID_HPP
#define OCL_GUARD_PORTABLE_UUID_UUID_HPP

#include "platform/PlatformUuid.hpp"

namespace ocl
{

template<typename CharType>
class Uuid
{
public:
    typedef CharType char_type;

    /// Get the UUID as a string.
    CharType const* GetString() const noexcept;

    /// Create a new UUID.
    bool Generate();

private:
    PlatformUuid<CharType> m_uuid;
};

#include "internal/Uuid.inl"

} // namespace ocl

#endif /*OCL_GUARD_PORTABLE_UUID_UUID_HPP*/
