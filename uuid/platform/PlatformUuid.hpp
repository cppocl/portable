/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_UUID_PLATFORM_PLATFORMUUID_HPP
#define OCL_GUARD_PORTABLE_UUID_PLATFORM_PLATFORMUUID_HPP

#include "../../../compiler/compiler.hpp"
#include "../../../platform/platform.hpp"

#if (OCL_PLATFORM == OCL_PLATFORM_WINDOWS) && (OCL_COMPILER == OCL_COMPILER_MSVC)

#include "win/WinUuid.hpp"

namespace ocl
{
    template<typename char_type>
    using PlatformUuid = WinUuid<char_type>;
} // namespace ocl

#else

namespace ocl
{
    template<typename CharType>
    class PlatformUuid
    {
    public:
        /// Get the UUID as a string.
        CharType const* GetString() const noexcept
        {
            return nullptr;
        }

        /// Create a new UUID.
        bool Generate()
        {
            return false;
        }
    };
} // namespace ocl

#endif

#endif /*OCL_GUARD_PORTABLE_UUID_PLATFORM_PLATFORMUUID_HPP*/
