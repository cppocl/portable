/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PORTABLE_UUID_PLATFORM_WIN_WINUUID_HPP
#define OCL_GUARD_PORTABLE_UUID_PLATFORM_WIN_WINUUID_HPP

#include <Rpc.h>
#include <windows.h>
#include <cstddef>

namespace ocl
{

/// Manage a UUID, including generating one, or getting a string.
/// To use this class you will need to include Rpcrt4.lib to your linker libraries.
template<typename CharType>
class WinUuid
{
public:
    /// Default constructor will generate a UUID automatically.
    WinUuid();

    ~WinUuid();

public:
    ::UUID const& GetUUID() const noexcept;

    /// Get the UUID as a string.
    CharType const* GetString() const noexcept;

    /// Create a new UUID.
    bool Generate();

/// Static helpers (internal use only)
private:
    static std::size_t GetLength(RPC_CSTR str);
    static std::size_t GetLength(RPC_WSTR str);

    static RPC_STATUS GetUuidString(::UUID const& uuid, RPC_CSTR& uuid_str);
    static RPC_STATUS GetUuidString(::UUID const& uuid, RPC_WSTR& uuid_str);

    static void UuidStringFree(RPC_CSTR& uuid_str);
    static void UuidStringFree(RPC_WSTR& uuid_str);

    /// Helper to handle conversion from RPC_CSTR or RPC_WSTR to char and wchar_t.
    template<typename T1, typename T2>
    void StringCopy(T1* dest, T2 const* src)
    {
        T1* tmp = dest;
        for (; *src != 0; ++tmp, ++src)
            *tmp = static_cast<T1>(*src);
        *tmp = 0;
    }

/// Types (internal use only)
private:
    template<typename T, typename unused>
    struct InternalTypes;

    template<typename unused>
    struct InternalTypes<char, unused>
    {
        typedef RPC_CSTR RPC_STR_TYPE;
    };

    template<typename unused>
    struct InternalTypes<wchar_t, unused>
    {
        typedef RPC_WSTR RPC_STR_TYPE;
    };

    /// Map wchar_t to RPC_WSTR or char to RPC_CSTR, based on the CharType.
    typedef typename InternalTypes<CharType, bool>::RPC_STR_TYPE RPC_STR_TYPE;

/// Data (internal use only)
private:
    ::UUID m_uuid;
    CharType* m_uuid_str;
};

/// Pull in the implementation for the member functions.
#include "WinUuid.inl"

} // namespace ocl

#endif // OCL_GUARD_PORTABLE_UUID_PLATFORM_WIN_WINUUID_HPP
