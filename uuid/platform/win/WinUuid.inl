/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType>
WinUuid<CharType>::WinUuid() : m_uuid_str(nullptr)
{
    (void)Generate();
}

template<typename CharType>
WinUuid<CharType>::~WinUuid()
{
    delete[] m_uuid_str;
}

template<typename CharType>
::UUID const& WinUuid<CharType>::GetUUID() const noexcept
{
    return m_uuid;
}

template<typename CharType>
CharType const* WinUuid<CharType>::GetString() const noexcept
{
    return m_uuid_str;
}

template<typename CharType>
bool WinUuid<CharType>::Generate()
{
    bool success = false;

    ::UUID uuid;
    if (::UuidCreate(&uuid) == RPC_S_OK)
    {
        RPC_STR_TYPE uuid_rpc_str = nullptr;
        if (GetUuidString(uuid, uuid_rpc_str) == RPC_S_OK)
        {
            std::size_t len = GetLength(uuid_rpc_str);
            CharType* uuid_str = new CharType[len + 1];
            StringCopy(uuid_str, uuid_rpc_str);
            UuidStringFree(uuid_rpc_str);

            m_uuid = uuid;
            m_uuid_str = uuid_str;
        }
    }

    return success;
}

template<typename CharType>
std::size_t WinUuid<CharType>::GetLength(RPC_CSTR str)
{
    RPC_CSTR tmp = str;
    for (; *tmp != 0; ++tmp)
        ;
    return static_cast<std::size_t>(tmp - str);
}

template<typename CharType>
std::size_t WinUuid<CharType>::GetLength(RPC_WSTR str)
{
    RPC_WSTR tmp = str;
    for (; *tmp != 0; ++tmp)
        ;
    return static_cast<std::size_t>(tmp - str);
}

template<typename CharType>
RPC_STATUS WinUuid<CharType>::GetUuidString(::UUID const& uuid, RPC_CSTR& uuid_str)
{
    return ::UuidToStringA(&uuid, &uuid_str); // RPC_S_OK for success.
}

template<typename CharType>
RPC_STATUS WinUuid<CharType>::GetUuidString(::UUID const& uuid, RPC_WSTR& uuid_str)
{
    return ::UuidToStringW(&uuid, &uuid_str); // RPC_S_OK for success.
}

template<typename CharType>
void WinUuid<CharType>::UuidStringFree(RPC_CSTR& uuid_str)
{
    ::RpcStringFreeA(&uuid_str);
    uuid_str = nullptr;
}

template<typename CharType>
void WinUuid<CharType>::UuidStringFree(RPC_WSTR& uuid_str)
{
    ::RpcStringFreeW(&uuid_str);
    uuid_str = nullptr;
}
